//
//  UnderReviewsViewController.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/12/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


//var count: Int = 0

var underReviewResult = NSDictionary()
var underReviewNumber = 1

class UnderReviewsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate
{

    
       @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuBarBtn: UIButton!
    
       var myColor = [String]()
    
       var refreshControl:UIRefreshControl!
    
       var dynamicView = UIView()
    
       var underReviewEssay:NSMutableArray = []
    
       var elements: NSMutableArray = []
    
       var currentPage = 0
       var nextPage = 0
    
       var x: CGFloat = 0.0
       var navx: CGFloat = 0.0
    
       var page = 0
    
       var limit = 50
    
       var hitWebService:Bool = true

    
    
      @IBOutlet weak var navBar: UINavigationBar!
    
    
      //MARk: - menuBtnPressed() method
      @IBAction func menuBtnPressed(sender: AnyObject)
      {

        if currentUserRole == "teacher"
        {
            if(x == -myMenuView.frame.size.width)
            {
                //navBar.frame.origin.x = 0
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = 0
                navx = myMenuView.frame.size.width
            }
                
            else
            {
                //navBar.frame.origin.x = myMenuView.frame.size.width
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = -myMenuView.frame.size.width
                navx = 0
            }
            
            
            UIView.animateWithDuration(0.5, animations: {
                
                self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
            })
        }
        else
        {
            if(x == -myMenuViewStudent.frame.size.width)
            {
                //navBar.frame.origin.x = 0
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = 0
                navx = myMenuViewStudent.frame.size.width
            }
                
            else
            {
                //navBar.frame.origin.x = myMenuView.frame.size.width
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = -myMenuViewStudent.frame.size.width
                navx = 0
            }
            
            
            UIView.animateWithDuration(0.5, animations: {
                
                self.navBar.frame.origin.x = self.navx
                myMenuViewStudent.frame.origin.x = self.x
                
            })
        }
        
    }
    
     //MARK: - viewWillAppear() method
     override func viewWillAppear(animated: Bool)
     {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.hidden=true
    
    }
    
    
    //MARK: - iPadCompatibility()
    func iPadCompatibility()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            menuBarBtn.frame = CGRectMake(0, 0, 45, 45)
        }
        
    }

    
    //MARK: - viewDidLoad() method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //STATUS BAR COLOR
        let statusBarView:UIView = UIView()
        statusBarView.frame = CGRectMake(0, 0, self.view.frame.width, 20)
        statusBarView.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 0.85)
        self.view.addSubview(statusBarView)
        
        navBar.barTintColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1)


        
        // x = -320
        currentVC = self
        if currentUserRole == "teacher"
        {
            self.view.addSubview(myMenuView)
            
            myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
            x = -myMenuView.frame.size.width
        }
        else
        {
            self.view.addSubview(myMenuViewStudent)
            
            myMenuViewStudent.frame = CGRectMake(-myMenuViewStudent.frame.size.width, self.view.frame.origin.y,myMenuViewStudent.frame.size.width ,myMenuViewStudent.frame.size.height)
            x = -myMenuViewStudent.frame.size.width
        }
        
        
        //underReviewimageView.image = UIImage(named: "ic_underreview.png")
        
        // Pull to Refresh Control
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: Selector("refresh"), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
        
        iPadCompatibility()
        
        createActivityIndicator()
        
        fetchData(0, limit: limit, reloadFromStart:false)
        
        tableView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 10
}

    //MARK: - createActivityIndicator() method
    func createActivityIndicator()
    {
        //CREATING VIEW IN A TABLE VIEW
        
        dynamicView.frame = CGRectMake(0,0,self.view.frame.width,self.view.frame.height)
        dynamicView.backgroundColor = UIColor.blackColor()
        dynamicView.alpha = 0.75
        
        
        //CREATING ACTIVITY INDICATOR
        var activityIndicator: UIActivityIndicatorView
        = UIActivityIndicatorView(activityIndicatorStyle:UIActivityIndicatorViewStyle.White)
        
        var xx = ((dynamicView.frame.width/2) - (activityIndicator.frame.width/2))
        var y = ((dynamicView.frame.height/2) - (activityIndicator.frame.height/2))
        activityIndicator.frame = CGRectMake(xx, y, activityIndicator.frame.width, activityIndicator.frame.height)
        
        dynamicView.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        
        self.view.addSubview(dynamicView)
    }
    
    
    
    //MARK: - refresh() method
    func refresh()
    {
        println("after refreshing")
        
        fetchData(0, limit: underReviewEssay.count, reloadFromStart:true)
    }

    
    
    //MARK: - fetxhData() API for fetching essays underReview
    func fetchData(page: Int, limit: Int, reloadFromStart:Bool)
    {
        
        dynamicView.hidden = false
        
        // UnderReview essay api
        var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=underreview&uid=\(user_id)&page=\(page)&limit=\(limit)"
        
        println(urlPath)
        
        let url: NSURL = NSURL(string : urlPath)!
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            
            
            
            if error != nil
            {
                self.dynamicView.hidden = true
                println(error.localizedDescription)
            }
            
            println(data)
            
            var err: NSError?
            
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                var resultDict:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
                self.dynamicView.hidden = true
                
                if err != nil
                {
                    println("JSON Error \(err!.localizedDescription)")
                }
                
                
                println(resultDict)
                
                var success = resultDict.valueForKey("status") as! String
                println(success)
                
                
                if var posts = resultDict.valueForKey("post") as? NSMutableArray
                {
                    
                    println(posts)
                    if reloadFromStart
                    {
                        self.underReviewEssay = []
                    }
                    
                    for post in posts
                    {
                        self.underReviewEssay.addObject(post)
                    }
                    
                    if posts.count < limit
                    {
                        self.hitWebService = false
                    }
                    else
                    {
                        self.hitWebService = true
                    }
                    
                    self.tableView.reloadData()
                    
                    if reloadFromStart
                    {
                        self.refreshControl?.endRefreshing()
                    }
                    

                    
                }
                    
                else
                {
                    var alert = UIAlertView(title: "Error..!", message: "Data not Found", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                    {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    alert.show()
                    println("Login failed1")
                    
                    self.dynamicView.hidden = true
                }
                
                
                self.tableView.reloadData()
                
               // self.dynamicView.hidden = true
                
            })
            
        })
        
        task.resume()
        

    }
    
    
    
    //MARK: - scrollViewDidScroll() method
    func scrollViewDidScroll(_scrollView: UIScrollView){
        var newScroll = Int(_scrollView.contentOffset.y)
        
        var maxScroll = (underReviewEssay.count*100)-Int(self.tableView.frame.height)
        
        if newScroll >= maxScroll && hitWebService == true
        {
            hitWebService = false
            fetchData(underReviewEssay.count, limit: limit, reloadFromStart:false)
        }
        
        println((6*100)-self.tableView.frame.height)
    }

    
    
    //MARK: - UITableViewMethods()
    func tableView(tableView: UITableView, numberOfSectionsInTable sections:Int) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return underReviewEssay.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UnderReviewTableViewCell
        
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        tableView.separatorColor = UIColor.clearColor()
        
        
        cell.backBtn.frame = CGRectMake(0, 0, cell.frame.width, cell.frame.height)
        //cell.backBtn.backgroundColor = UIColor.grayColor()
        cell.backBtn.userInteractionEnabled = false

        
        
        cell.cellView.frame = CGRectMake((0.03125*cell.contentView.frame.width), 0, (cell.contentView.frame.width)-(2*(0.03125*cell.contentView.frame.width)), 80)
        cell.cellView.layer.borderWidth = 1.5
        cell.cellView.layer.borderColor = UIColor.lightGrayColor().CGColor
        cell.cellView.layer.masksToBounds = true
        
        
        cell.colorLabel.frame = CGRectMake(0, 0, 0.01875*cell.contentView.frame.width, cell.cellView.frame.height)
        
        
        cell.colorLabel.backgroundColor = getRandomColor()      //calling colors method
        
        
        
        cell.view.frame = CGRectMake(0.109375*cell.contentView.frame.width, 0, 0.843*cell.contentView.frame.width, 80)
        
        cell.countLabel.text = "\(indexPath.row + 1)"
        cell.countLabel.frame = CGRectMake(0.03125*cell.contentView.frame.width, 0, 30, cell.cellView.frame.height)
        cell.countLabel.textColor = UIColor.blackColor()
        //cell.countLabel.font = UIFont.systemFontOfSize(20)
        cell.countLabel.font = UIFont.boldSystemFontOfSize(15)
        cell.countLabel.textAlignment = NSTextAlignment.Center
        
        
        cell.text_Label.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
        cell.text_Label.frame = CGRectMake(0.03125*cell.contentView.frame.width, 0, 0.6875*cell.contentView.frame.width, 58)
        cell.text_Label.numberOfLines = 3
        cell.text_Label.font = UIFont.boldSystemFontOfSize(14)
        cell.text_Label.textColor = UIColor.darkGrayColor()
        
        
        cell.PostStatus.text = "Assigned to:"
        cell.PostStatus.frame = CGRectMake(0.03125*cell.contentView.frame.width, 60, 0.390*cell.contentView.frame.width, 15)
        cell.PostStatus.font = UIFont.systemFontOfSize(11)
        cell.PostStatus.textColor = UIColor(red: 50/255.0, green: 50/255.0, blue: 50/255.0, alpha: 1.0)

        
        
        cell.assignedTo_Label.text = "Assigned to Robert Thomas"
        cell.assignedTo_Label.frame = CGRectMake(0.23125*cell.contentView.frame.width, 60, 0.390*cell.contentView.frame.width, 15)
        cell.assignedTo_Label.font = UIFont.boldSystemFontOfSize(11)
        cell.assignedTo_Label.textColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
        
        cell.date_Label.text = "Jan 24, 2015"
        cell.date_Label.frame = CGRectMake(0.453125*cell.contentView.frame.width, 60, 0.21875*cell.contentView.frame.width, 15)
        cell.date_Label.font = UIFont.boldSystemFontOfSize(11)
        cell.date_Label.textColor = UIColor(red: 80/255.0, green: 80/255.0, blue: 80/255.0, alpha: 1.0)
        
        cell.watchImage.image = UIImage(named: "icon-watch.png")
        cell.watchImage.frame = CGRectMake(0.40625*cell.contentView.frame.width, 60, 0.04375*cell.contentView.frame.width, 14)
        
        var image = UIImage(named: "icon-2.png")
        cell.accessoryImage.frame = CGRectMake(0.74375*cell.contentView.frame.width, 0, 0.09375*cell.contentView.frame.width, cell.view.frame.height)
        cell.accessoryImage.setImage(image, forState: UIControlState.Normal)
        
        
        var content="";
        var obj = underReviewEssay[indexPath.row] as! NSDictionary
        println(obj)
        
        println(underReviewEssay[indexPath.row]["post-title"])
        println(underReviewEssay[indexPath.row]["acceptdate"])
        println(underReviewEssay[indexPath.row]["assignto"])
        println(underReviewEssay[indexPath.row]["post-status"])
        println(underReviewEssay[indexPath.row]["post-id"])
        
        cell.text_Label.text = underReviewEssay[indexPath.row]["post-title"] as? String
        cell.date_Label.text = underReviewEssay[indexPath.row]["acceptdate"] as? String
        cell.assignedTo_Label.text = underReviewEssay[indexPath.row]["assignto"] as? String
        
        return cell
        
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        underReviewResult = underReviewEssay[indexPath.row] as! NSDictionary
        
        underReviewNumber = indexPath.row+1
        
        println("move to underReview View")
        
        println(underReviewResult)
        
        let underReviewVC = self.storyboard!.instantiateViewControllerWithIdentifier("UnderReviewEditViewControllerSegue")as! UnderReviewEditViewController
        
        self.navigationController?.pushViewController(underReviewVC, animated: true)

        
    }
    
    
    
    
    //MARK: - getRandomColor() method
    func getRandomColor() -> UIColor
    {
        
        var randomRed:CGFloat = CGFloat(drand48())
        
        var randomGreen:CGFloat = CGFloat(drand48())
        
        var randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
    

    //MARK: - didReceiveMemoryWarning() method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
