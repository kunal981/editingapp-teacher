//
//  MyMenuViewController.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


var InvitationsResult:NSDictionary!



class InvitationsViewController: UIViewController, UIAlertViewDelegate,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate
{
    
    @IBOutlet weak var tableView: UITableView!
  
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
   
    var refreshControl:UIRefreshControl!
    
    var invitationsArray:NSMutableArray = []
    
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    var dynamicView = UIView()
    
    var page = 0
    var limit = 50
    
    var hitWebService:Bool = true
    
    @IBOutlet weak var menuBtn_lbl: UIButton!
    var alert: UIAlertView!
    
    var acceptPost: NSDictionary!
    var rejectPost: NSDictionary!
    
    @IBOutlet weak var navBar: UINavigationBar!
   
    
     //MARK: - menuBtnPressed() button
    @IBAction func menuBtnPressed(sender: AnyObject)
    {
            
        if currentUserRole == "teacher"
        {
            if(x == -myMenuView.frame.size.width)
            {
                //navBar.frame.origin.x = 0
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = 0
                navx = myMenuView.frame.size.width
            }
                
            else
            {
                //navBar.frame.origin.x = myMenuView.frame.size.width
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = -myMenuView.frame.size.width
                navx = 0
            }
            
            
            UIView.animateWithDuration(0.5, animations: {
                
                self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
            })
        }
        else
        {
            if(x == -myMenuViewStudent.frame.size.width)
            {
                //navBar.frame.origin.x = 0
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = 0
                navx = myMenuViewStudent.frame.size.width
            }
                
            else
            {
                //navBar.frame.origin.x = myMenuView.frame.size.width
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = -myMenuViewStudent.frame.size.width
                navx = 0
            }
            
            
            UIView.animateWithDuration(0.5, animations: {
                
                self.navBar.frame.origin.x = self.navx
                myMenuViewStudent.frame.origin.x = self.x
                
            })
        }

    }
    
    
    
    
    
     //MARK: - viewDidLoad() method
     override func viewDidLoad()
    {
        super.viewDidLoad()
        
    
         //STATUS BAR COLOR
        let statusBarView:UIView = UIView()
        statusBarView.frame = CGRectMake(0, 0, self.view.frame.width, 20)
        statusBarView.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 0.85)
        self.view.addSubview(statusBarView)
        
        navBar.barTintColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1)
        
         //tableView.backgroundColor = UIColor.blackColor()
        
        currentVC = self
        
//        invitationButton.backgroundColor = UIColor(red: 35/255.0, green: 158/255.0, blue: 219/255.0, alpha: 1.0)
//        invitationimageView.image = UIImage(named: "ic_invitation_hover.png")
//        
        if currentUserRole == "teacher"
        {
            self.view.addSubview(myMenuView)
            
            myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
            x = -myMenuView.frame.size.width
        }
        else
        {
            self.view.addSubview(myMenuViewStudent)
            
            myMenuViewStudent.frame = CGRectMake(-myMenuViewStudent.frame.size.width, self.view.frame.origin.y,myMenuViewStudent.frame.size.width ,myMenuViewStudent.frame.size.height)
            x = -myMenuViewStudent.frame.size.width
        }

        iPadCompatibility()
        
        addPullToRefreshTableView()     // calling refresh method
        
        activityIndicator()             //calling activity indicator method
        dynamicView.hidden = false
        fetchData(0, limit: limit, reloadFromStart:false)
        
        tableView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 10
        
        

    }
    
    
    //MARK: - activityIndicator()
    func activityIndicator()
    {
        //CREATING VIEW IN A TABLE VIEW
        dynamicView.frame = CGRectMake(0,0,self.view.frame.width,self.view.frame.height)
        dynamicView.backgroundColor = UIColor.blackColor()
        dynamicView.alpha = 0.75
        self.view.addSubview(dynamicView)
        
        //CREATING ACTIVITY INDICATOR
        var activityIndicator: UIActivityIndicatorView
        = UIActivityIndicatorView(activityIndicatorStyle:UIActivityIndicatorViewStyle.White)
        
        
        var x_activityIndicator = ((dynamicView.frame.width/2) - (activityIndicator.frame.width/2))
        var y_activityIndicator = ((dynamicView.frame.height/2) - (activityIndicator.frame.height/2))
        activityIndicator.frame = CGRectMake(x_activityIndicator, y_activityIndicator, activityIndicator.frame.width, activityIndicator.frame.height)
        
        dynamicView.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
 
    }
    
    
    //MARK: - iPadCompatibility()
    func iPadCompatibility()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            menuBtn_lbl.frame = CGRectMake(0, -2, 40, 40)
        }
    }
    

    
     //MARK: - fetchData() Invitation Api
    func fetchData(page: Int, limit: Int, reloadFromStart:Bool)
    {
        
        
        var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=invitation&uid=\(user_id)&page=\(page)&limit=\(limit)"
        
        println(urlPath)
        
        let url: NSURL = NSURL(string : urlPath)!
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            
            
            
            if error != nil
            {
                self.dynamicView.hidden = true
                
                println(error.localizedDescription)
            }
            
            println(data)
            
            var err: NSError?
            
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                
                self.dynamicView.hidden = true
                
                InvitationsResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
                
                if err != nil
                {
                    println("JSON Error \(err!.localizedDescription)")
                }
                
                println(InvitationsResult)
                
                var success = InvitationsResult.valueForKey("status") as! String
                println(success)
                
                
                if var posts = InvitationsResult.valueForKey("post") as? NSMutableArray
                {
                    
                    println(posts)
                    
                    if reloadFromStart
                    {
                        self.invitationsArray = []
                    }
                    
                    for post in posts
                    {
                        self.invitationsArray.addObject(post)
                    }
                    
                    if posts.count < limit
                    {
                        self.hitWebService = false
                    }
                    else
                    {
                        self.hitWebService = true
                    }
                    
                    self.tableView.reloadData()
                    
                    if reloadFromStart
                    {
                        self.refreshControl?.endRefreshing()
                    }
                    
                }
                else
                {
                    self.alert = UIAlertView(title: "", message: "Data not Found", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                    {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    self.alert.show()
                    println("Login failed1")
                    
                    self.dynamicView.hidden = true
                }
                
                
                
                self.tableView.reloadData()
                
                self.dynamicView.hidden = true
                
            })
            
        })
        
        task.resume()
    }

    
     //MARK: - refresh table function
    func addPullToRefreshTableView()
    {
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: Selector("refresh"), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    
    
    func refresh()
    {
        println("after refresh")
        
        fetchData(0, limit: invitationsArray.count, reloadFromStart:true)
        
    }
    

    
     //MARK: - viewWillAppear()
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        
        //self.navigationController?.navigationBar.hidden = true
        UIApplication.sharedApplication().statusBarHidden = false
        
    }
    
    
    
    
     //MARK: - scrollViewDidScroll function
    func scrollViewDidScroll(_scrollView: UIScrollView)
    {
        var newScroll = Int(_scrollView.contentOffset.y)
        
        var maxScroll = (invitationsArray.count*100)-Int(self.tableView.frame.height)
        
        if newScroll >= maxScroll && hitWebService == true
        {
            hitWebService = false
            fetchData(invitationsArray.count, limit: limit, reloadFromStart:false)
        }
        
        println((6*100)-self.tableView.frame.height)
    }
    
    
    
    
    //MARK: - didReceiveMemoryWarning() method
     override func didReceiveMemoryWarning()
     {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    
    //MARK: - UITableView() methods
    func tableView(tableView: UITableView, numberOfSectionsInTable sections:Int) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       
        return invitationsArray.count
    }
    
    

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)as! InvitationsTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        tableView.separatorColor = UIColor.clearColor()
        
        //cell.backgroundColor = UIColor(red: 209/255.0, green: 209/255.0, blue: 209/255.0, alpha: 1.0)
        
        
        
        cell.backBtn.frame = CGRectMake(0, 0, cell.frame.width, cell.frame.height)
        cell.backBtn.userInteractionEnabled = false
        
        
        cell.cellView.frame = CGRectMake((0.03125*cell.contentView.frame.width), 0, (cell.contentView.frame.width)-(2*(0.03125*cell.contentView.frame.width)), 80)
        cell.cellView.layer.borderWidth = 1.5
        cell.cellView.layer.borderColor = UIColor.lightGrayColor().CGColor
        cell.cellView.layer.masksToBounds = true

    
        cell.colorLabel.frame = CGRectMake(0, 0, 0.01875*cell.contentView.frame.width, cell.cellView.frame.height)
        
        
        cell.colorLabel.backgroundColor = getRandomColor()          //calling getRandomColor() method
        
        
        
        cell.view.frame = CGRectMake(0.109375*cell.contentView.frame.width, 0, 0.590625*cell.contentView.frame.width, 80)
        //cell.view.backgroundColor = UIColor.grayColor()
        
        
        cell.countLabel.text = "\(indexPath.row + 1)"
        cell.countLabel.frame = CGRectMake(0.03125*cell.contentView.frame.width, 0, 30, cell.cellView.frame.height)
        cell.countLabel.textColor = UIColor.blackColor()
        //cell.countLabel.font = UIFont.systemFontOfSize(20)
        cell.countLabel.font = UIFont.boldSystemFontOfSize(15)
        cell.countLabel.textAlignment = NSTextAlignment.Center
        
        
        cell.text_Label.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
        cell.text_Label.frame = CGRectMake(0.03125*cell.contentView.frame.width, 0, 0.6875*cell.contentView.frame.width, 58)
       // cell.text_Label.numberOfLines = 3
        cell.text_Label.font = UIFont.boldSystemFontOfSize(14)
        cell.text_Label.textColor = UIColor.darkGrayColor()
        
        cell.PostStatus.text = "Author:"
        cell.PostStatus.frame = CGRectMake(0.03125*cell.contentView.frame.width, 60, 0.390*cell.contentView.frame.width, 15)
        cell.PostStatus.font = UIFont.systemFontOfSize(11)
        cell.PostStatus.textColor = UIColor(red: 50/255.0, green: 50/255.0, blue: 50/255.0, alpha: 1.0)
        
        
        cell.assignedTo_Label.text = "Assigned to Robert Thomas"
        cell.assignedTo_Label.frame = CGRectMake(0.146875*cell.contentView.frame.width, 60, 0.390*cell.contentView.frame.width, 15)
        cell.assignedTo_Label.font = UIFont.boldSystemFontOfSize(11)
        cell.assignedTo_Label.textColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
        
        cell.date_Label.text = "Jan 24, 2015"
        cell.date_Label.frame = CGRectMake(0.359375*cell.contentView.frame.width, 60, 0.21875*cell.contentView.frame.width, 15)
        cell.date_Label.font = UIFont.boldSystemFontOfSize(11)
        cell.date_Label.textColor = UIColor(red: 80/255.0, green: 80/255.0, blue: 80/255.0, alpha: 1.0)
        
        cell.watchImage.image = UIImage(named: "icon-watch.png")
        cell.watchImage.frame = CGRectMake(0.3125*cell.contentView.frame.width, 60, 0.04375*cell.contentView.frame.width, 14)
        
        
        let image:UIImage = UIImage(named: "ic_accept.png")!
        
        cell.acceptButton.frame = CGRectMake(0.703125*cell.contentView.frame.width,8,0.21875*cell.contentView.frame.width, 30)
        cell.acceptButton.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1)
        cell.acceptButton.setTitle ("Accept", forState: UIControlState.Normal)
        cell.acceptButton.titleLabel?.font = UIFont.boldSystemFontOfSize(13)
        //cell.acceptButton.tag = 1
        cell.acceptButton.tag = indexPath.row
        
        cell.acceptButton.setImage(image, forState: UIControlState.Normal)

        cell.acceptButton.addTarget(self, action:"acceptAlert:",forControlEvents: UIControlEvents.TouchUpInside)
        
        cell.acceptButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5)
        cell.acceptButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
        
        

        
        let rejectImage:UIImage = UIImage(named: "ic_reject.png")!
        
        cell.rejectButton.frame = CGRectMake(0.703125*cell.contentView.frame.width,42,0.21875*cell.contentView.frame.width, 30)
        cell.rejectButton.backgroundColor = UIColor(red: 241/255, green: 74/255, blue:75/255, alpha: 1)
        cell.rejectButton.setTitle ("Reject", forState: UIControlState.Normal)
        cell.rejectButton.titleLabel?.font = UIFont.boldSystemFontOfSize(13)
        
        //cell.rejectButton.tag = 2
        cell.rejectButton.tag = indexPath.row
        
        cell.rejectButton.setImage(rejectImage, forState: UIControlState.Normal)
        cell.rejectButton.addTarget(self, action:"rejectAlert:",forControlEvents: UIControlEvents.TouchUpInside)
        

        cell.rejectButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5)
        cell.rejectButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)

    
        println(indexPath.row)
        
        
        var content="";
        var obj = invitationsArray[indexPath.row] as! NSDictionary
        println(obj)
        
        println(invitationsArray[indexPath.row]["post-title"])
        println(invitationsArray[indexPath.row]["postdate"])
        println(invitationsArray[indexPath.row]["author"])
        
        cell.text_Label.text = invitationsArray[indexPath.row]["post-title"] as? String
        cell.date_Label.text = invitationsArray[indexPath.row]["postdate"] as? String
        cell.assignedTo_Label.text = invitationsArray[indexPath.row]["author"] as? String
        
        return cell
    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        currentEssay = invitationsArray[indexPath.row] as! NSDictionary
        
        currentEssayNumber = indexPath.row+1
        
        println("move to my essay view")
        
        println(currentEssay)
        
        let myEssayVC = self.storyboard!.instantiateViewControllerWithIdentifier("InvitationDetailsViewControllerSegue") as! InvitationDetailsViewController
        
        self.navigationController?.pushViewController(myEssayVC, animated: true)
        
    }

    
    
    //MARK: -  getRandomColor() method
    func getRandomColor() -> UIColor
    {
        
        var randomRed:CGFloat = CGFloat(drand48())
        
        var randomGreen:CGFloat = CGFloat(drand48())
        
        var randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }

    
    
    
     //MARK: - accept button method
    func acceptAlert(sender: UIButton!)
    {
        
        InvitationsResult = invitationsArray[sender.tag] as! NSDictionary
        
        println(myInvitationResult)
        

        dynamicView.hidden = false
        
        // for alerts
        alert = UIAlertView(title: "", message: "Do you want to accept this post?", delegate: self, cancelButtonTitle: "No", otherButtonTitles: "Yes")
        
        alert.tag = 1
        
        let no_Btn = UIAlertAction(title:"No", style: .Cancel)
        {
                (action: UIAlertAction!) -> Void in
        }
        
        
        
        let yes_Btn = UIAlertAction(title:"Yes", style: .Default)
        {
                (action: UIAlertAction!) -> Void in
        }
        
        
        alert.show()
    }
    
    
    
    //MARK: - UIAlertViewDelegate() method
    func alertView(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int)
    {
        
        if ((buttonIndex == 1) && (alert.tag == 1))
        {
            println(InvitationsResult)
            
            
            post_id = InvitationsResult["post-id"] as! Int
            println(post_id)
            
            
            acceptEssay()       //calling AcceptEssay() API
        }
        else
        {
            dynamicView.hidden = true
        }
        
        
        
        
        
        // MARk: - reject essay api
        if ((buttonIndex == 1) && (alert.tag == 4))
        {
            println("reject button clicked")
            
            println(InvitationsResult)
            if InvitationsResult["post-id"] != nil
            {
            
            post_id = InvitationsResult["post-id"] as! Int
            }
            println(post_id)

            
            rejectEssay()           //calling rejectEssay() API
        }
        else
        {
            dynamicView.hidden = true
        }

    }
    
    
    
    
    //MARK: - acceptEssay() API
    func acceptEssay()
    {
        dynamicView.hidden = false
        
        var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=accept&uid=\(user_id)&post-id=\(post_id)"
        
        println(urlPath)
        
        let url: NSURL = NSURL(string: urlPath.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: nil, range: nil))!
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            
            if error != nil
            {
                
               self.dynamicView.hidden = true
                
                // If there is an error in the web request, print it to the console
                println(error.localizedDescription)
            }
            println(data)
            
            
            
            var err: NSError?
            
            self.acceptPost = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err)as! NSDictionary
            
            println(self.acceptPost)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                 self.dynamicView.hidden = true
                
                if (self.acceptPost != nil)
                {
                    
                    var success = self.acceptPost.valueForKey("status") as! String
                    println(success)
                    
                    
                    if(success == "Success")
                    {
                        println("Acccepted")
                        
                        
                        self.alert = UIAlertView(title: "", message: "Post Accepted", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                        
                        self.alert.tag = 2
                        
                        let okBtn = UIAlertAction(title:"OK", style: .Default)
                        {
                                (action: UIAlertAction!) -> Void in
                                
                        }
                        
                        
                        self.alert.show()
                    }
                    
                }
                    
                else
                {
                    self.alert = UIAlertView(title: "Internet Error..!", message: "Please check your Internet Settings", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    self.alert.tag = 3
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                        {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    self.alert.show()
                    
                }
                
                
            })
            
            
        })
        task.resume()

    }
    
    
    
    
    //MARK; - rejectEssay() API
    func rejectEssay()
    {
        dynamicView.hidden = false
        
        var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=reject&uid=\(user_id)&post-id=\(post_id)"
        
        println(urlPath)
        
        let url: NSURL = NSURL(string: urlPath.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: nil, range: nil))!
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            
            if error != nil
            {
                
                self.dynamicView.hidden = true
                
                // If there is an error in the web request, print it to the console
                println(error.localizedDescription)
            }
            println(data)
            
            
            
            var err: NSError?
            
            self.rejectPost = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
            
            println(self.rejectPost)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                self.dynamicView.hidden = true
                
                if (self.rejectPost != nil)
                {
                    
                    var success = self.rejectPost.valueForKey("status") as! String
                    println(success)
                    
                    
                    if(success == "Success")
                    {
                        println("Rejected")
                        
                        
                        self.alert = UIAlertView(title: "", message: "Post Rejected", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                        
                        self.alert.tag = 5
                        
                        let okBtn = UIAlertAction(title:"OK", style: .Default)
                            {
                                (action: UIAlertAction!) -> Void in
                                
                        }
                        
                        
                        self.alert.show()
                    }
                    
                }
                    
                else
                {
                    self.alert = UIAlertView(title: "Internet Error..!", message: "Please check your Internet Settings", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                        {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    self.alert.show()
                    self.dynamicView.hidden = true
                }
                
                
            })
            
            
        })
        task.resume()

    }

    
     //MARK: - clickedButtonAtIndex method
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if alert.tag == 2
        {
            let ok_btn = self.storyboard!.instantiateViewControllerWithIdentifier("MyInvitationsViewControllerSegue")as! MyInvitationsViewController
            
            self.navigationController?.pushViewController(ok_btn,animated: true)
        }
    }
    

    
     //MARK: - reject button alert
    func rejectAlert(sender: UIButton!)
    {
        
        InvitationsResult = invitationsArray[sender.tag] as! NSDictionary
        
        println(myInvitationResult)
        

        dynamicView.hidden = false
        
        // for alerts
        alert = UIAlertView(title: "", message: "Do you want to reject this post?", delegate: self, cancelButtonTitle: "No", otherButtonTitles: "Yes")
        
        alert.tag = 4
        
        let no_Btn = UIAlertAction(title:"No", style: .Cancel)
        {
                (action: UIAlertAction!) -> Void in
        }
        
        
        
        let yes_Btn = UIAlertAction(title:"Yes", style: .Default)
        {
                (action: UIAlertAction!) -> Void in
        }
        
        
        alert.show()
               
    }

    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

