//
//  InvitationDetailsViewController.swift
//  EditingApp_Teacher
//
//  Created by mrinal khullar on 7/16/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit




var editEssay = NSDictionary()
var viewEssay = NSDictionary()
var postValuesDictionary: NSDictionary = NSDictionary()

var post_id:Int = Int()


class InvitationDetailsViewController: UIViewController,UITextViewDelegate,UIAlertViewDelegate
{

    @IBOutlet weak var barButton: UIButton!
   
    @IBOutlet weak var watchIcon: UIImageView!
    var subView:UIView = UIView()
    
    var alert_accept:UIAlertView!
    var acceptPost:NSDictionary!
    
    var rejectPost:NSDictionary!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var mySubView: UIView!
    
    @IBOutlet weak var myEssayTextView: UITextView!
    
    @IBOutlet weak var count_Label: UILabel!
    @IBOutlet weak var assignTo_label: UILabel!
    @IBOutlet weak var date_Label: UILabel!
    
    @IBOutlet weak var accept_btn: UIButton!
    var x:CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    @IBOutlet weak var reject_btn: UIButton!
    
    //MARK: - back button method
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    //MARK: - iPadCompatibility() method
    func iPadCompatibility()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            barButton.frame = CGRectMake(0, 0, 40, 40)
            
            myEssayTextView.font = UIFont.systemFontOfSize(18)
            
            myEssayTextView.editable = true
            myEssayTextView.font = myEssayTextView.font
            myEssayTextView.editable = false
            
            count_Label.frame = CGRectMake(0, 0, 65, 60)
            count_Label.font = UIFont.boldSystemFontOfSize(24)

            watchIcon.frame = CGRectMake(500, 24, 32, 32)
            
            date_Label.font = UIFont.systemFontOfSize(22)
            
            assignTo_label.font = UIFont.boldSystemFontOfSize(24)
            assignTo_label.frame = CGRectMake(75, 22, 300, 27)
            
            accept_btn.titleLabel?.font = UIFont.systemFontOfSize(22)
            reject_btn.titleLabel?.font = UIFont.systemFontOfSize(22)
            
            
            mySubView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 25
            accept_btn.frame.origin.y = mySubView.frame.origin.y + mySubView.frame.height + 20
            reject_btn.frame.origin.y = mySubView.frame.origin.y + mySubView.frame.height + 20
            
        }
        else
        {
            
            mySubView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 10
            accept_btn.frame.origin.y = mySubView.frame.origin.y + mySubView.frame.height + 10
            reject_btn.frame.origin.y = mySubView.frame.origin.y + mySubView.frame.height + 10
            
            
            
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                
            
            }
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                
            }
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                barButton.frame = CGRectMake(0, 0, 35, 35)
            }
                
            else
            {
                
            }
            

        }
        
    }

    
    //MARK: - accept button method
    @IBAction func accept_Btn(sender: AnyObject)
    {
        subView.hidden=false
        
        // for alerts
        alert_accept = UIAlertView(title: "", message: "Do you want to accept this post?", delegate: self, cancelButtonTitle: "No", otherButtonTitles: "Yes")
        
        alert_accept.tag = 1
        
        let no_Btn = UIAlertAction(title:"No", style: .Cancel)
        {
                (action: UIAlertAction!) -> Void in
        }
        
        
        
        let yes_Btn = UIAlertAction(title:"Yes", style: .Default)
        {
                (action: UIAlertAction!) -> Void in
        }
        
        
        alert_accept.show()
        
    }
    
    
    
    //MARK: - UIAlertViewDelegate() method
    func alertView(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int)
    {
        
        if ((buttonIndex == 1) && (alert_accept.tag == 1))
        {
            println("yes btn clicked")
            
            subView.hidden = false
            
            acceptEssay()           //calling acceptEssay() API
        }
        else
        {
            subView.hidden = true
        }
        
        
        
        // MARk: - reject essay api
        if ((buttonIndex == 1) && (alert_accept.tag == 4))
        {
            subView.hidden = false
            rejectEssay()        //calling rejectEssay() API
        }
        else
        {
            subView.hidden = true
        }
        
    }
    
    
    //MARK: - acceptEssay() API
    func acceptEssay()
    {
        
        subView.hidden = false
        
        var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=accept&uid=\(user_id)&post-id=\(post_id)"
        
        println(urlPath)
        
        let url: NSURL = NSURL(string: urlPath.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: nil, range: nil))!
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            
            if error != nil
            {
                
                self.subView.hidden = true
                
                // If there is an error in the web request, print it to the console
                println(error.localizedDescription)
            }
            println(data)
            
            
            
            var err: NSError?
            
            self.acceptPost = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
            
            println(self.acceptPost)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                self.subView.hidden = true
                
                if (self.acceptPost != nil)
                {
                    
                    var success = self.acceptPost.valueForKey("status") as! String
                    println(success)
                    
                    
                    if(success == "Success")
                    {
                        println("Acccepted")
                        
                        
                        self.alert_accept = UIAlertView(title: "", message: "Post Accepted", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                        
                        self.alert_accept.tag = 2
                        
                        let okBtn = UIAlertAction(title:"OK", style: .Default)
                            {
                                (action: UIAlertAction!) -> Void in
                                
                        }
                        
                        
                        self.alert_accept.show()
                    }
                    
                }
                    
                else
                {
                    self.alert_accept = UIAlertView(title: "Internet Error..!", message: "Please check your Internet Settings", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    self.alert_accept.tag = 3
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                        {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    self.alert_accept.show()
                    
                }
                
                
            })
            
            
        })
        task.resume()
        
    }
    
    
    
    //MARK: - rejectEssay() API
    func rejectEssay()
    {
        
        subView.hidden = false
        
        var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=reject&uid=\(user_id)&post-id=\(post_id)"
        
        println(urlPath)
        
        let url: NSURL = NSURL(string: urlPath.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: nil, range: nil))!
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            
            if error != nil
            {
                
                self.subView.hidden = true
                
                // If there is an error in the web request, print it to the console
                println(error.localizedDescription)
            }
            println(data)
            
            
            
            var err: NSError?
            
            self.rejectPost = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
            
            println(self.rejectPost)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                self.subView.hidden = true
                
                if (self.rejectPost != nil)
                {
                    
                    var success = self.rejectPost.valueForKey("status") as! String
                    println(success)
                    
                    
                    if(success == "Success")
                    {
                        println("Rejected")
                        
                        
                        self.alert_accept = UIAlertView(title: "", message: "Post Rejected", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                        
                        self.alert_accept.tag = 5
                        
                        let okBtn = UIAlertAction(title:"OK", style: .Default)
                            {
                                (action: UIAlertAction!) -> Void in
                                
                        }
                        
                        
                        self.alert_accept.show()
                    }
                    
                }
                    
                else
                {
                    self.alert_accept = UIAlertView(title: "Internet Error..!", message: "Please check your Internet Settings", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                        {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    self.alert_accept.show()
                    
                }
                
                
            })
            
            
        })
        task.resume()
        
    }
    
    //MARK:- clickedButtonAtIndex method
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if alert_accept.tag == 2
        {
            let ok_btn = self.storyboard!.instantiateViewControllerWithIdentifier("MyInvitationsViewControllerSegue") as! MyInvitationsViewController
            
            self.navigationController?.pushViewController(ok_btn,animated: true)
        }
    }
    
    
    //MARK:- reject button alert
    @IBAction func reject_Btn(sender: AnyObject)
    {
        
        
        subView.hidden = false
        
        // for alerts
        alert_accept = UIAlertView(title: "", message: "Do you want to reject this post?", delegate: self, cancelButtonTitle: "No", otherButtonTitles: "Yes")
        
        alert_accept.tag = 4
        
        let no_Btn = UIAlertAction(title:"No", style: .Cancel)
            {
                (action: UIAlertAction!) -> Void in
        }
        
        
        
        let yes_Btn = UIAlertAction(title:"Yes", style: .Default)
        {
                (action: UIAlertAction!) -> Void in
        }
        
        
        alert_accept.show()
        
    }
    
    
    
    
    //MARK: - viewDidLoad() method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //STATUS BAR COLOR
        let statusBarView:UIView = UIView()
        statusBarView.frame = CGRectMake(0, 0, self.view.frame.width, 20)
        statusBarView.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 0.85)
        self.view.addSubview(statusBarView)
        
        navBar.barTintColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1)
        
        
        mySubView.layer.borderColor = UIColor.whiteColor().CGColor
        mySubView.layer.borderWidth = 2
        mySubView.layer.masksToBounds = true
        
        iPadCompatibility()
        
        createIndicator()          //calling activity Indicator() method
        
        viewAcceptedEssay()         //calling API for view accepted essay
        
        accept_btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5)
        accept_btn.titleEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0)
        
        reject_btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5)
        reject_btn.titleEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0)
        
       
    }
    
    
    
    
    //MARK: - activityIndicator() method
    func createIndicator()
    {
        
        
        subView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        subView.backgroundColor = UIColor.blackColor()
        subView.alpha = 0.75
        //subView.hidden = true
        self.view.addSubview(subView)
        
        
        
        let activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle:UIActivityIndicatorViewStyle.White)
        subView.addSubview(activityIndicator)
        var x = (subView.frame.width/2)-(activityIndicator.frame.width/2)
        println(x)
        var y = ((subView.frame.height/2)-(activityIndicator.frame.height/2))
        println(y)
        
        activityIndicator.frame = CGRectMake(x, y, activityIndicator.frame.width,activityIndicator.frame.height)
        
        activityIndicator.startAnimating()
        
        println("activity indicator starts")
        
    }
    
    
    
    //MARK: - viewAcceptedEssay() API
    func viewAcceptedEssay()
    {
        
        post_id = currentEssay["post-id"] as! Int
        println(post_id)
        
        
        subView.hidden = false
        
        var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=viewacceptedessay"
        
        var post:NSString = "uid=\(user_id)&post-id=\(post_id)"
        
        NSLog("PostData: %@",post);
        
        var url1:NSURL = NSURL(string: urlPath)!
        
        var postData:NSData = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength:NSString = String( postData.length )
        
        var request:NSMutableURLRequest = NSMutableURLRequest(URL: url1)
        request.HTTPMethod = "POST"
        request.HTTPBody = postData
        request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        
        var reponseError: NSError?
        var response: NSURLResponse?
        
        var urlData: NSData? = NSURLConnection.sendSynchronousRequest(request, returningResponse:&response, error:&reponseError)
        
        if ( urlData != nil )
        {
            let res = response as! NSHTTPURLResponse!;
            
            NSLog("Response code: %ld", res.statusCode);
            
            if (res.statusCode >= 200 && res.statusCode < 300)
            {
                var responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                
                NSLog("Response ==> %@", responseData);
                
                var error: NSError?
                
                
                subView.hidden  = true
                
                viewEssay = NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers , error: &error) as! NSDictionary
                
                
                var success:String = viewEssay.valueForKey("status") as! String
                
                
                println(success)
                
                
                if(success == "Success")
                {
                    println("invitations view")
                    
                    postValuesDictionary  = viewEssay["post"] as! NSDictionary
                    println(postValuesDictionary)
                    
                    subView.hidden = true
                    
                    myEssayTextView.text = postValuesDictionary["content"] as! String
                    assignTo_label.text = postValuesDictionary["title"] as? String
                    date_Label.text = postValuesDictionary["postdate"] as? String
                    
                    count_Label.text = "\(currentEssayNumber)" as String
                    
                    
                }
                else
                {
                    
                    var alert = UIAlertView(title: "Error..!", message: "Data not Found", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                        {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    alert.show()
                    println("Login failed1")
                }
                
            } else {
                
                println("Login failed2")
                
            }
        }
        else
        {
            
            //subView.hidden = true
            
            var alert = UIAlertView(title: "Internet Error..!", message: "Please check your Internet Settings", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
            
            let okBtn = UIAlertAction(title:"OK", style: .Default)
            {
                    (action: UIAlertAction!) -> Void in
            }
            
            
            alert.show()
            
            
            println("Login failed3")
        }
        
        
        println(viewEssay)
        
        println(currentEssay)
        println("row number = \(currentEssayNumber)")
        
        
    }
    
    
    
    //MARK: - textViewShouldReturn() method
    func textViewShouldReturn(textView: UITextView!) -> Bool
    {
        
        textView.resignFirstResponder()
        return true
    }
    
    
    
    
    
    //MARK: - hideKeyBoard() method
    @IBAction func hideKeyBoardButton(sender: AnyObject)
    {
        myEssayTextView.resignFirstResponder()
    }
    
    
    //MARK: - didReceiveMemoryWarning() method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
