//
//  UnderReviewTableViewCell.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/12/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class UnderReviewTableViewCell: UITableViewCell
{

    var cellView: UIView = UIView()
    var view: UIView = UIView()
    var colorLabel : UILabel = UILabel()
    var countLabel: UILabel = UILabel()
    var text_Label: UILabel = UILabel()
    var assignedTo_Label: UILabel = UILabel()
    var watchImage: UIImageView = UIImageView()
    var date_Label: UILabel = UILabel()
    var accessoryImage: UIButton = UIButton()
    var backBtn: UIButton = UIButton()
    var PostStatus: UILabel = UILabel()
    
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        contentView.addSubview(cellView)
        contentView.addSubview(backBtn)
        view.addSubview(text_Label)
        view.addSubview(PostStatus)
        view.addSubview(assignedTo_Label)
        view.addSubview(watchImage)
        view.addSubview(date_Label)
        view.addSubview(accessoryImage)
        
        cellView.addSubview(colorLabel)
        cellView.addSubview(countLabel)
        cellView.addSubview(view)
        
        
        var contentDictionary = ["color": colorLabel,
            "count": countLabel,
            "text": text_Label,
             "status": PostStatus,
            "task assigned": assignedTo_Label,
            "image": watchImage,
            "date": date_Label,
            "ButtonImage": accessoryImage]
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            text_Label.font = UIFont.systemFontOfSize(20)
            PostStatus.font = UIFont.systemFontOfSize(17)
            assignedTo_Label.font = UIFont.boldSystemFontOfSize(17)
            date_Label.font = UIFont.systemFontOfSize(17)
            
            //assignedTo_Label.frame = CGRectMake(130, 55, 100, 25)
            
            countLabel.frame = CGRectMake(20, 0, 40, cellView.frame.height)
            
            
            PostStatus.frame = CGRectMake(20, 53, 100, 25)
            assignedTo_Label.frame = CGRectMake(120, 53, 100, 25)
            
            watchImage.frame = CGRectMake(200, 50, 26, 26)
            date_Label.frame = CGRectMake(230, 52, 200, 25)
            

            
            countLabel.font = UIFont.boldSystemFontOfSize(20)
            
            
              accessoryImage.frame = CGRectMake(590, 0, 55, cellView.frame.height)
        }
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                assignedTo_Label.frame = CGRectMake(77, 60, 130, 15)
                
                PostStatus.frame  = CGRectMake(10, 60, 80, 15)
                
                watchImage.frame = CGRectMake(130, 61, 14, 14)
                
                date_Label.frame = CGRectMake(145, 60, 70, 15)
            }
                
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                assignedTo_Label.frame = CGRectMake(77, 60, 130, 15)
                
                PostStatus.frame  = CGRectMake(10, 60, 80, 15)
                
                watchImage.frame = CGRectMake(130, 61, 14, 14)
                
                date_Label.frame = CGRectMake(145, 60, 70, 15)

            }
                
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                
                assignedTo_Label.font = UIFont.boldSystemFontOfSize(12)
                assignedTo_Label.frame = CGRectMake(95, 60, 60, 15)
                
                
                PostStatus.font = UIFont.systemFontOfSize(12)
                PostStatus.frame  = CGRectMake(20, 60, 100, 15)
                
                watchImage.frame = CGRectMake(170, 60, 14, 14)
                
                
                
                date_Label.font = UIFont.systemFontOfSize(12)
                date_Label.frame = CGRectMake(186, 60, 70, 15)
                
            }
                
            else
            {
                assignedTo_Label.frame = CGRectMake(80, 60, 60, 15)
                
                watchImage.frame = CGRectMake(145, 60, 14, 14)
                
                date_Label.frame = CGRectMake(162, 60, 80, 15)
            }
        }
      
    }

}
