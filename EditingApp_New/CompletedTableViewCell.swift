//
//  CompletedTableViewCell.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/26/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class CompletedTableViewCell: UITableViewCell
{
        
        
        //@IBOutlet weak var img: UIButton!
        @IBOutlet weak var textView: UITextView!
        
        var cellView: UIView = UIView()
        var view: UIView = UIView()
        var colorLabel : UILabel = UILabel()
        var countLabel: UILabel = UILabel()
        var text_Label: UILabel = UILabel()
        var assignedTo_Label: UILabel = UILabel()
        var watchImage: UIImageView = UIImageView()
        var date_Label: UILabel = UILabel()
        var accessoryImage: UIButton = UIButton()
        var backBtn: UIButton = UIButton()
        var PostStatus: UILabel = UILabel()
        var completedOn:UILabel = UILabel()
    
        override func awakeFromNib()
        {
            super.awakeFromNib()
            
            contentView.addSubview(cellView)
            contentView.addSubview(backBtn)
            //contentView.addSubview(view)
            //contentView.addSubview(colorLabel)
            //contentView.addSubview(countLabel)
            view.addSubview(text_Label)
            view.addSubview(PostStatus)
            view.addSubview(assignedTo_Label)
            view.addSubview(watchImage)
            view.addSubview(date_Label)
            view.addSubview(accessoryImage)
            view.addSubview(completedOn)
            
            cellView.addSubview(colorLabel)
            cellView.addSubview(countLabel)
            cellView.addSubview(view)
            
            
            var contentDictionary = ["color": colorLabel,
                "count": countLabel,
                "status": PostStatus,
                "text": text_Label,
                "task assigned": assignedTo_Label,
                "image": watchImage,
                "date": date_Label,
                "ButtonImage": accessoryImage,
                "completedOn": completedOn]
    }
    
    

    
    
    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            
            
            if currentUserRole == "teacher"
            {
                text_Label.font = UIFont.systemFontOfSize(20)
                PostStatus.font = UIFont.systemFontOfSize(17)
                assignedTo_Label.font = UIFont.boldSystemFontOfSize(17)
                date_Label.font = UIFont.systemFontOfSize(17)
                completedOn.font = UIFont.systemFontOfSize(17)
                
                accessoryImage.frame = CGRectMake(590, 0, 55, cellView.frame.height)
                
                countLabel.frame = CGRectMake(20, 0, 40, cellView.frame.height)
                
                //assignedTo_Label.frame = CGRectMake(90, 55, 100, 25)
                
               // watchImage.frame = CGRectMake(200, 53, 26, 26)
                completedOn.frame = CGRectMake(200, 53, 120, 25)
                //date_Label.frame = CGRectMake(355, 55, 200, 25)
                
                countLabel.font = UIFont.boldSystemFontOfSize(20)
                
                
                PostStatus.frame = CGRectMake(20, 53, 60, 25)
                assignedTo_Label.frame = CGRectMake(80, 53, 100, 25)
                
                watchImage.frame = CGRectMake(320, 50, 26, 26)
                date_Label.frame = CGRectMake(350, 52, 200, 25)
                

            }
            else
            {
                text_Label.font = UIFont.systemFontOfSize(22)
                PostStatus.font = UIFont.systemFontOfSize(17)
                assignedTo_Label.font = UIFont.boldSystemFontOfSize(17)
                date_Label.font = UIFont.systemFontOfSize(17)
               
                 accessoryImage.frame = CGRectMake(590, 0, 55, cellView.frame.height)
                
                PostStatus.frame = CGRectMake(20, 53, 120, 25)
                assignedTo_Label.frame = CGRectMake(135, 53, 100, 25)
                
                watchImage.frame = CGRectMake(250, 50, 26, 26)
                date_Label.frame = CGRectMake(280, 52, 200, 25)
                

                
                countLabel.font = UIFont.boldSystemFontOfSize(20)
            }
            
            
        }
        
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                
                if currentUserRole == "teacher"
                {
                    assignedTo_Label.frame = CGRectMake(50, 60, 130, 15)
                    
                    PostStatus.frame  = CGRectMake(10, 60, 60, 15)
                    
                    watchImage.frame = CGRectMake(95, 61, 14, 14)
                    
                    completedOn.frame = CGRectMake(110, 60, 100, 15)
                    
                    date_Label.frame = CGRectMake(180, 60, 70, 15)
                }
                else
                {
                    assignedTo_Label.frame = CGRectMake(86, 60, 60, 15)
                    
                    PostStatus.frame  = CGRectMake(10, 60, 80, 15)
                    
                    watchImage.frame = CGRectMake(145, 61, 14, 14)
                    
                    date_Label.frame = CGRectMake(160, 60, 70, 15)
                }
                
                
            }
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
               if currentUserRole == "teacher"
               {
                assignedTo_Label.frame = CGRectMake(50, 60, 130, 15)
                
                PostStatus.frame  = CGRectMake(10, 60, 60, 15)
                
                watchImage.frame = CGRectMake(95, 61, 14, 14)
                
                completedOn.frame = CGRectMake(110, 60, 100, 15)
                
                date_Label.frame = CGRectMake(180, 60, 70, 15)
               }
                
               else
               {
                  assignedTo_Label.frame = CGRectMake(86, 60, 60, 15)
                
                  PostStatus.frame  = CGRectMake(10, 60, 80, 15)
                
                  watchImage.frame = CGRectMake(145, 61, 14, 14)
                
                  date_Label.frame = CGRectMake(160, 60, 70, 15)

               }
                
            }
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                
                if currentUserRole == "teacher"
                {
                    assignedTo_Label.font = UIFont.boldSystemFontOfSize(12)
                    assignedTo_Label.frame = CGRectMake(65, 60, 130, 15)
                    
                    
                    PostStatus.font = UIFont.systemFontOfSize(12)
                    PostStatus.frame  = CGRectMake(20, 60, 60, 15)
                    
                    watchImage.frame = CGRectMake(120, 60, 14, 14)
                    
                    completedOn.font = UIFont.systemFontOfSize(12)
                    completedOn.frame = CGRectMake(137, 60, 100, 15)
                    
                    date_Label.font = UIFont.systemFontOfSize(12)
                    date_Label.frame = CGRectMake(226, 60, 70, 15)
                    
                }
                else
                {
                    assignedTo_Label.font = UIFont.boldSystemFontOfSize(12)
                    assignedTo_Label.frame = CGRectMake(103, 60, 60, 15)
                    
                    
                    PostStatus.font = UIFont.systemFontOfSize(12)
                    PostStatus.frame  = CGRectMake(20, 60, 100, 15)
                    
                    watchImage.frame = CGRectMake(170, 60, 14, 14)
                    
                    
                    
                    date_Label.font = UIFont.systemFontOfSize(12)
                    date_Label.frame = CGRectMake(186, 60, 70, 15)
                    
                    
                }
                
            }
                
            else
            {
                
                if currentUserRole == "teacher"
                {
                    
                    assignedTo_Label.frame = CGRectMake(60, 60, 60, 15)
                    
                    PostStatus.frame  = CGRectMake(20, 60, 60, 15)
                    
                    watchImage.frame = CGRectMake(115, 60, 14, 14)
                    
                    completedOn.frame = CGRectMake(132, 60, 100, 15)
                    
                    date_Label.frame = CGRectMake(210, 60, 70, 15)
                    
                }
                else
                {
                    //assignedTo_Label.font = UIFont.boldSystemFontOfSize(12)
                    assignedTo_Label.frame = CGRectMake(88, 60, 60, 15)
                    
                    watchImage.frame = CGRectMake(145, 60, 14, 14)
                    
                    date_Label.frame = CGRectMake(162, 60, 80, 15)
                    
                }
                
            }
            
        }
        
        
       
    }

}
