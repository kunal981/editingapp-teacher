//
//  MyProfileViewController.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit
import MobileCoreServices



class MyProfileViewController: UIViewController,UIAlertViewDelegate,UITextViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UIPickerViewDelegate,UINavigationControllerDelegate
{

    
    var email:NSString = NSString()
    
    var subView:UIView = UIView()
    var imageData64Base: NSString = NSString()
    var imagePickerUpdate = UIImagePickerController()
    var profileDictionary: NSDictionary = NSDictionary()
    
    @IBOutlet weak var menuBarBtn: UIButton!
    @IBOutlet weak var myInfoLine: UILabel!
    @IBOutlet weak var profileLine: UILabel!
    @IBOutlet weak var memberLine: UILabel!
    @IBOutlet weak var DescriptionView: UIView!
    @IBOutlet weak var memPlanTitle: UILabel!
    @IBOutlet weak var memFeeTitle: UILabel!
    @IBOutlet weak var emailTitle: UILabel!
    @IBOutlet weak var phoneTitle: UILabel!
    @IBOutlet weak var countryTitle: UILabel!
    @IBOutlet weak var memDetailstitle: UILabel!
    @IBOutlet weak var myInfoTitle_lbl: UILabel!
    @IBOutlet weak var myDesc_title_lbl: UILabel!
    @IBOutlet weak var profileDescription_textfield: UITextField!
    @IBOutlet weak var saveBtn_lbl: UIButton!
    @IBOutlet weak var editProfileInfo_lbl: UIButton!
    //@IBOutlet weak var mem_FeeField: UITextField!
    @IBOutlet weak var email_text: UITextField!
  
    @IBOutlet weak var barButton: UIButton!
    @IBOutlet weak var changeCountry_textField: UITextField!
    
    
    @IBOutlet weak var phone_textField: UITextField!
  
    @IBOutlet weak var lastname_textField: UITextField!
    @IBOutlet weak var firstname_textField: UITextField!
    
    @IBOutlet weak var done_Button: UIButton!
    @IBOutlet weak var doneBtn_Label: UIButton!
    
    @IBOutlet weak var editBtn_label: UIButton!
    @IBOutlet weak var edit_label: UILabel!
    @IBOutlet weak var edit_image: UIImageView!
    
    @IBOutlet weak var WrapperInfo: UILabel!
   
    
    @IBOutlet weak var description_text: UITextField!
    @IBOutlet weak var profileEdit_Label: UIButton!
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var changeCountry: UILabel!
    @IBOutlet weak var memberShipFee: UILabel!
    
    @IBOutlet weak var membershipPlan: UILabel!
    @IBOutlet weak var email_Label: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var x:CGFloat = 0.0
    var navx: CGFloat = 0.0

    @IBOutlet weak var profileLabel: UILabel!
  
    @IBOutlet weak var navBar: UINavigationBar!
    
    
    //MARK: - menuBtnPressed()
    @IBAction func menuBtnPressed(sender: AnyObject)
    {
        
        if currentUserRole == "teacher"
        {
            if(x == -myMenuView.frame.size.width)
            {
                //navBar.frame.origin.x = 0
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = 0
                navx = myMenuView.frame.size.width
            }
                
            else
            {
                //navBar.frame.origin.x = myMenuView.frame.size.width
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = -myMenuView.frame.size.width
                navx = 0
            }
            
            
            UIView.animateWithDuration(0.5, animations: {
                
                self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
            })
        }
        else
        {
            if(x == -myMenuViewStudent.frame.size.width)
            {
                //navBar.frame.origin.x = 0
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = 0
                navx = myMenuViewStudent.frame.size.width
            }
                
            else
            {
                //navBar.frame.origin.x = myMenuView.frame.size.width
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = -myMenuViewStudent.frame.size.width
                navx = 0
            }
            
            
            UIView.animateWithDuration(0.5, animations: {
                
                self.navBar.frame.origin.x = self.navx
                myMenuViewStudent.frame.origin.x = self.x
                
            })
        }
        
    }
    
    //MARK: - iPadCompatibility() method
    func iPadCompatibility()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            barButton.frame = CGRectMake(0, 0, 45, 45)
            
            profileImage.frame = CGRectMake(280, 30, 200, 180)
            
            WrapperInfo.font = UIFont.systemFontOfSize(22)
            lastname_textField.font = UIFont.systemFontOfSize(22)
            firstname_textField.font = UIFont.systemFontOfSize(22)
            
            doneBtn_Label.titleLabel?.font = UIFont.systemFontOfSize(24)
            edit_label.font = UIFont.systemFontOfSize(24)
            edit_label.frame = CGRectMake(670, 210, 50, 20)
            edit_image.frame = CGRectMake(645, 210, 20, 20)
            
            editProfileInfo_lbl.titleLabel?.font = UIFont.systemFontOfSize(22)
            saveBtn_lbl.titleLabel?.font = UIFont.systemFontOfSize(22)
            
            changeCountry.font = UIFont.systemFontOfSize(18)
            changeCountry_textField.font = UIFont.systemFontOfSize(18)
            
            phone_textField.font = UIFont.systemFontOfSize(18)
            phoneNumber.font = UIFont.systemFontOfSize(18)
            
            email_Label.font = UIFont.systemFontOfSize(18)
            email_text.font = UIFont.systemFontOfSize(18)
            
            profileLabel.font = UIFont.systemFontOfSize(18)
            profileDescription_textfield.font = UIFont.systemFontOfSize(18)
            
            memberShipFee.font = UIFont.systemFontOfSize(18)
            membershipPlan.font = UIFont.systemFontOfSize(18)
            
            memPlanTitle.font = UIFont.systemFontOfSize(18)
            memFeeTitle.font = UIFont.systemFontOfSize(18)
            
            countryTitle.font = UIFont.systemFontOfSize(18)
            emailTitle.font = UIFont.systemFontOfSize(18)
            phoneTitle.font = UIFont.systemFontOfSize(18)
            
            myDesc_title_lbl.font = UIFont.systemFontOfSize(18)
            myInfoTitle_lbl.font = UIFont.systemFontOfSize(18)
            memDetailstitle.font = UIFont.systemFontOfSize(18)
            
            DescriptionView.frame = CGRectMake(25, 370, 450, 150)
            profileDescription_textfield.frame = CGRectMake(25, 30, 450, 100)
            profileLabel.frame = CGRectMake(25, 30, 450, 100)
            
            profileLine.frame = CGRectMake(25, 50, 650, 3)
            memberLine.frame = CGRectMake(25, 40, 650, 3)
            myInfoLine.frame = CGRectMake(25, 40, 650, 3)

            countryTitle.frame = CGRectMake(30, 45 , 250, 45)
            changeCountry.frame = CGRectMake(250, 45, 350, 45)
            changeCountry_textField.frame = CGRectMake(250, 45, 350, 45)
            
            //changeCountry.backgroundColor = UIColor.blackColor()
            
            emailTitle.frame = CGRectMake(30, 130, 250, 45)
            email_Label.frame = CGRectMake(250, 130, 350, 45)
            email_text.frame = CGRectMake(250, 130, 350, 45)
            
            
            phoneTitle.frame = CGRectMake(30, 90, 250, 45)
            phoneNumber.frame  = CGRectMake(250, 90, 350, 45)
            phone_textField.frame = CGRectMake(250, 90, 350, 45)
            
            
            memberShipFee.frame = CGRectMake(250, 40, 350, 45)

            membershipPlan.frame  = CGRectMake(250, 85, 350, 45)
                       
        }
        
    }
    

    
    
    //MARK: - profileEditBtn()
    @IBAction func profileEditBtn(sender: AnyObject)
    {
        firstname_textField.becomeFirstResponder()
        
        self.WrapperInfo.hidden = true
        
        self.doneBtn_Label.hidden = false
        self.edit_label.hidden = true
        self.edit_image.hidden = true
        self.firstname_textField.hidden = false
        self.lastname_textField.hidden = false
    
        
        self.firstname_textField.text = self.profileDictionary["firstname"] as! String
        self.lastname_textField.text = self.profileDictionary["lastname"] as! String
       // self.country_textField.text = self.profileDictionary["country"] as String
        //profileImage.image = self.profileDictionary["picture"] as? UIImage
        
        //self.WrapperInfo.text = "\(self.firstname_textView.text) \(self.lastname_textView.text), \(self.country_textView.text)"

//        var profileUrl: String = self.profileDictionary.valueForKey("picture") as String
//        var url: NSURL = NSURL(string: profileUrl)!
//        var data : NSData = NSData(contentsOfURL: url)!
//        self.profileImage.image = UIImage(data: data)!
//        self.profileImage.backgroundColor = UIColor.clearColor()
//        
//        imageView.image = UIImage(data: data)!

       // profileImage.image = UIImage(named: "unknown.jpg")
        
        
    }
    

    //MARK: - changeProfileImage() method
    @IBAction func changeProfileImage(sender: AnyObject)
    {
        
        println("change image button clicked")
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            
            imagePickerUpdate.delegate = self
            
            imagePickerUpdate.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            
            imagePickerUpdate.mediaTypes = [kUTTypeImage]
            
            imagePickerUpdate.allowsEditing = true
            
            self.presentViewController(imagePickerUpdate, animated: true, completion: nil)
        }
            
        else
        {
            
            var alert = UIAlertView(title: "Image not Updated", message: "Please check your Internet Settings", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
            
            let okBtn = UIAlertAction(title:"OK", style: .Default)
            {
                    (action: UIAlertAction!) -> Void in
            }
            
            
            alert.show()
            
            NSLog("failed")
        }
  
    }
    
    
    // MARK: - UIImagePicker Delegate Method
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!)
    {

        profileImage.image = image
        
        imageData64Base = convertImageToBase64(image)
        
        picker .dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        picker .dismissViewControllerAnimated(true, completion: nil)
    }

    
     func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool)
     {
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
     }
    
    
    //MARK: - convertImageToBase64()
    func convertImageToBase64(image: UIImage) -> String
    {
        var imageData:NSData = UIImagePNGRepresentation(image)
        var string : NSString = imageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.allZeros)
        string = string .stringByReplacingOccurrencesOfString("+", withString: "%2B")
        return string as String

    }
    
    


    //MARK: - updateInfo_DoneBtn()
    @IBAction func updateInfo_DoneBtn(sender: AnyObject)
    {
        self.firstname_textField.hidden = true
        self.lastname_textField.hidden = true
        
        self.WrapperInfo.hidden = false
        self.WrapperInfo.text = "\(self.firstname_textField.text) \(self.lastname_textField.text)"
        
        
        self.doneBtn_Label.hidden = true
        self.editBtn_label.hidden = false
        self.edit_image.hidden = false
        self.edit_label.hidden = false
        
        lastname_textField.resignFirstResponder()
        firstname_textField.resignFirstResponder()
        
    }
    
    
    
    //MARK: - textFieldShouldReturn()
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField .resignFirstResponder()
        return true
    }

    
    
     //MARK: - hideKeyBoardButton()
    @IBAction func hideKeyBoardButton(sender: AnyObject)
    {
        firstname_textField.resignFirstResponder()
        lastname_textField.resignFirstResponder()
    }
    
    
    
    //MARK: - activityIndicator() method
    func activityIndicator()
    {
        subView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        subView.backgroundColor = UIColor.blackColor()
        subView.alpha = 0.75
        self.view.addSubview(subView)
        
        let activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle:UIActivityIndicatorViewStyle.White)
        subView.addSubview(activityIndicator)
        var x_position = (subView.frame.width/2)-(activityIndicator.frame.width/2)
        println(x_position)
        var y_position = ((subView.frame.height/2)-(activityIndicator.frame.height/2))
        println(y_position)
        
        activityIndicator.frame = CGRectMake(x_position, y_position, activityIndicator.frame.width,activityIndicator.frame.height)
        
        activityIndicator.startAnimating()
    }

    
    
    //MARK: - viewDidLoad() method
    override func viewDidLoad()
    {
        
        
        super.viewDidLoad()
        
        firstname_textField.delegate = self
        lastname_textField.delegate = self
        //country_textField.delegate = self
        changeCountry_textField.delegate=self
        phone_textField.delegate=self
        email_text.delegate=self
        //mem_FeeField.delegate=self
       // mem_PlanField.delegate=self
        description_text.delegate=self
        
        //self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width/2
        self.profileImage.layer.borderWidth = 4
        self.profileImage.layer.borderColor = UIColor(red: 44/255, green: 241/255, blue:285/255, alpha: 0.9).CGColor
        self.profileImage.layer.masksToBounds = true
       
        //STATUS BAR COLOR
        let statusBarView:UIView = UIView()
        statusBarView.frame = CGRectMake(0, 0, self.view.frame.width, 20)
        statusBarView.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 0.9)
        self.view.addSubview(statusBarView)
        
        navBar.barTintColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 0.85)
            
        profileView.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 0.9)
        
        
        profileEdit_Label.layer.borderWidth = 2
        profileEdit_Label.layer.masksToBounds = true
        profileEdit_Label.layer.borderColor = UIColor.whiteColor().CGColor
        
        
        
        // x = -320
        currentVC = self
        if currentUserRole == "teacher"
        {
            self.view.addSubview(myMenuView)
            
            myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
            x = -myMenuView.frame.size.width
        }
        else
        {
            self.view.addSubview(myMenuViewStudent)
            
            myMenuViewStudent.frame = CGRectMake(-myMenuViewStudent.frame.size.width, self.view.frame.origin.y,myMenuViewStudent.frame.size.width ,myMenuViewStudent.frame.size.height)
            x = -myMenuViewStudent.frame.size.width
        }
        
       // profileLabel.numberOfLines = 3
        profileLabel.font = UIFont.systemFontOfSize(13)
        
        iPadCompatibility()
        
        activityIndicator()         //calling activity indicator
        subView.hidden = false
        getProfileInfo()        //calling API for Inforamtion getting
        
       // profileView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 5

        
 }
    
    
    
    
     //MARK: - getProfileInfo() API
    func getProfileInfo()
    {
        
        if currentUserRole == "teacher"
        {
            subView.hidden = false
            
            var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=viewprofile&uid=\(user_id)"
            
            println(urlPath)
            
            let url: NSURL = NSURL(string : urlPath)!
            
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
                
                
                
                if error != nil
                {
                    self.subView.hidden = true
                    println(error.localizedDescription)
                }
                
                println(data)
                
                var err: NSError?
                
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    self.subView.hidden = true
                    
                    var resultDict:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
                    
                    if err != nil
                    {
                        println("JSON Error \(err!.localizedDescription)")
                    }
                    
                    println("Result Dictionary View Profile = \(resultDict)")
                    
                    var status = resultDict.valueForKey("status") as! String
                    println(status)
                    
                    
                    if(status == "Success")
                    {
                        
                        self.profileDictionary  = resultDict["post"] as! NSDictionary
                        println("POST values in a Dictionary are = \(self.profileDictionary)")
                        
                    }
                    
                    
                    // TO GET THE PROFILE PICTURE
                    var profileUrl: String = self.profileDictionary.valueForKey("picture")as! String
                    
                    var url: NSURL = NSURL(string: profileUrl)!
                    
                    if let data : NSData = NSData(contentsOfURL: url)
                    {
                        self.profileImage.image = UIImage(data: data)!
                        self.profileImage.backgroundColor = UIColor.clearColor()
                        
                        imageView.image = UIImage(data: data)!
                    }
                    
                    
                    
                    var firstName_update: String = self.profileDictionary.valueForKey("firstname")as! String
                    println("first name updated: \(firstName_update)")
                    
                    var lastName_update: String = self.profileDictionary.valueForKey("lastname")as! String
                    println("last name updated: \(lastName_update)")
                    
                    firstname_menu = "\(firstName_update)"
                    lastname_menu = "\(lastName_update)"
                    
                    nameLabel.text = "\(firstName_update) \(lastName_update)"
                    println(nameLabel)
                    
                    var firstname:String = self.profileDictionary["firstname"]as! String
                    var lastname:String = self.profileDictionary["lastname"] as! String
                    
                    
                    self.changeCountry.text = self.profileDictionary["country"] as? String
                    
                    self.WrapperInfo.text = "\(firstname) \(lastname)"
                    
                    self.phoneNumber.text = self.profileDictionary["phone"] as? String
                    
                    self.email_Label.text = self.profileDictionary["email"] as? String
                    
                    self.profileLabel.text = self.profileDictionary["description"] as? String
                    
                    self.memberShipFee.text = self.profileDictionary["plan-price"] as? String
                    
                    self.membershipPlan.text = self.profileDictionary["membership-plan"] as? String
                    
                    
                    
                    //for updations
                    self.changeCountry_textField.text = self.profileDictionary["country"] as? String
                    
                    self.phone_textField.text = self.profileDictionary["phone"] as? String
                    
                    self.email_text.text = self.profileDictionary["email"] as? String
                    
                    self.description_text.text = self.profileDictionary["description"] as? String
                    
                    //  self.mem_FeeField.text = self.profileDictionary["plan-price"] as? String
                    
                    // self.mem_PlanField.text = self.profileDictionary["membership-plan"] as? String
                    
                    self.firstname_textField.text = self.profileDictionary["firstname"] as? String
                    self.lastname_textField.text = self.profileDictionary["lastname"] as? String
                    
                    
                    
                    println(self.profileDictionary["membership-plan"])
                    var value = self.profileDictionary["membership-plan"] as? String
                    println(value)
                    
                    
                    
                    if self.membershipPlan.text == nil
                    {
                        self.membershipPlan.text = "No membership-plan"
                    }
                    else
                    {
                        self.membershipPlan.text = self.profileDictionary["membership-plan"] as? String
                    }
                    
                    
                    if self.memberShipFee.text == nil
                    {
                        self.memberShipFee.text = "No membership fee"
                    }
                    else
                    {
                        self.memberShipFee.text = self.profileDictionary["plan-price"] as? String
                    }
                    
                    
                    
                })
                
            })
            
            task.resume()
            

            
        }
        else
        {
            
            subView.hidden = false
            
            var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=viewprofile&uid=\(user_id)"
            
            println(urlPath)
            
            let url: NSURL = NSURL(string : urlPath)!
            
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
                
                
                
                if error != nil
                {
                    self.subView.hidden = true
                    println(error.localizedDescription)
                }
                
                println(data)
                
                var err: NSError?
                
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    self.subView.hidden = true
                    
                    var resultDict:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
                    
                    if err != nil
                    {
                        println("JSON Error \(err!.localizedDescription)")
                    }
                    
                    println("Result Dictionary View Profile = \(resultDict)")
                    
                    var status = resultDict.valueForKey("status") as! String
                    println(status)
                    
                    
                    if(status == "Success")
                    {
                        //println("Login SUCCESS")
                        
                        self.profileDictionary  = resultDict["post"] as! NSDictionary
                        println("POST values in a Dictionary are = \(self.profileDictionary)")
                        
                    }
                    
                    
                    // TO GET THE PROFILE PICTURE
                    var profileUrl: String = self.profileDictionary.valueForKey("picture")as! String
                    
                    var url: NSURL = NSURL(string: profileUrl)!
                    
                    if let data : NSData = NSData(contentsOfURL: url)
                    {
                        self.profileImage.image = UIImage(data: data)!
                        self.profileImage.backgroundColor = UIColor.clearColor()
                        
                        imageView_Student.image = UIImage(data: data)!
                    }
                    
                    
                    
                    var firstName_update: String = self.profileDictionary.valueForKey("firstname")as! String
                    println("first name updated: \(firstName_update)")
                    
                    var lastName_update: String = self.profileDictionary.valueForKey("lastname")as! String
                    println("last name updated: \(lastName_update)")
                    
                    firstname_menu = "\(firstName_update)"
                    lastname_menu = "\(lastName_update)"
                    
                    nameLabel_Student.text = "\(firstName_update) \(lastName_update)"
                    println(nameLabel_Student)
                    
                    
                    
                    //                var firstname:String = self.profileDictionary["firstname"]as! String
                    //                var lastname:String = self.profileDictionary["lastname"] as! String
                    //                // var country:String = self.profileDictionary["country"] as! String
                    
                    self.firstname_textField.text = self.profileDictionary["firstname"] as! String
                    
                    self.lastname_textField.text = self.profileDictionary["lastname"] as! String
                    
                    
                    
                    self.changeCountry.text = self.profileDictionary["country"] as? String
                    
                    self.WrapperInfo.text = "\(self.firstname_textField.text) \(self.lastname_textField.text)"
                    
                    self.phoneNumber.text = self.profileDictionary["phone"] as? String
                    
                    self.email_Label.text = self.profileDictionary["email"] as? String
                    
                    self.profileLabel.text = self.profileDictionary["description"] as? String
                    
                    self.memberShipFee.text = self.profileDictionary["plan-price"] as? String
                    
                    self.membershipPlan.text = self.profileDictionary["membership-plan"] as? String
                    
                    
                    
                    //for updations
                    self.changeCountry_textField.text = self.profileDictionary["country"] as? String
                    
                    self.phone_textField.text = self.profileDictionary["phone"] as? String
                    
                    self.email_text.text = self.profileDictionary["email"] as? String
                    
                    self.description_text.text = self.profileDictionary["description"] as? String
                    
                    //  self.mem_FeeField.text = self.profileDictionary["plan-price"] as? String
                    
                    // self.mem_PlanField.text = self.profileDictionary["membership-plan"] as? String
                    
                    self.firstname_textField.text = self.profileDictionary["firstname"] as? String
                    self.lastname_textField.text = self.profileDictionary["lastname"] as? String
                    // self.country_textField.text = self.profileDictionary["country"] as String
                    
                    
                    println(self.profileDictionary["membership-plan"])
                    var value = self.profileDictionary["membership-plan"] as? String
                    println(value)
                    
                    
                    
                    if self.membershipPlan.text == nil
                    {
                        self.membershipPlan.text = "No membership-plan"
                    }
                    else
                    {
                        self.membershipPlan.text = self.profileDictionary["membership-plan"] as? String
                    }
                    
                    
                    if self.memberShipFee.text == nil
                    {
                        self.memberShipFee.text = "No membership fee"
                    }
                    else
                    {
                        self.memberShipFee.text = self.profileDictionary["plan-price"] as? String
                    }
                    
                    
                    
                })
                
            })
            
            task.resume()
            

            
        }
        

        

    }
    
    //MARK: - edit_button() method
    @IBAction func Edit_Button(sender: AnyObject)
    {
        
        phoneNumber.hidden = true
        changeCountry.hidden = true
        email_Label.hidden = true
        
        phone_textField.hidden = false
        email_text.hidden = false
        changeCountry_textField.hidden = false
        
        profileLabel.hidden = true
        description_text.hidden = false
        
        description_text.becomeFirstResponder()

    }
    
    
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        
        println("editing starts")
        
        if textField.tag == 22
        {
           self.view.frame.origin.y -= 200
        }
        else
        {
             self.view.frame.origin.y = 0
        }
        
        
        return true
    }
    
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    {
        println("end editing")
        
        if textField.tag == 22
        {
            self.view.frame.origin.y = 0
        }
        
       return true
    }
    

   
    
    //MARK: - SaveProfile_btn() method
    @IBAction func SaveProfile_btn(sender: AnyObject)
    {
        
        if ((firstname_textField.text == "") || (lastname_textField.text == "") || (phone_textField.text == "") || (email_text.text == "") || (changeCountry_textField.text == "") || (description_text.text == ""))
        {
            
            subView.hidden = false
            
            var loginAlert = UIAlertController(title: "", message: "Please Enter the Fields", preferredStyle: UIAlertControllerStyle.Alert)
            
            loginAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                self.subView.hidden = true
            }))
            self.presentViewController(loginAlert, animated: true, completion: nil)
            
            
        }
        else if !isValidEmail(email_text.text)

        {
            subView.hidden = false
            var alert = UIAlertView(title: "", message: "Enter valid Email", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
            
            let okBtn = UIAlertAction(title:"OK", style: .Default)
            {
                    (action: UIAlertAction!) -> Void in
            }
            
            
            alert.show()
            
            
            println("email is not valid")
        }
        else
        {
            updateProfile()         //calling update Profile API
        }
    
    }
    
    //MARK: - updateProfile()
    func updateProfile()
    {
        
        subView.hidden = false
        
        //UPDATE PROFILE api
        
        var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=profileupdate&uid=\(user_id)"
        
        println(imageData64Base)
        
        var post:NSString = "firstname=\(firstname_textField.text)&lastname=\(lastname_textField.text)&password=\(passwordString)&phone=\(phone_textField.text)&desc=\(description_text.text)&country=\(changeCountry_textField.text)&picture=\(imageData64Base)"
        
        println("password is = \(passwordString)")
        
        NSLog("PostData: %@",post);
        
        var url1:NSURL = NSURL(string: urlPath)!
        
        var postData:NSData = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength:NSString = String( postData.length )
        
        var request:NSMutableURLRequest = NSMutableURLRequest(URL: url1)
        request.HTTPMethod = "POST"
        request.HTTPBody = postData
        request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        
        var reponseError: NSError?
        var response: NSURLResponse?
        
        var urlData: NSData? = NSURLConnection.sendSynchronousRequest(request, returningResponse:&response, error:&reponseError)
        
        if (urlData != nil)
        {
            
            let res = response as! NSHTTPURLResponse!;
            
            NSLog("Response code: %ld", res.statusCode);
            
            if (res.statusCode >= 200 && res.statusCode < 300)
            {
                var responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                
                NSLog("Response ==> %@", responseData);
                
                var error: NSError?
                
                var updateProfile = NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers , error: &error) as! NSDictionary
                
                
                var success:String = updateProfile.valueForKey("status") as! String
                
                
                println(success)
                
                
                if(success == "Success")
                {
                    
                    /*if (profileImage.image != nil)
                    {
                    var imgUrl : NSString = updateProfile.valueForKey("picture") as NSString
                    
                    println(imgUrl)
                    
                    var url: NSURL = NSURL(string: imgUrl)!
                    
                    var imgData: NSData = NSData(contentsOfURL: url)!
                    
                    self.profileImage.image = UIImage(data: imgData)!
                    self.profileImage.backgroundColor = UIColor.clearColor()
                    
                    imageView.image = UIImage(data: imgData)!
                    
                    }
                    else
                    {
                    
                    var alert = UIAlertView(title: "Error Occured", message: "Image Not Picked from Gallery", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                    {
                    (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    alert.show()
                    
                    }*/
                    
                    
                    self.WrapperInfo.text = "\(self.firstname_textField.text) \(self.lastname_textField.text)"
                    
                    self.profileLabel.text = "\(description_text.text)"
                    
                    self.changeCountry.text = "\(changeCountry_textField.text)"
                    
                    self.phoneNumber.text = "\(phone_textField.text)"
                    
                    self.email_Label.text = "\(email_text.text)"
                    
                    
            
                    
                    
                    var alert = UIAlertView(title: "", message: "Profile Updated Successfully", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                    {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    alert.show()
                    
                    
                    println("UPDATE SUCCESSFUL")
                    
                }
                else
                {
                    
                    subView.hidden = false
                    
                    var alert = UIAlertView(title: "", message: "Profile not updated Successfully", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                        {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    alert.show()
                    
                }
                
                
                
            }
            else
            {
                
                println("Login failed2")
                
            }
        }
        else
        {
            
            subView.hidden = false
            
            var alert = UIAlertView(title: "Internet Error..!", message: "Please check your Internet Settings", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
            
            let okBtn = UIAlertAction(title:"OK", style: .Default)
                {
                    (action: UIAlertAction!) -> Void in
            }
            
            
            alert.show()
            
        }
        
        
        }
        
        
        

    
    
    
    
    //MARK: - isValidEmail()
    func isValidEmail(emailid: NSString)->Bool
    {
        
        var isValid = true
        
        if !emailid.containsString(" ")
        {
            var atRateSplitArray = emailid.componentsSeparatedByString("@")
            
            if(atRateSplitArray.count>=2)
            {
                for component in atRateSplitArray
                {
                    if component as! String == ""
                    {
                        isValid = false
                    }
                }
                
                if(isValid)
                {
                    var dotSplitArray = atRateSplitArray[atRateSplitArray.count-1].componentsSeparatedByString(".")
                    
                    if(dotSplitArray.count>=2)
                    {
                        for component in dotSplitArray
                        {
                            if component as! String == ""
                            {
                                isValid = false
                            }
                        }
                    }
                    else
                    {
                        isValid = false
                        
                    }
                }
            }
            else
            {
                isValid = false
                
            }
            
        }
        else
        {
            isValid = false
            
        }
        
        
        
        return isValid
    }
    

    
    
    //MARK: - alertView() method
    func alertView(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int)
    {
        if (buttonIndex == 0)
        {
            println("Ok button clicked")
            
            subView.hidden = true
            
        }
     }


    //MARK: - didReceiveMemoryWarning() method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
