//
//  MyMenuTableViewCell.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class InvitationsTableViewCell: UITableViewCell
{

    let padding:CGFloat = 5
    //@IBOutlet weak var img: UIButton!
    @IBOutlet weak var textView: UITextView!

    var cellView: UIView = UIView()
    var view: UIView = UIView()
    var colorLabel : UILabel = UILabel()
    var countLabel: UILabel = UILabel()
    var text_Label: UILabel = UILabel()
    var assignedTo_Label: UILabel = UILabel()
    var watchImage: UIImageView = UIImageView()
    var date_Label: UILabel = UILabel()
    var accessoryImage: UIButton = UIButton()
    var backBtn: UIButton = UIButton()
    var PostStatus: UILabel = UILabel()
    
    
    
    var acceptButton: UIButton = UIButton()
    var rejectButton: UIButton = UIButton()
    


    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        contentView.addSubview(cellView)
        contentView.addSubview(backBtn)
        //contentView.addSubview(view)
        //contentView.addSubview(colorLabel)
        //contentView.addSubview(countLabel)
        view.addSubview(text_Label)
        view.addSubview(PostStatus)
        view.addSubview(assignedTo_Label)
        view.addSubview(watchImage)
        view.addSubview(date_Label)
        view.addSubview(accessoryImage)
        
        cellView.addSubview(colorLabel)
        cellView.addSubview(countLabel)
        cellView.addSubview(view)
        
        cellView.addSubview(acceptButton)
        
        cellView.addSubview(rejectButton)
        
       

                
        var contentDictionary = ["color": colorLabel,
                                 "count": countLabel,
                                 "status": PostStatus,
                                 "text": text_Label,
                                 "task assigned": assignedTo_Label,
                                 "image": watchImage,
                                 "date": date_Label,
                                  "ButtonImage": accessoryImage,
                                "acceptButton": acceptButton,
                                "rejectButton": rejectButton]
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            text_Label.font = UIFont.systemFontOfSize(20)
            PostStatus.font = UIFont.systemFontOfSize(18)
            assignedTo_Label.font = UIFont.boldSystemFontOfSize(18)
            date_Label.font = UIFont.systemFontOfSize(18)

            countLabel.frame = CGRectMake(20, 0, 40, cellView.frame.height)

            
            acceptButton.frame = CGRectMake(555, 5, 155, 32)
            acceptButton.imageEdgeInsets = UIEdgeInsetsMake(0,-2,0,5)
            acceptButton.titleEdgeInsets = UIEdgeInsetsMake(0,5,0,0)
            
            
            rejectButton.frame = CGRectMake(555, 42, 155, 32)
            rejectButton.imageEdgeInsets = UIEdgeInsetsMake(2, -5, 0, 5)
            rejectButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
            
            PostStatus.frame = CGRectMake(20, 53, 70, 25)
            assignedTo_Label.frame = CGRectMake(80, 53, 100, 25)
            
            watchImage.frame = CGRectMake(200, 50, 26, 26)
            date_Label.frame = CGRectMake(230, 52, 200, 25)
            

            
             acceptButton.titleLabel?.font = UIFont.systemFontOfSize(18)
             rejectButton.titleLabel?.font = UIFont.systemFontOfSize(18)
            
            countLabel.font = UIFont.boldSystemFontOfSize(20)
            
        }
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                assignedTo_Label.frame = CGRectMake(50, 60, 130, 15)
                
                PostStatus.frame  = CGRectMake(10, 60, 60, 15)
                
                watchImage.frame = CGRectMake(95, 61, 14, 14)
                
                date_Label.frame = CGRectMake(110, 60, 70, 15)
            }
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                assignedTo_Label.frame = CGRectMake(50, 60, 130, 15)
                
                PostStatus.frame  = CGRectMake(10, 60, 60, 15)
                
                watchImage.frame = CGRectMake(95, 61, 14, 14)
                
                date_Label.frame = CGRectMake(110, 60, 70, 15)

            }
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                
                assignedTo_Label.font = UIFont.boldSystemFontOfSize(12)
                assignedTo_Label.frame = CGRectMake(65, 60, 130, 15)
                
                PostStatus.font = UIFont.systemFontOfSize(12)
                PostStatus.frame  = CGRectMake(20, 60, 60, 15)
                
                watchImage.frame = CGRectMake(120, 60, 14, 14)
                
                date_Label.font = UIFont.systemFontOfSize(12)
                date_Label.frame = CGRectMake(137, 60, 70, 15)
                
            }
                
            else
            {
                assignedTo_Label.frame = CGRectMake(55, 60, 60, 15)
                
                watchImage.frame = CGRectMake(120, 60, 14, 14)
                
                date_Label.frame = CGRectMake(137, 60, 80, 15)
            }
        }
        
        
       
        
    }
    
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
    }

}
