//
//  MyPlanPREMIERViewController.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class MyPlanPREMIERViewController: UIViewController
{

    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    
    @IBOutlet weak var navBar: UINavigationBar!
       
    var x:CGFloat = 0.0
    var navx:CGFloat = 0.0

    @IBOutlet weak var textViewField: UIView!
    
    @IBOutlet weak var barButton: UIButton!
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var premierPlanView: UIView!
    @IBOutlet weak var securityView: UIView!
    @IBOutlet weak var fullEmailView: UIView!
    @IBOutlet weak var mobileAppView: UIView!
    @IBOutlet weak var priorityView: UIView!

    @IBOutlet weak var getItNow_btn: UIButton!
    

    
    
    
    @IBOutlet weak var mostPopularLBL: UILabel!
    @IBOutlet weak var premier_lbl: UILabel!
    @IBOutlet weak var priorityImage: UIButton!
    @IBOutlet weak var unlimitedStorageImage: UIButton!
    
    @IBOutlet weak var mobileAppImage: UIButton!
    
    @IBOutlet weak var fullEmailSupport_Image: UIButton!
    @IBOutlet weak var unlimitedImage: UIButton!
    
    @IBOutlet weak var enhnacedSecurity_Image: UIButton!
    @IBOutlet weak var privateURL_Image: UIButton!
    
    

    
    @IBOutlet weak var enhancedSecurity_Lbl: UILabel!
    @IBOutlet weak var privateURL_Lbl: UILabel!
    @IBOutlet weak var unlimited_Lbl: UILabel!
    @IBOutlet weak var prioritySupport_Lbl: UILabel!
    @IBOutlet weak var requests_Lbl: UILabel!
    
    @IBOutlet weak var darkBlue_Lbl: UILabel!
    @IBOutlet weak var fullEmail_Lbl: UILabel!
    @IBOutlet weak var mobileApp_Lbl: UILabel!
    @IBOutlet weak var unlimitedStorage_Lbl: UILabel!

    
    
    //MARK: - backBtnPressed() method
    @IBAction func backBtnPressed(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true) 
    }
    
    
    
    
    //MARK: - premierPlanBtn() method
    @IBAction func premierPlanBtn(sender: AnyObject)
    {
        
    }
    
    
    
    //MARK: - iPadCompatibility() method
    func iPadCompatibility()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            barButton.frame = CGRectMake(0, 0, 40, 40)
            
            darkBlue_Lbl.frame = CGRectMake(0, 120, 720, 55)
            darkBlue_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 24)
            
            
            premier_lbl.frame = CGRectMake(210, 25, 300, 30)
            mostPopularLBL.frame = CGRectMake(265, 65, 200, 35)
            

            
            premier_lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 40)
            mostPopularLBL.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 32)
            getItNow_btn.titleLabel?.font = UIFont.systemFontOfSize(24)
            
            prioritySupport_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            unlimitedStorage_Lbl.font = UIFont(name: "Helvetica Neue-CondensedBold", size: 20)
            mobileApp_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            unlimited_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            fullEmail_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            privateURL_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            enhancedSecurity_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            requests_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            unlimitedStorage_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            
            
            
            priorityImage.frame = CGRectMake(20, 10, 30, 30)
            unlimitedStorageImage.frame = CGRectMake(20, 10, 30, 30)
            mobileAppImage.frame = CGRectMake(20, 10, 30, 30)
            unlimitedImage.frame = CGRectMake(20, 10, 30, 30)
            fullEmailSupport_Image.frame = CGRectMake(20, 10, 30, 30)
            privateURL_Image.frame = CGRectMake(20, 10, 30, 30)
            enhnacedSecurity_Image.frame = CGRectMake(20, 10, 30, 30)

            
            wrapperView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 20
            
            premierPlanView.frame.origin.y = wrapperView.frame.origin.y + wrapperView.frame.height
            
            getItNow_btn.frame.origin.y = premierPlanView.frame.origin.y + premierPlanView.frame.height
            
        }
        else
        {
            wrapperView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 10
            
            premierPlanView.frame.origin.y = wrapperView.frame.origin.y + wrapperView.frame.height
            
            getItNow_btn.frame.origin.y = premierPlanView.frame.origin.y + premierPlanView.frame.height
        }
    }

    
    //MARK: - viewDidLoad() method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //STATUS BAR COLOR
        let statusBarView:UIView = UIView()
        statusBarView.frame = CGRectMake(0, 0, self.view.frame.width, 20)
        statusBarView.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 0.85)
        self.view.addSubview(statusBarView)
        
        navBar.barTintColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1)
        
        iPadCompatibility()
   
        
        textViewField.layer.borderWidth = 1.0
        textViewField.layer.masksToBounds = true
        textViewField.layer.borderColor = UIColor.whiteColor().CGColor
        
        
        
        priorityView.layer.borderWidth = 0.5
        priorityView.layer.borderColor =  UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1).CGColor
        priorityView.layer.masksToBounds = true
        
        fullEmailView.layer.borderWidth = 0.5
        fullEmailView.layer.borderColor =  UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1).CGColor
        fullEmailView.layer.masksToBounds = true
        
        mobileAppView.layer.borderWidth = 0.5
        mobileAppView.layer.borderColor =  UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1).CGColor
        mobileAppView.layer.masksToBounds = true
        
        
        securityView.layer.borderWidth = 0.5
        securityView.layer.borderColor =  UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1).CGColor
        securityView.layer.masksToBounds = true

}
    
    
    
    //MARK: - didReceiveMemoryWarning() method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
