//
//  CompletedEditViewController.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/26/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


var completedView = NSDictionary()
var completeDict = NSDictionary()



class CompletedEditViewController: UIViewController,UITextViewDelegate,UIAlertViewDelegate
{

    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var watchIcon: UIImageView!
    @IBOutlet weak var barButton: UIButton!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var mySubView: UIView!
    
    @IBOutlet weak var myEssayTextView: UITextView!
    
    @IBOutlet weak var count_Label: UILabel!
    @IBOutlet weak var assignTo_label: UILabel!
    @IBOutlet weak var date_Label: UILabel!
    
    var dynamicView:UIView = UIView()
    
    
    //MARK: - back button
    @IBAction func backBtnPressed(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    
    
    
    //MARK: - viewDidLoad()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //STATUS BAR COLOR
        let statusBarView:UIView = UIView()
        statusBarView.frame = CGRectMake(0, 0, self.view.frame.width, 20)
        statusBarView.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 0.85)
        self.view.addSubview(statusBarView)
        
        navBar.barTintColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1)

        
        
        mySubView.layer.borderColor = UIColor.whiteColor().CGColor
        mySubView.layer.borderWidth = 2
        mySubView.layer.masksToBounds = true
        
        iPadCompatibility()
        
        createActivityIndicator()           //calling activity indicator
        completedViewEssay()                //calling completedEssay() api
        
        
    }
    
    
    
    
    //MARK: - iPadCompatibility() method
    func iPadCompatibility()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            barButton.frame = CGRectMake(0, 0, 40, 40)
            
            myEssayTextView.font = UIFont.systemFontOfSize(18)
            
            myEssayTextView.editable = true
            myEssayTextView.font = myEssayTextView.font
            myEssayTextView.editable = false
            
            count_Label.frame = CGRectMake(0, 0, 65, 60)
            count_Label.font = UIFont.boldSystemFontOfSize(24)

            
            watchIcon.frame = CGRectMake(500, 24, 32, 32)
            
            date_Label.font = UIFont.systemFontOfSize(22)
            
            assignTo_label.font = UIFont.boldSystemFontOfSize(24)
            assignTo_label.frame = CGRectMake(75, 22, 300, 27)
            
            mySubView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 25

            
        }
        else
        {
            mySubView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 10
            
            
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                
                
            }
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                
            }
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                barButton.frame = CGRectMake(0, 0, 35, 35)
            }
                
            else
            {
                
            }

        }
        
    }

    
    //MARK: - activityIndicator()
    func createActivityIndicator()
    {
        dynamicView.frame = CGRectMake(0,0,self.view.frame.width,self.view.frame.height)
        dynamicView.backgroundColor = UIColor.blackColor()
        dynamicView.alpha = 0.75
        
        
        //CREATING ACTIVITY INDICATOR
        var activityIndicator: UIActivityIndicatorView
        = UIActivityIndicatorView(activityIndicatorStyle:UIActivityIndicatorViewStyle.White)
        
        var xx = (dynamicView.frame.width/2) - (activityIndicator.frame.width/2)
        var y = (dynamicView.frame.height/2) - (activityIndicator.frame.height/2)
        activityIndicator.frame = CGRectMake(xx, y, activityIndicator.frame.width, activityIndicator.frame.height)
        
        dynamicView.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        
        self.view.addSubview(dynamicView)
    }
    
    

    
    //MARK: - completedViewEssay() API
    func completedViewEssay()
    {
        
        
        if currentUserRole == "teacher"
        {
            post_id = completedResult["post-id"] as! Int
            println(post_id)
            
            dynamicView.hidden = false
            
            // View Essay api
            var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=viewacceptedessay"
            
            var post:NSString = "uid=\(user_id)&post-id=\(post_id)"
            
            NSLog("PostData: %@",post);
            
            var url1:NSURL = NSURL(string: urlPath)!
            
            var postData:NSData = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            var postLength:NSString = String( postData.length )
            
            var request:NSMutableURLRequest = NSMutableURLRequest(URL: url1)
            request.HTTPMethod = "POST"
            request.HTTPBody = postData
            request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            
            
            var reponseError: NSError?
            var response: NSURLResponse?
            
            var urlData: NSData? = NSURLConnection.sendSynchronousRequest(request, returningResponse:&response, error:&reponseError)
            
            if ( urlData != nil ) {
                let res = response as! NSHTTPURLResponse!;
                
                NSLog("Response code: %ld", res.statusCode);
                
                if (res.statusCode >= 200 && res.statusCode < 300)
                {
                    var responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                    
                    NSLog("Response ==> %@", responseData);
                    
                    var error: NSError?
                    
                    dynamicView.hidden = true
                    
                    completeDict = NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers , error: &error) as! NSDictionary
                    
                    
                    var success:String = completeDict.valueForKey("status") as! String
                    
                    
                    println(success)
                    
                    if(success == "Success")
                    {
                        println("Login SUCCESS")
                        
                        postValuesDictionary  = completeDict["post"] as! NSDictionary
                        println(postValuesDictionary)
                        
                        
                    }
                    else
                    {
                        
                        var alert = UIAlertView(title: "Error..!", message: "Data not Found", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                        
                        let okBtn = UIAlertAction(title:"OK", style: .Default)
                            {
                                (action: UIAlertAction!) -> Void in
                        }
                        
                        
                        alert.show()
                        
                        println("Login failed1")
                        
                        dynamicView.hidden = true
                    }
                    
                }
                else
                {
                    
                    println("Login failed2")
                    
                }
            }
            else
            {
                var alert = UIAlertView(title: "Internet Error..!", message: "Please check your Internet Connection", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                
                let okBtn = UIAlertAction(title:"OK", style: .Default)
                    {
                        (action: UIAlertAction!) -> Void in
                }
                
                
                alert.show()
                
                
                println("Login failed3")
                
                dynamicView.hidden = true
            }
            
            
            println(completeDict)
            
            println(completedResult)
            println(completedCountNumber)
            
            
            myEssayTextView.text = postValuesDictionary["content"] as! String
            assignTo_label.text = postValuesDictionary["title"] as? String
            date_Label.text = postValuesDictionary["postdate"] as? String
            
            
            count_Label.text = "\(completedCountNumber)"
            
        }
        else
        {
            dynamicView.hidden = false
            
            post_id = completedResult["post-id"] as! Int
            println(post_id)
            
            
            // View Essay api
            
            var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=viewessay"
            
            var post:NSString = "uid=\(user_id)&post-id=\(post_id)"
            
            NSLog("PostData: %@",post);
            
            var url1:NSURL = NSURL(string: urlPath)!
            
            var postData:NSData = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            var postLength:NSString = String( postData.length )
            
            var request:NSMutableURLRequest = NSMutableURLRequest(URL: url1)
            request.HTTPMethod = "POST"
            request.HTTPBody = postData
            request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            
            
            var reponseError: NSError?
            var response: NSURLResponse?
            
            var urlData: NSData? = NSURLConnection.sendSynchronousRequest(request, returningResponse:&response, error:&reponseError)
            
            if ( urlData != nil ) {
                let res = response as! NSHTTPURLResponse!;
                
                NSLog("Response code: %ld", res.statusCode);
                
                if (res.statusCode >= 200 && res.statusCode < 300)
                {
                    var responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                    
                    NSLog("Response ==> %@", responseData);
                    
                    var error: NSError?
                    
                    completeDict = NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers , error: &error) as! NSDictionary
                    
                    dynamicView.hidden = true
                    
                    var success:String = completeDict.valueForKey("status") as! String
                    
                    
                    println(success)
                    
                    if(success == "Success")
                    {
                        println("Login SUCCESS")
                        
                        postValuesDictionary  = completeDict["post"] as! NSDictionary
                        println(postValuesDictionary)
                        
                        
                    }
                    else
                    {
                        
                        var alert = UIAlertView(title: "Error..!", message: "Data not Found", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                        
                        let okBtn = UIAlertAction(title:"OK", style: .Default)
                            {
                                (action: UIAlertAction!) -> Void in
                        }
                        
                        
                        alert.show()
                        
                        println("Login failed1")
                        
                        dynamicView.hidden = true
                    }
                    
                }
                else
                {
                    
                    println("Login failed2")
                    
                }
            }
            else
            {
                var alert = UIAlertView(title: "Internet Error..!", message: "Please check your Internet Connection", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                
                let okBtn = UIAlertAction(title:"OK", style: .Default)
                    {
                        (action: UIAlertAction!) -> Void in
                }
                
                
                alert.show()
                
                
                println("Login failed3")
                
                dynamicView.hidden = true
            }
            
            
            println(completeDict)
            
            println(completedResult)
            println(completedCountNumber)
            
            
            myEssayTextView.text = postValuesDictionary["content"] as! String
            assignTo_label.text = postValuesDictionary["title"] as? String
            date_Label.text = postValuesDictionary["postdate"] as? String
            
            
            count_Label.text = "\(completedCountNumber)"
            
            
        }

    }
    
    //MARK: - textfield method
    func textViewShouldReturn(textView: UITextView!) -> Bool
    {
        
        textView.resignFirstResponder()
        return true
    }
    
    
    
    
    //MARK: - hideKeyBoardButton()
    @IBAction func hideKeyBoardButton(sender: AnyObject)
    {
        myEssayTextView.resignFirstResponder()
    }
    
    
    
    //MARK: - didReceiveMemoryWarning() method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
