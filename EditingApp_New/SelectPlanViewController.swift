//
//  SelectPlanViewController.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class SelectPlanViewController: UIViewController
{

    
      let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
      var navx: CGFloat = 0.0
      var x:CGFloat = 0.0
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var menuBarBtn: UIButton!
   
    @IBOutlet weak var professionalView: UIView!
    @IBOutlet weak var basicView: UIView!

    @IBOutlet weak var premierView: UIView!
  
    @IBOutlet weak var basic_lbl: UILabel!
    @IBOutlet weak var professional_lbl: UILabel!
    
    @IBOutlet weak var premier_lbl: UILabel!
    @IBOutlet weak var lessThan_lbl: UILabel!
    @IBOutlet weak var mostPopular_lbl: UILabel!
    
    @IBOutlet weak var fourtyNine_lbl: UILabel!
    @IBOutlet weak var redImage_lbl: UIImageView!
    @IBOutlet weak var oneFourty_lbl: UILabel!
    @IBOutlet weak var greenImage_lbl: UIImageView!
    @IBOutlet weak var bestValue_lbl: UILabel!
    
    @IBOutlet weak var twenty_lbl: UILabel!
    @IBOutlet weak var blueImage_lbl: UIImageView!
    
    @IBOutlet weak var monthlyGreen: UILabel!
    @IBOutlet weak var monthlyRed: UILabel!
    //MARK: - menuBtnPressed() method
    @IBOutlet weak var monthlyBlue: UILabel!
    
    
    @IBOutlet weak var barButton: UIButton!
    
    @IBOutlet weak var arrowImageGreen: UIImageView!
    @IBOutlet weak var getItNow_Lbl: UILabel!
    
    @IBOutlet weak var redImage: UIImageView!
    @IBOutlet weak var wrapperbtn_green: UIButton!
    
    @IBOutlet weak var getItNow_Blue: UILabel!
    @IBOutlet weak var blueImage: UIImageView!
    @IBOutlet weak var wrapperBtn_red: UIButton!
    @IBOutlet weak var wrapperBtn_Blue: UIButton!
    @IBOutlet weak var activePlan_Lbl: UILabel!
    @IBAction func menuBtnPressed(sender: AnyObject)
    {
        
        
        if currentUserRole == "teacher"
        {
            if(x == -myMenuView.frame.size.width)
            {
                //navBar.frame.origin.x = 0
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = 0
                navx = myMenuView.frame.size.width
            }
                
            else
            {
                //navBar.frame.origin.x = myMenuView.frame.size.width
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = -myMenuView.frame.size.width
                navx = 0
            }
            
            
            UIView.animateWithDuration(0.5, animations: {
                
                self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
            })
        }
        else
        {
            if(x == -myMenuViewStudent.frame.size.width)
            {
                //navBar.frame.origin.x = 0
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = 0
                navx = myMenuViewStudent.frame.size.width
            }
                
            else
            {
                //navBar.frame.origin.x = myMenuView.frame.size.width
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = -myMenuViewStudent.frame.size.width
                navx = 0
            }
            
            
            UIView.animateWithDuration(0.5, animations: {
                
                self.navBar.frame.origin.x = self.navx
                myMenuViewStudent.frame.origin.x = self.x
                
            })
        }
    }
    
    
    
    //MARK: - iPadCompatibility() method
    func iPadCompatibility()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            barButton.frame = CGRectMake(0, 0, 45, 45)
            
            basic_lbl.font = UIFont.boldSystemFontOfSize(26)
            professional_lbl.font = UIFont.boldSystemFontOfSize(26)
            premier_lbl.font = UIFont.boldSystemFontOfSize(26)
            
            mostPopular_lbl.font = UIFont.systemFontOfSize(24)
            lessThan_lbl.font = UIFont.systemFontOfSize(24)
            bestValue_lbl.font = UIFont.systemFontOfSize(24)
            
            greenImage_lbl.frame = CGRectMake(570, 20, 150, 120)
            redImage_lbl.frame = CGRectMake(570, 20, 150, 120)
            blueImage_lbl.frame = CGRectMake(570, 20, 150, 120)
            
            twenty_lbl.font = UIFont.systemFontOfSize(30)
            fourtyNine_lbl.font = UIFont.systemFontOfSize(30)
            oneFourty_lbl.font = UIFont.systemFontOfSize(22)
            
            twenty_lbl.frame = CGRectMake(610, 50, 50, 25)
            fourtyNine_lbl.frame = CGRectMake(610, 50, 50, 25)
            oneFourty_lbl.frame = CGRectMake(610, 50, 50, 25)
            
            monthlyGreen.font = UIFont.systemFontOfSize(20)
            monthlyBlue.font = UIFont.systemFontOfSize(20)
            monthlyRed.font = UIFont.systemFontOfSize(20)
            
            monthlyRed.frame = CGRectMake(590, 82, 100, 25)
            monthlyGreen.frame = CGRectMake(590, 82, 100, 25)
            monthlyBlue.frame = CGRectMake(590, 82, 100, 25)
            
            
            arrowImageGreen.frame = CGRectMake(50,100, 50, 50)
            getItNow_Lbl.frame = CGRectMake(100, 100, 250, 50)
            wrapperbtn_green.frame = CGRectMake(50, 100, 300, 50)
            //wrapperbtn_green.backgroundColor = UIColor.blackColor()
            getItNow_Lbl.font = UIFont.boldSystemFontOfSize(22)
            
            redImage.frame = CGRectMake(50,100, 50, 50)
            activePlan_Lbl.frame = CGRectMake(100, 100, 250, 50)
            wrapperBtn_red.frame = CGRectMake(50, 100, 300, 50)
            //wrapperbtn_green.backgroundColor = UIColor.blackColor()
            activePlan_Lbl.font = UIFont.boldSystemFontOfSize(22)
            
            
            blueImage.frame = CGRectMake(50,100, 50, 50)
            getItNow_Blue.frame = CGRectMake(100, 100, 250, 50)
            wrapperBtn_Blue.frame = CGRectMake(50, 100, 300, 50)
            //wrapperbtn_green.backgroundColor = UIColor.blackColor()
            getItNow_Blue.font = UIFont.boldSystemFontOfSize(22)
            
            
            imageIcon.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 25
            
            basicView.frame.origin.y = imageIcon.frame.origin.y + imageIcon.frame.height + 25
            
            professionalView.frame.origin.y = basicView.frame.origin.y + basicView.frame.height + 25
            
            premierView.frame.origin.y = professionalView.frame.origin.y + professionalView.frame.height + 25
        
            
        }
        else
        {
            imageIcon.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 10
            
            basicView.frame.origin.y = imageIcon.frame.origin.y + imageIcon.frame.height + 10
            
            professionalView.frame.origin.y = basicView.frame.origin.y + basicView.frame.height + 10
            
            premierView.frame.origin.y = professionalView.frame.origin.y + professionalView.frame.height + 10
            

        }
        
    }

    
     //MARK: - viewDidLoad() method
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
        //STATUS BAR COLOR
        let statusBarView:UIView = UIView()
        statusBarView.frame = CGRectMake(0, 0, self.view.frame.width, 20)
        statusBarView.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 0.85)
        self.view.addSubview(statusBarView)
        
        navBar.barTintColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1)
        
       // MyPlanimageView.image = UIImage(named: "ic_myplan.png")
       
        //x = -320
        currentVC = self
        if currentUserRole == "teacher"
        {
            self.view.addSubview(myMenuView)
            
            myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
            x = -myMenuView.frame.size.width
        }
        else
        {
            self.view.addSubview(myMenuViewStudent)
            
            myMenuViewStudent.frame = CGRectMake(-myMenuViewStudent.frame.size.width, self.view.frame.origin.y,myMenuViewStudent.frame.size.width ,myMenuViewStudent.frame.size.height)
            x = -myMenuViewStudent.frame.size.width
        }
        
        
        iPadCompatibility()
    }
    
    
      //MARK: - basicPlanBtn() method
    @IBAction func basicPlanBtn(sender: AnyObject)
    {
        println("Btn Clicked Event")
        
        let basicPlanVC = self.storyboard!.instantiateViewControllerWithIdentifier("MyPlanBASICViewControllerSegue") as! MyPlanBASICViewController
        
        self.navigationController?.pushViewController(basicPlanVC, animated: true)
        
        
    }

    
    //MARK: - didReceiveMemoryWarning() method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
     //MARK: - professionalPlanBtn() method
    @IBAction func professionalPlanBtn(sender: AnyObject)
    {
        println("Btn Clicked Event")
        let professionalPlanVC = self.storyboard!.instantiateViewControllerWithIdentifier("MyPlanPROFESSIONALViewControllerSegue")as! MyPlanPROFESSIONALViewController
        
        self.navigationController?.pushViewController(professionalPlanVC, animated: true)
        

    }
    
    
     //MARK: - premierPlanBtn() method
    @IBAction func premierPlanBtn(sender: AnyObject)
    {
         println("Btn Clicked Event")
        let premierPlanVC = self.storyboard!.instantiateViewControllerWithIdentifier("MyPlanPREMIERViewControllerSegue") as! MyPlanPREMIERViewController
        
        self.navigationController?.pushViewController(premierPlanVC, animated: true)
        

    }
    
    
    
    
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
