//
//  NewEssay_MenuViewController.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/30/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class EditMyInvitationsViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate,UIAlertViewDelegate
{
    
    //var postId:String = String()
    var alert_action :UIAlertView!
    
    @IBOutlet weak var titleTextView: UITextView!
    
    @IBOutlet weak var contentsTextView: UITextView!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
  
    
    
    @IBOutlet weak var navBar: UINavigationBar!
    
    var result_Save:NSDictionary!
    var result_Completed:NSDictionary!
    var x:CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    @IBOutlet weak var barButton: UIButton!
    var subView:UIView = UIView()
    
    
    @IBOutlet weak var save_btn: UIButton!
    
    @IBOutlet weak var complete_btn: UIButton!
    
    
    
    //MARK: - back_btn()
    @IBAction func back_btn(sender: AnyObject)
    {
         self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    //MARK: - iPadCompatibility()
    func iPadCompatibility()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            barButton.frame = CGRectMake(0, 0, 40, 40)
        
            
            titleTextView.font = UIFont.systemFontOfSize(22)
            contentsTextView.font = UIFont.systemFontOfSize(22)
            
            //contentsTextView.frame = CGRectMake(65, 23, 162, 25)
            
            save_btn.titleLabel?.font = UIFont.systemFontOfSize(22)
            complete_btn.titleLabel?.font = UIFont.systemFontOfSize(22)
            
            titleTextView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 20
            contentsTextView.frame.origin.y = titleTextView.frame.origin.y + titleTextView.frame.height + 20
            save_btn.frame.origin.y = contentsTextView.frame.origin.y + contentsTextView.frame.height + 20
            complete_btn.frame.origin.y = contentsTextView.frame.origin.y + contentsTextView.frame.height + 20
            
        }
        else
        {
            
            titleTextView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 10
            contentsTextView.frame.origin.y = titleTextView.frame.origin.y + titleTextView.frame.height + 10
            save_btn.frame.origin.y = contentsTextView.frame.origin.y + contentsTextView.frame.height + 10
            complete_btn.frame.origin.y = contentsTextView.frame.origin.y + contentsTextView.frame.height + 10
            
            
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                
                
            }
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                
            }
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                barButton.frame = CGRectMake(0, 0, 35, 35)
            }
                
            else
            {
                
            }

        }
        
    }

    
     //MARK: - UITextFieldDelegates() method
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        //titleTextField.resignFirstResponder()
        return true
    }
    
    

    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        
        println("editing starts")
        
        var placeholderText = "Write your title here"
        if textView.tag == 1
        {
            placeholderText = ""
            
            //titleTextView.frame.size.height -= 30
            
        }
        
        if textView.tag == 2
        {
            placeholderText = "Write your article here"
            
            self.view.frame.origin.y -= 160
        }
        else
        {
            self.view.frame.origin.y = 0
        }
        
        
        
        if textView.text == placeholderText
        {
            textView.text = ""
        }
        
        return true
    }
    
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool
    {
        println("end editing")
        
        var placeholderText = ""
        if textView.tag == 1
        {
            placeholderText = "Write your title here"
            
            // titleTextView.frame.size.height += 30
        }
        
        if textView.tag == 2
        {
            placeholderText = "Write your article here"
            
            self.view.frame.origin.y = 0
        }
        
        if textView.text == ""
        {
            textView.text = placeholderText
        }

        return true
    }
    
    
    //MARK: - viewDidLoad()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        iPadCompatibility()
        
        titleTextView.delegate = self
        contentsTextView.delegate = self
        
        //STATUS BAR COLOR
        let statusBarView:UIView = UIView()
        statusBarView.frame = CGRectMake(0, 0, self.view.frame.width, 20)
        statusBarView.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 0.85)
        self.view.addSubview(statusBarView)
        
        navBar.barTintColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1)
        
       titleTextView.becomeFirstResponder()
        
        
        titleTextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        titleTextView.layer.borderWidth = 1.5
        titleTextView.layer.masksToBounds = true
        
        
        contentsTextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        
        createActivityIndicator()         //calling activity Indicator() method
       
        editMyInvitation()          //calling edit essay API
        
    }


    
    
    //MARK: - activityIndicator() method
    func createActivityIndicator()
    {
        subView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        subView.backgroundColor = UIColor.blackColor()
        subView.alpha = 0.75
        self.view.addSubview(subView)
        
        
        
        let activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle:UIActivityIndicatorViewStyle.White)
        subView.addSubview(activityIndicator)
        var x = (subView.frame.width/2)-(activityIndicator.frame.width/2)
        println(x)
        var y = ((subView.frame.height/2)-(activityIndicator.frame.height/2))
        println(y)
        
        activityIndicator.frame = CGRectMake(x, y, activityIndicator.frame.width,activityIndicator.frame.height)
        
        activityIndicator.startAnimating()
    }

    
    //MARK: - editMyInvitation() API
    func editMyInvitation()
    {
       // println(myInvitationResult)
        
        post_id = myInvitationResult["post-id"] as! Int
        println(post_id)

        
         subView.hidden = false
        
        // to view data for editing
        var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=viewacceptedessay&uid=\(user_id)&post-id=\(post_id)"
        
        println(urlPath)
        
        let url: NSURL = NSURL(string : urlPath)!
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            
            
            
            if error != nil
            {
                self.subView.hidden = true
                println(error.localizedDescription)
            }
            
            println(data)
            
            var err: NSError?
            
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                self.subView.hidden = true
                
                var resultDict:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
                
                if err != nil
                {
                    println("JSON Error \(err!.localizedDescription)")
                }
                
                println("edit essay = \(resultDict)")
                
                var status = resultDict.valueForKey("status")as! String
                println(status)
                
                
                if(status == "Success")
                {
                    postValuesDictionary  = resultDict["post"] as! NSDictionary
                    println(postValuesDictionary)
                    
                }
                
                
                
                self.titleTextView.text = postValuesDictionary["title"] as! String
                self.contentsTextView.text = postValuesDictionary["content"] as! String
                
                self.subView.hidden = true
            })
            
        })
        
        task.resume()
        
    }

    

     //MARK: - save_btn() method
    @IBAction func save_btn(sender: AnyObject)
    {

        
        println("save button clicked")
        
        subView.hidden = false
        
        alert_action = UIAlertView(title: "", message: "Do you want to save the post?", delegate: self, cancelButtonTitle: "No", otherButtonTitles: "Yes")
        
        alert_action.tag = 3
        
        let noBtn = UIAlertAction(title:"No", style: .Cancel)
        {
                (action: UIAlertAction!) -> Void in
        }
        
        
        let yes_Btn = UIAlertAction(title:"Yes", style: .Default)
        {
                (action: UIAlertAction!) -> Void in
        }
        
        
        
        alert_action.show()
   }



    //MARK: - complete_btn() method
    @IBAction func complete_btn(sender: AnyObject)
    {
        
        println("complete button clicked")
        
       subView.hidden = false
        
        alert_action = UIAlertView(title: "", message: "Do you want to complete this post?", delegate: self, cancelButtonTitle: "No", otherButtonTitles: "Yes")
        
        alert_action.tag = 6
        
        let no_Btn = UIAlertAction(title:"No", style: .Cancel)
        {
                (action: UIAlertAction!) -> Void in
        }
        
        
        let yes_Btn = UIAlertAction(title:"Yes", style: .Default)
        {
                (action: UIAlertAction!) -> Void in
        }
        
        
        alert_action.show()

    }
    
    
    
    //MARK: - clickedButtonAtIndex() method
  func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
  {
    
        // MARK: - after post saved successfully
        if alert_action.tag == 9
        {
            subView.hidden = true
            
            let ok_btn = self.storyboard!.instantiateViewControllerWithIdentifier("MyInvitationsViewControllerSegue") as! MyInvitationsViewController
            
            self.navigationController?.pushViewController(ok_btn,animated: true)

        }
    
    
    
    // MARK: - after post completed successfully
        if alert_action.tag == 10
        {
            subView.hidden = true
            
            let ok_btn = self.storyboard!.instantiateViewControllerWithIdentifier("CompletedViewControllerSegue") as! CompletedViewController
            
            self.navigationController?.pushViewController(ok_btn,animated: true)
        }
   
  }
    
    
    
    
    //MARK: - didDismissWithButtonIndex() method
    func alertView(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int)
    {
        
        //MARK: - for saving essay
        if ((alert_action.tag == 3) && (buttonIndex == 1))
        {
            println("yes button clicked for save")
            
            subView.hidden = false
            
            saveEssay()                 //calling saveEssay() API
        }
        else
        {
            subView.hidden = true
        }
        
        
        
        //MARK: - for completing an essay
        if ((alert_action.tag == 6) && (buttonIndex == 1))
        {
            println("yes button clicked for complete")
            
            subView.hidden = false
            
            completeEssay()         //calling complete Essay() API
        }
        else
        {
            subView.hidden = true
        }
        
    }
    
    
    
    //MARK: - saveEssay() API
    func saveEssay()
    {
        subView.hidden = false
        
        var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=checkessay"
        
        var post:NSString = "uid=\(user_id)&post-id=\(post_id)&title=\(titleTextView.text)&content=\(contentsTextView.text)&complete=false&comment="
        
        NSLog("PostData: %@",post);
        
        var url1:NSURL = NSURL(string: urlPath)!
        
        var postData:NSData = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength:NSString = String( postData.length )
        
        var request:NSMutableURLRequest = NSMutableURLRequest(URL: url1)
        request.HTTPMethod = "POST"
        request.HTTPBody = postData
        request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        
        var reponseError: NSError?
        var response: NSURLResponse?
        
        var urlData: NSData? = NSURLConnection.sendSynchronousRequest(request, returningResponse:&response, error:&reponseError)
        
        if ( urlData != nil ) {
            let res = response as! NSHTTPURLResponse!;
            
            NSLog("Response code: %ld", res.statusCode);
            
            if (res.statusCode >= 200 && res.statusCode < 300)
            {
                var responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                
                NSLog("Response ==> %@", responseData);
                
                var error: NSError?
                
                
                 subView.hidden = true
                
                result_Save = NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers , error: &error) as! NSDictionary
                
                
                var success:String = result_Save.valueForKey("status") as! String
                
                println(success)
                
                if(success == "Success")
                {
                    
                    alert_action = UIAlertView(title: "", message: "Post saved successfully", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    
                    alert_action.tag = 9
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                    {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    alert_action.show()
                    
                }
                    
                else
                {
                    
                    alert_action = UIAlertView(title: "", message: "Post not saved successfully", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    alert_action.tag = 4
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                    {
                            (action: UIAlertAction!) -> Void in
                            
                    }
                    
                    
                    alert_action.show()
                    
                    subView.hidden = true
                    
                    println("Login failed1")
                }
                
            }
            else
            {
                subView.hidden = true
                println("Login failed2")
                
            }
        }
        else
        {
            alert_action = UIAlertView(title: "Internet Error..!", message: "Please check your Internet Connection", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
            
            alert_action.tag = 5
            
            let okBtn = UIAlertAction(title:"OK", style: .Default)
            {
                    (action: UIAlertAction!) -> Void in
            }
            
            
            alert_action.show()
            
            
            subView.hidden  = true
            println("Login failed3")
        }
        
        
        println(result_Save)
    }
    
    
 
    //MARK: - completeEssay() API
    func completeEssay()
    {
        subView.hidden = true
        
        var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=checkessay"
        
        var post:NSString = "uid=\(user_id)&post-id=\(post_id)&title=\(titleTextView.text)&content=\(contentsTextView.text)&complete=true&comment="
        
        NSLog("PostData: %@",post);
        
        var url1:NSURL = NSURL(string: urlPath)!
        
        var postData:NSData = post.dataUsingEncoding(NSASCIIStringEncoding)!
        
        var postLength:NSString = String( postData.length )
        
        var request:NSMutableURLRequest = NSMutableURLRequest(URL: url1)
        request.HTTPMethod = "POST"
        request.HTTPBody = postData
        request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        
        var reponseError: NSError?
        var response: NSURLResponse?
        
        var urlData: NSData? = NSURLConnection.sendSynchronousRequest(request, returningResponse:&response, error:&reponseError)
        
        if ( urlData != nil ) {
            let res = response as! NSHTTPURLResponse!;
            
            NSLog("Response code: %ld", res.statusCode);
            
            if (res.statusCode >= 200 && res.statusCode < 300)
            {
                var responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                
                NSLog("Response ==> %@", responseData);
                
                var error: NSError?
                
                subView.hidden = true
                
                result_Completed = NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers , error: &error) as! NSDictionary
                
                
                var success:String = result_Completed.valueForKey("status") as! String
                
                println(success)
                
                if(success == "Success")
                {
                    // println("Completed Successfully")
                    
                    alert_action = UIAlertView(title: "", message: "Post completed successfully", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    
                    alert_action.tag = 10
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                    {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    alert_action.show()
                    
                    subView.hidden = true
                }
                    
                else
                {
                    
                    alert_action = UIAlertView(title: " ", message: "Post not completed successfully", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    alert_action.tag = 7
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                        {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    alert_action.show()
                    
                    subView.hidden = true
                    println("Login failed1")
                }
                
            }
            else
            {
                subView.hidden = true
                println("Login failed2")
                
            }
        }
        else
        {
            alert_action = UIAlertView(title: "Internet Error..!", message: "Please check your Internet Connection", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
            
            
            alert_action.tag = 8
            
            let okBtn = UIAlertAction(title:"OK", style: .Default)
            {
                    (action: UIAlertAction!) -> Void in
            }
            
            
            alert_action.show()
            
            subView.hidden = true
            println("Login failed3")
        }
        
        
        println(result_Completed)
 
    }
    
    
    //MARK: - hideKeyPad() method
    @IBAction func hideKeyPad(sender: AnyObject)
    {
        //titleTextField.resignFirstResponder()
        titleTextView .resignFirstResponder()
        contentsTextView.resignFirstResponder()
    }
    
    
    //MARK: - didReceiveMemoryWarning() method 
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

       

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
