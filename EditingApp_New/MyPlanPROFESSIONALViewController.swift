//
//  MyPlanPROFESSIONALViewController.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class MyPlanPROFESSIONALViewController: UIViewController
{
    
    
    let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    
    var navx: CGFloat = 0.0
    var x:CGFloat = 0.0

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var wrapperView: UIView!
            
    @IBOutlet weak var professionalPlanView: UIView!
    @IBOutlet weak var textViewField: UIView!
    
    @IBOutlet weak var activePlan: UIButton!
    @IBOutlet weak var securityView: UIView!
    @IBOutlet weak var fullEmailView: UIView!
    @IBOutlet weak var mobileAppView: UIView!
    @IBOutlet weak var priorityView: UIView!
    
    
    
    
    @IBOutlet weak var requestsImage: UILabel!
    @IBOutlet weak var mostPopularLBL: UILabel!
    @IBOutlet weak var proFessional_lbl: UILabel!
    @IBOutlet weak var priorityImage: UIButton!
    @IBOutlet weak var unlimitedStorageImage: UIButton!

    @IBOutlet weak var mobileAppImage: UIButton!
    
    @IBOutlet weak var fullEmailSupport_Image: UIButton!
    @IBOutlet weak var unlimitedImage: UIButton!
    
    @IBOutlet weak var enhnacedSecurity_Image: UIButton!
    @IBOutlet weak var privateURL_Image: UIButton!
    @IBOutlet weak var darkRed_Lbl: UILabel!
    
    @IBOutlet weak var enhancedSecurity_Lbl: UILabel!
    @IBOutlet weak var privateURL_Lbl: UILabel!
    @IBOutlet weak var unlimited_Lbl: UILabel!
    @IBOutlet weak var prioritySupport_Lbl: UILabel!
    @IBOutlet weak var requests_Lbl: UILabel!
    
    @IBOutlet weak var fullEmail_Lbl: UILabel!
    @IBOutlet weak var mobileApp_Lbl: UILabel!
    @IBOutlet weak var unlimitedStorage_Lbl: UILabel!
    
     //MARK: - iPadCompatibility() method
    @IBOutlet weak var barButton: UIButton!
    func iPadCompatibility()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
             barButton.frame = CGRectMake(0, 0, 40, 40)
            
            darkRed_Lbl.frame = CGRectMake(0, 120, 720, 55)
            darkRed_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 24)
            
           // refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            
            proFessional_lbl.frame = CGRectMake(230, 25, 300, 30)
            mostPopularLBL.frame = CGRectMake(270, 65, 200, 35)
            
            
            
            proFessional_lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 40)
            mostPopularLBL.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 32)
            activePlan.titleLabel?.font = UIFont.systemFontOfSize(24)
            
            //requests_Lbl.font = UIFont.boldSystemFontOfSize(24)
            prioritySupport_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            unlimitedStorage_Lbl.font = UIFont(name: "Helvetica Neue-CondensedBold", size: 20)
            mobileApp_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            unlimited_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            fullEmail_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            privateURL_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            enhancedSecurity_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            requests_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            unlimitedStorage_Lbl.font = UIFont(name: "HelveticaNeue-CondensedBold", size: 20)
            
            
            
            //requests_Lbl.frame = CGRectMake(20, 10, 30, 25)

            priorityImage.frame = CGRectMake(20, 10, 30, 30)
            unlimitedStorageImage.frame = CGRectMake(20, 10, 30, 30)
            mobileAppImage.frame = CGRectMake(20, 10, 30, 30)
            unlimitedImage.frame = CGRectMake(20, 10, 30, 30)
            fullEmailSupport_Image.frame = CGRectMake(20, 10, 30, 30)
            privateURL_Image.frame = CGRectMake(20, 10, 30, 30)
            enhnacedSecurity_Image.frame = CGRectMake(20, 10, 30, 30)

            
            
            wrapperView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 20
            
            professionalPlanView.frame.origin.y = wrapperView.frame.origin.y + wrapperView.frame.height
            
            activePlan.frame.origin.y = professionalPlanView.frame.origin.y + professionalPlanView.frame.height

        }
        else
        {
            wrapperView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 10
            
            professionalPlanView.frame.origin.y = wrapperView.frame.origin.y + wrapperView.frame.height
            
            activePlan.frame.origin.y = professionalPlanView.frame.origin.y + professionalPlanView.frame.height
        }
    }

    
    //MARK: - backBtnPressed() method
    @IBAction func backBtnPressed(sender: AnyObject)
    {
         self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    
      //MARK: - activePlanProfessionalBtn() method
    @IBAction func activePlanProfessionalBtn(sender: AnyObject)
    {
        
    }

    
    
    
    //MARK: - viewDidLoad() method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //STATUS BAR COLOR
        let statusBarView:UIView = UIView()
        statusBarView.frame = CGRectMake(0, 0, self.view.frame.width, 20)
        statusBarView.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 0.85)
        self.view.addSubview(statusBarView)
        
        navBar.barTintColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1)
        
        iPadCompatibility()

        textViewField.layer.borderWidth = 1.0
        textViewField.layer.masksToBounds = true
        textViewField.layer.borderColor = UIColor.whiteColor().CGColor
        
        priorityView.layer.borderWidth = 0.5
        priorityView.layer.borderColor =  UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1).CGColor
        priorityView.layer.masksToBounds = true
        
        fullEmailView.layer.borderWidth = 0.5
        fullEmailView.layer.borderColor =  UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1).CGColor
        fullEmailView.layer.masksToBounds = true
        
        mobileAppView.layer.borderWidth = 0.5
        mobileAppView.layer.borderColor =  UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1).CGColor
        mobileAppView.layer.masksToBounds = true

        
        securityView.layer.borderWidth = 0.5
        securityView.layer.borderColor =  UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1).CGColor
        securityView.layer.masksToBounds = true

   }

    
   
    //MARK: - didReceiveMemoryWarning() method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
