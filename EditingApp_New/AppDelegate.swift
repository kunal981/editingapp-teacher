//
//  AppDelegate.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


var currentVC: UIViewController!


var currentUserRole = ""

var myMenuView :UIView = UIView()

var myMenuViewStudent :UIView = UIView()



let invitationButton:UIButton = UIButton()
let myInvitationButton:UIButton = UIButton()
let completedButton:UIButton = UIButton()
let myAccountButton:UIButton = UIButton()
let signOutButton:UIButton = UIButton()

 //var myEssayimageView : UIImageView = UIImageView()

var imageView : UIImageView = UIImageView()
var nameLabel: UILabel = UILabel()
var titleLabel: UILabel = UILabel()
var lineImage1: UIImageView = UIImageView()
let myProfileButton:UIButton = UIButton()



 var invitationimageView : UIImageView = UIImageView()
 var myInvitationimageView : UIImageView = UIImageView()
 var completedViewimageView : UIImageView = UIImageView()
 var myAccountimageView : UIImageView = UIImageView()



 var myEssayimageView : UIImageView = UIImageView()
 var newEssayimageView : UIImageView = UIImageView()
 var underReviewimageView : UIImageView = UIImageView()
 var completedViewimageView_Student : UIImageView = UIImageView()
 var MyPlanimageView : UIImageView = UIImageView()
var myAccountimageView_Student : UIImageView = UIImageView()




let myEssayButton:UIButton = UIButton()
let newEssayButton:UIButton = UIButton()
let underReviewButton:UIButton = UIButton()
let completedButton_Student:UIButton = UIButton()
let MyPlanButton:UIButton = UIButton()
let myAccountButton_Student:UIButton = UIButton()
let signOutButton_Student:UIButton = UIButton()

//var myEssayimageView : UIImageView = UIImageView()


var imageView_Student : UIImageView = UIImageView()
var nameLabel_Student: UILabel = UILabel()
var titleLabel_Student: UILabel = UILabel()
var lineImage1_Student: UIImageView = UIImageView()
let myProfileButton_Student:UIButton = UIButton()



var documentDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as! String

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UIAlertViewDelegate
{

   
    var window: UIWindow?
    
    
    //MARK: - TEACHER MODULE VARIABLE DECLARATIONS
    
    //MARK: 1.Invitation View Variables
    var invitationView :UIView = UIView()
    let invitationLabel:UILabel = UILabel()
    var lineImage2: UIImageView = UIImageView()
    
    
     //MARK: 2.My Invitation View Variables
    var myInvitationView :UIView?
    let myInvitationLabel:UILabel = UILabel()
    var lineImage3: UIImageView = UIImageView()

   
    
     //MARK: 3.Completed View Variables
    var completedView :UIView?
    let completedViewLabel:UILabel = UILabel()
    var completedViewlineImage: UIImageView = UIImageView()



    
     //MARK: 4.My Account View Variables
    var myAccountView :UIView?
    let myAccountLabel:UILabel = UILabel()
    var lineImage5: UIImageView = UIImageView()

   
    
     //MARK: 5.SignOut View Variables
    var signOutView :UIView?
    let signOutLabel:UILabel = UILabel()
    var signOutimageView : UIImageView = UIImageView()
    var lineImage: UIImageView = UIImageView()
    
    //MARK: 6.AlertView View Variables Teacher
    var alertView: UIView = UIView()
    var alert: UIAlertView = UIAlertView()
    let signOutBtn: UIAlertAction = UIAlertAction()
    let cancelBtn: UIAlertAction = UIAlertAction()
    
    
    
    
    
    
   
    
    //MARK: - STUDENT MODULE VARIABLE DECLARATIONS
    
    
    //MARK: 1.My Essay View Variables
    var myEssayView :UIView = UIView()
    let myEssayLabel:UILabel = UILabel()
    var myEssayLineImage: UIImageView = UIImageView()
    
    
    //MARK: 2.New Essay View Variables
    var newEssayView :UIView?
    let newEssayLabel:UILabel = UILabel()
    var newEssayLineImage: UIImageView = UIImageView()
    
    
    
    //MARK: 3.UnderReview View Variables
    var underReviewView :UIView?
    let underReviewLabel:UILabel = UILabel()
    
    var underReviewLineImage: UIImageView = UIImageView()
    
    
    
    //MARK: 4.Completed View Variables
    var completedView_Student :UIView?
    let completedViewLabel_Student:UILabel = UILabel()
    var completedViewlineImage_Student: UIImageView = UIImageView()
    
    
    
    //MARK: 5.My Plan View Variables
    var MyPlanView :UIView?
    let MyPlanLabel:UILabel = UILabel()
    var MyPlanlineImage: UIImageView = UIImageView()

    
    //MARK: 6.My Account View Variables
    var myAccountView_Student :UIView?
    let myAccountLabel_Student:UILabel = UILabel()
   
    var myAccountLineImage_Student: UIImageView = UIImageView()
    
    
    //MARK: 7.SignOut View Variables
    var signOutView_Student :UIView?
    let signOutLabel_Student:UILabel = UILabel()
    var signOutimageView_Student : UIImageView = UIImageView()
    var signOutLineImage_Student: UIImageView = UIImageView()
    

     //MARK: 8.AlertView View Variables Student
    var alertView_Student: UIView = UIView()
    var alert_Student: UIAlertView = UIAlertView()
    let signOutBtn_Student: UIAlertAction = UIAlertAction()
    let cancelBtn_Student: UIAlertAction = UIAlertAction()
    
    
    
    //MARK: - didFinishLaunchingWithOptions()
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        println(documentDirectory)
        
        createMenu()
        
       // iPadCompatibility()
        
        window?.addSubview(myMenuView)
        window?.addSubview(myMenuViewStudent)
    
        // to show the status bar in white color
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
        
        return true

    }
    
    
    //MARK: - createMenu()
    func createMenu()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            nameLabel.font = UIFont.boldSystemFontOfSize(24)
                
            titleLabel.font = UIFont.boldSystemFontOfSize(20)
            
            nameLabel_Student.font = UIFont.boldSystemFontOfSize(24)
                
            titleLabel_Student.font = UIFont.boldSystemFontOfSize(20)
        
            myMenuView = UIView(frame:CGRectMake(0, 0, 320, 800))
            
            myMenuViewStudent = UIView(frame:CGRectMake(0, 0, 320, 800))
        }
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                nameLabel.font = UIFont.boldSystemFontOfSize(18)
                
                titleLabel.font = UIFont.boldSystemFontOfSize(14)
                
                nameLabel_Student.font = UIFont.boldSystemFontOfSize(18)
                
                titleLabel_Student.font = UIFont.boldSystemFontOfSize(14)
                
                myMenuView = UIView(frame:CGRectMake(0, 0, 320, 800))
                
                myMenuViewStudent = UIView(frame:CGRectMake(0, 0, 320, 800))
                
            }
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                nameLabel.font = UIFont.boldSystemFontOfSize(16)
                
                titleLabel.font = UIFont.boldSystemFontOfSize(12)
                
                nameLabel_Student.font = UIFont.boldSystemFontOfSize(16)
                
                titleLabel_Student.font = UIFont.boldSystemFontOfSize(12)
                
                myMenuView = UIView(frame: CGRectMake(0, 0, 320, 800))
                
                myMenuViewStudent = UIView(frame: CGRectMake(0, 0, 320, 800))
            }
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                nameLabel.font = UIFont.boldSystemFontOfSize(18)
                
                titleLabel.font = UIFont.boldSystemFontOfSize(14)
                
                nameLabel_Student.font = UIFont.boldSystemFontOfSize(18)
                
                titleLabel_Student.font = UIFont.boldSystemFontOfSize(14)
                
                myMenuView = UIView(frame: CGRectMake(0, 0, 320, 800))
                
                myMenuViewStudent = UIView(frame: CGRectMake(0, 0, 320, 800))
                
            }
            else
            {
                nameLabel.font = UIFont.boldSystemFontOfSize(18)
                
                titleLabel.font = UIFont.boldSystemFontOfSize(14)
                
                nameLabel_Student.font = UIFont.boldSystemFontOfSize(18)
                
                titleLabel_Student.font = UIFont.boldSystemFontOfSize(14)
                
                myMenuView = UIView(frame:CGRectMake(0, 0,320 , 800))
                
                myMenuViewStudent = UIView(frame: CGRectMake(0, 0, 320, 800))
            }
            
 
        }
        
        myMenuView.backgroundColor = UIColor( red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        
        
        myMenuViewStudent.backgroundColor = UIColor( red: 242/255, green: 111/255, blue:112/255, alpha: 1)

        
        
    
        
        //MARK: - (a)User Profile Image Teacher
        imageView  = UIImageView(frame:CGRectMake((14/320)*UIScreen.mainScreen().bounds.size.width, (35/568)*UIScreen.mainScreen().bounds.size.height,(60/320)*UIScreen.mainScreen().bounds.size.width,(60/320)*UIScreen.mainScreen().bounds.size.width))
        imageView.image = UIImage(named: "unknown.jpg")
      
        
        
        myMenuView .addSubview(imageView)
        
        
        
        
        
        //MARK: - (b)User Profile Name Teacher
        nameLabel.frame = CGRectMake((95/320)*UIScreen.mainScreen().bounds.size.width,(35/568)*UIScreen.mainScreen().bounds.size.height,(150/320)*UIScreen.mainScreen().bounds.size.width,(20/568)*UIScreen.mainScreen().bounds.size.height)
        
        nameLabel.textColor = UIColor.whiteColor()
        nameLabel.textAlignment = NSTextAlignment.Left
        nameLabel.text = "Arianda Hurley"
        nameLabel.userInteractionEnabled = true
        
        
        nameLabel.text = "\(firstname_menu) \(lastname_menu)"

        
        myMenuView .addSubview(nameLabel)
        
        
        
        
        
        //MARK: - (c)User Profile Role Teacher
        titleLabel.frame = CGRectMake((95/320)*UIScreen.mainScreen().bounds.size.width,(60/568)*UIScreen.mainScreen().bounds.size.height,(150/320)*UIScreen.mainScreen().bounds.size.width,(20/568)*UIScreen.mainScreen().bounds.size.height)
        
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.textAlignment = NSTextAlignment.Left
        titleLabel.text = "Author"
        
        myMenuView.addSubview(titleLabel)
        
        
        
        
     
        
        
        //MARK: - (d)Profile Back Button teacher
        myProfileButton.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(35/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuView.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(76/568)*UIScreen.mainScreen().bounds.size.height)
        myProfileButton.setTitle ("", forState: UIControlState.Normal)
        myProfileButton.addTarget(self, action:"goToMyAccount:",forControlEvents: UIControlEvents.TouchUpInside)
    
        
        myMenuView.addSubview(myProfileButton)
        
        
        
        
     

        
        
        // adding an image image
        lineImage1.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (132/568)*UIScreen.mainScreen().bounds.size.height, (0.75*(myMenuView.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)
        
        lineImage1.image = UIImage(named:"img_line.png")
        
        myMenuView .addSubview(lineImage1)
        
        
        
        
    
        
        //MARK: - TEACHER MODULE VIEW DECLARATIONS
        
        
        //MARK: - 1.Invitation View Teacher
        println("mymenuview width = \(myMenuView.frame.width)")
        
        invitationView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (133/568)*UIScreen.mainScreen().bounds.size.height, (myMenuView.frame.width/320)*UIScreen.mainScreen().bounds.size.width, (51/568)*UIScreen.mainScreen().bounds.size.height))

        invitationView.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        
        
        myMenuView .addSubview(invitationView)
        
        var uiscreenWidth = UIScreen.mainScreen().bounds.size.width
        var invitationView_width = ((myMenuView.frame.width/320)*UIScreen.mainScreen().bounds.size.width)
        var buttonWidth = ((0.85*(myMenuView.frame.width)/320)*UIScreen.mainScreen().bounds.size.width)
        var uiScreenHeight = UIScreen.mainScreen().bounds.size.height
        var subViewHeight = ((51/568)*UIScreen.mainScreen().bounds.size.height)

        
        println("invitation view width = \(invitationView_width)")
        println("invitation button width = \(buttonWidth)")
        println("uiScreen view width = \(uiscreenWidth)")
        println("uiScreen view height = \(uiScreenHeight)")
        println("subView height = \(subViewHeight)")


        
        invitationimageView  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width, (5/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height))
        invitationimageView.image = UIImage(named:"ic_invitation.png")

        //invitationButton.backgroundColor = UIColor.grayColor()
        
        invitationButton .addSubview(invitationimageView)
        
        
        invitationLabel.frame = CGRectMake((65/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        //myEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        invitationLabel.textColor = UIColor.whiteColor()
        invitationLabel.textAlignment = NSTextAlignment.Left
        invitationLabel.text = "Invitations"
        
        
        
        invitationButton .addSubview(invitationLabel)
        
        
        
        invitationButton.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuView.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(51/568)*UIScreen.mainScreen().bounds.size.height)
        
        invitationButton.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        
        invitationButton.setTitle ("", forState: UIControlState.Normal)
        invitationButton.addTarget(self, action:"goToInvitationViewController:",forControlEvents: UIControlEvents.TouchUpInside)
        //myEssayButton.addTarget(self, action:"highLightedButton",forControlEvents: UIControlEvents.TouchUpInside)
        
        invitationView.addSubview(invitationButton)
        invitationButton.tag = 0
        
        
        // adding an image image
        lineImage2.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (184/568)*UIScreen.mainScreen().bounds.size.height, (0.75*(myMenuView.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)

        lineImage2.image = UIImage(named:"img_line.png")
        
        myMenuView .addSubview(lineImage2)
        
        
        
        //MARK: - 2.My Invitation View Teacher
        myInvitationView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (185/568)*UIScreen.mainScreen().bounds.size.height, (myMenuView.frame.width/320)*UIScreen.mainScreen().bounds.size.width, (51/568)*UIScreen.mainScreen().bounds.size.height))
        
        myInvitationView!.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        myMenuView.addSubview(myInvitationView!)
        
        
        myInvitationimageView = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width, (5/568)*UIScreen.mainScreen().bounds.size.height,  (40/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        
        myInvitationimageView.image = UIImage(named:"ic_my_invitation.png")
        
        myInvitationButton.addSubview(myInvitationimageView)
        
        
        
        myInvitationLabel.frame = CGRectMake((65/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        myInvitationLabel.textColor = UIColor.whiteColor()
        myInvitationLabel.textAlignment = NSTextAlignment.Left
        myInvitationLabel.text = "My Invitations"
        
        myInvitationButton.addSubview(myInvitationLabel)
        
        
        
        myInvitationButton.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuView.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(51/568)*UIScreen.mainScreen().bounds.size.height)
        
        myInvitationButton.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        myInvitationButton.setTitle ("", forState: UIControlState.Normal)
        myInvitationButton.addTarget(self, action:"goToMyInvitationViewController:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        
        myInvitationView?.addSubview(myInvitationButton)
        
        myInvitationButton.tag = 1
        
        // adding an image image
        lineImage3.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (236/568)*UIScreen.mainScreen().bounds.size.height, (0.75*(myMenuView.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)
        
        lineImage3.image = UIImage(named:"img_line.png")
        
        
        myMenuView .addSubview(lineImage3)
        
        
       
        
        //MARK: - 3.Completed View Teacher
        completedView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (237/568)*UIScreen.mainScreen().bounds.size.height, (myMenuView.frame.width/320)*UIScreen.mainScreen().bounds.size.width, (51/568)*UIScreen.mainScreen().bounds.size.height))
        
        completedView!.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        
        myMenuView.addSubview(completedView!)
        
        
        
        completedViewimageView  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,  (40/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        completedViewimageView.image = UIImage(named:"ic_underreview.png")
        
        
        completedButton.addSubview(completedViewimageView)
        
        completedButton.tag = 2
        
        completedViewLabel.frame = CGRectMake((65/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        completedViewLabel.textColor = UIColor.whiteColor()
        completedViewLabel.textAlignment = NSTextAlignment.Left
        completedViewLabel.text = "Completed"
        
        
        completedButton.addSubview(completedViewLabel)
        
        
        completedButton.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuView.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(51/568)*UIScreen.mainScreen().bounds.size.height)
        
        completedButton.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        completedButton.setTitle ("", forState: UIControlState.Normal)
        completedButton.addTarget(self, action:"goToCompletedView:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        completedView?.addSubview(completedButton)
        
        
        
        // adding an image image
        completedViewlineImage.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (288/568)*UIScreen.mainScreen().bounds.size.height, (0.75*(myMenuView.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)
        
        completedViewlineImage.image = UIImage(named:"img_line.png")
        
        myMenuView .addSubview(completedViewlineImage)
        
        
        
        
       
        
        //MARK: - 4.My Account View teacher
        myAccountView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (289/568)*UIScreen.mainScreen().bounds.size.height, (myMenuView.frame.width/320)*UIScreen.mainScreen().bounds.size.width, (51/568)*UIScreen.mainScreen().bounds.size.height))

        
        myAccountView!.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        
        myMenuView.addSubview(myAccountView!)
        
        
        
        myAccountimageView  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,  (40/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        myAccountimageView.image = UIImage(named:"ic_profile.png")
        
        
        myAccountButton.addSubview(myAccountimageView)
        
        myAccountButton.tag = 3
        
        myAccountLabel.frame = CGRectMake((65/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        // underReviewLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        myAccountLabel.textColor = UIColor.whiteColor()
        myAccountLabel.textAlignment = NSTextAlignment.Left
        myAccountLabel.text = "My Account"
        
        
        myAccountButton.addSubview(myAccountLabel)
        
        
        myAccountButton.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuView.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(51/568)*UIScreen.mainScreen().bounds.size.height)

        
        myAccountButton.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
       
        myAccountButton.setTitle ("", forState: UIControlState.Normal)
        myAccountButton.addTarget(self, action:"goToMyAccount:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        myAccountView?.addSubview(myAccountButton)
        
        
        
        // adding an image image
        lineImage5.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (340/568)*UIScreen.mainScreen().bounds.size.height, (0.75*(myMenuView.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)
        
        lineImage5.image = UIImage(named:"img_line.png")
        
        myMenuView .addSubview(lineImage5)
        
        
        
        
        
         //MARK: - 5.Sign Out View teacher
        signOutView =  UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (341/568)*UIScreen.mainScreen().bounds.size.height, (myMenuView.frame.width/320)*UIScreen.mainScreen().bounds.size.width, (51/568)*UIScreen.mainScreen().bounds.size.height))
        
        signOutView!.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        //signOutView?.backgroundColor = UIColor.greenColor()
        myMenuView.addSubview(signOutView!)
        
        signOutButton.frame =  CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuView.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(51/568)*UIScreen.mainScreen().bounds.size.height)
        
        signOutButton.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        //signOutButton.backgroundColor = UIColor.clearColor()
        
        
        signOutButton.setTitle ("", forState: UIControlState.Normal)
        signOutButton.addTarget(self, action:"signOut:",forControlEvents: UIControlEvents.TouchUpInside)
        
        signOutButton.tag = 4
        
        
        signOutimageView  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width, (5/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        signOutimageView.image = UIImage(named:"ic_signout.png")
        
        signOutButton.addSubview(signOutimageView)
        
        
        signOutLabel.frame = CGRectMake((65/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
       
        signOutLabel.textColor = UIColor.whiteColor()
        signOutLabel.textAlignment = NSTextAlignment.Left
        signOutLabel.text = "Sign Out"
        
        
        
        signOutButton.addSubview(signOutLabel)
        
        signOutView?.addSubview(signOutButton)
        
        
        
        
        // adding an image image
        lineImage.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (391/568)*UIScreen.mainScreen().bounds.size.height, (0.75*(myMenuView.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)
        lineImage.image = UIImage(named:"img_line.png")
        
        myMenuView .addSubview(lineImage)
        
        
        alertView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (70/568)*UIScreen.mainScreen().bounds.size.height))
        
        alertView.backgroundColor = UIColor.blackColor()

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    //MARK: - STUDENT MODULE VIEW DECLARATIONS
     
        
    //MARK: - (a)Profile Image View Student
    imageView_Student  = UIImageView(frame:CGRectMake((14/320)*UIScreen.mainScreen().bounds.size.width, (35/568)*UIScreen.mainScreen().bounds.size.height,(60/320)*UIScreen.mainScreen().bounds.size.width,(60/320)*UIScreen.mainScreen().bounds.size.width))

        
    imageView_Student.image = UIImage(named: "unknown.jpg")
    //imageView.backgroundColor = UIColor.clearColor()
    

    
    myMenuViewStudent .addSubview(imageView_Student)
    
    
    //MARK: - (b)Profile Name Label Student
    nameLabel_Student.frame = CGRectMake((95/320)*UIScreen.mainScreen().bounds.size.width,(35/568)*UIScreen.mainScreen().bounds.size.height,(150/320)*UIScreen.mainScreen().bounds.size.width,(20/568)*UIScreen.mainScreen().bounds.size.height)
        
    nameLabel_Student.textColor = UIColor.whiteColor()
    nameLabel_Student.textAlignment = NSTextAlignment.Left
    nameLabel_Student.text = "Arianda Hurley"
    nameLabel_Student.userInteractionEnabled = true
    
    
    nameLabel_Student.text = "\(firstname_menu) \(lastname_menu)"
    
    
    myMenuViewStudent .addSubview(nameLabel_Student)
    

    
    //MARK: - (c)Profile User Role Student
    titleLabel_Student.frame = CGRectMake((95/320)*UIScreen.mainScreen().bounds.size.width,(60/568)*UIScreen.mainScreen().bounds.size.height,(150/320)*UIScreen.mainScreen().bounds.size.width,(20/568)*UIScreen.mainScreen().bounds.size.height)
        
    titleLabel_Student.textColor = UIColor.whiteColor()
    titleLabel_Student.textAlignment = NSTextAlignment.Left
    titleLabel_Student.text = "Author"
    
    myMenuViewStudent.addSubview(titleLabel_Student)
    
    
    
    
    
    //MARK: - (d)Profile Back Button Student
    myProfileButton_Student.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(35/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(76/568)*UIScreen.mainScreen().bounds.size.height)
        
    myProfileButton_Student.setTitle ("", forState: UIControlState.Normal)
    myProfileButton_Student.addTarget(self, action:"goToMyAccount:",forControlEvents: UIControlEvents.TouchUpInside)
    
    
    myMenuViewStudent.addSubview(myProfileButton_Student)
    
    

    
    
    
    // adding an image image
    lineImage1_Student.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (121/568)*UIScreen.mainScreen().bounds.size.height, (0.75*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)
        
    lineImage1_Student.image = UIImage(named:"img_line.png")
    
    myMenuViewStudent .addSubview(lineImage1_Student)
    


    
    
    
        //MARK: - 1.My Essay View Student
    
        myEssayView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (122/568)*UIScreen.mainScreen().bounds.size.height, (myMenuViewStudent.frame.width/320)*UIScreen.mainScreen().bounds.size.width, (51/568)*UIScreen.mainScreen().bounds.size.height))
        
        myEssayView.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
    
    
        myMenuViewStudent .addSubview(myEssayView)
    
    
    
        myEssayimageView  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width, (5/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height))
        myEssayimageView.image = UIImage(named:"ic_myessay.png")
       
        
        myEssayButton .addSubview(myEssayimageView)
        
        
        myEssayLabel.frame = CGRectMake((65/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        //myEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        myEssayLabel.textColor = UIColor.whiteColor()
        myEssayLabel.textAlignment = NSTextAlignment.Left
        myEssayLabel.text = "My Essay"
        
        
        
        myEssayButton .addSubview(myEssayLabel)
        
        
        
        myEssayButton.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(51/568)*UIScreen.mainScreen().bounds.size.height)
        
        myEssayButton.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        
        myEssayButton.setTitle ("", forState: UIControlState.Normal)
        myEssayButton.addTarget(self, action:"goToMyMenuViewController:",forControlEvents: UIControlEvents.TouchUpInside)
        //myEssayButton.addTarget(self, action:"highLightedButton",forControlEvents: UIControlEvents.TouchUpInside)
        
        myEssayView.addSubview(myEssayButton)
        myEssayButton.tag = 0
        
        // adding an image image
        myEssayLineImage.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (172/568)*UIScreen.mainScreen().bounds.size.height, (0.75*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)
        myEssayLineImage.image = UIImage(named:"img_line.png")
        
        myMenuViewStudent .addSubview(myEssayLineImage)
        
        
        
        //MARK: - 2.New Essay View Student
        newEssayView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (173/568)*UIScreen.mainScreen().bounds.size.height, (myMenuViewStudent.frame.width/320)*UIScreen.mainScreen().bounds.size.width, (51/568)*UIScreen.mainScreen().bounds.size.height))
        
        newEssayView!.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        myMenuViewStudent.addSubview(newEssayView!)
        
        
        newEssayimageView = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width, (5/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        newEssayimageView.image = UIImage(named:"ic_newessay.png")
        
        newEssayButton.addSubview(newEssayimageView)
        
        
        
        newEssayLabel.frame = CGRectMake((65/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        newEssayLabel.textColor = UIColor.whiteColor()
        newEssayLabel.textAlignment = NSTextAlignment.Left
        newEssayLabel.text = "New Essay"
        
        newEssayButton.addSubview(newEssayLabel)
        
        
        
        newEssayButton.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(51/568)*UIScreen.mainScreen().bounds.size.height)
        
        newEssayButton.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        newEssayButton.setTitle ("", forState: UIControlState.Normal)
        newEssayButton.addTarget(self, action:"goToNewEssayViewController:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        
        newEssayView?.addSubview(newEssayButton)
        
        newEssayButton.tag = 1
        
        // adding an image image
        newEssayLineImage.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (223/568)*UIScreen.mainScreen().bounds.size.height, (0.75*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)
        
        newEssayLineImage.image = UIImage(named:"img_line.png")
        
        
        myMenuViewStudent .addSubview(newEssayLineImage)
        
        
        
        
        //MARK: - 3.UnderReview View Student
        underReviewView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (224/568)*UIScreen.mainScreen().bounds.size.height, (myMenuViewStudent.frame.width/320)*UIScreen.mainScreen().bounds.size.width, (51/568)*UIScreen.mainScreen().bounds.size.height))
        
        underReviewView!.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        
        myMenuViewStudent.addSubview(underReviewView!)
        
        
        
        underReviewimageView  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        underReviewimageView.image = UIImage(named:"ic_underreview.png")
        
        
        underReviewButton.addSubview(underReviewimageView)
        
        underReviewButton.tag = 2
        
        underReviewLabel.frame = CGRectMake((65/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        // underReviewLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        underReviewLabel.textColor = UIColor.whiteColor()
        underReviewLabel.textAlignment = NSTextAlignment.Left
        underReviewLabel.text = "Under Review"
        
        
        underReviewButton.addSubview(underReviewLabel)
        
        
        underReviewButton.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(51/568)*UIScreen.mainScreen().bounds.size.height)
        
        underReviewButton.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        //myEssayButton.textColor = UIColor.blackColor()
        underReviewButton.setTitle ("", forState: UIControlState.Normal)
        underReviewButton.addTarget(self, action:"goToUnderReview:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        underReviewView?.addSubview(underReviewButton)
        
        
        
        // adding an image image
        underReviewLineImage.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (274/568)*UIScreen.mainScreen().bounds.size.height, (0.75*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)
        
        underReviewLineImage.image = UIImage(named:"img_line.png")
        
        myMenuViewStudent .addSubview(underReviewLineImage)
        
        
        
    
        
        //MARK: - 4.Completed View Student
        completedView_Student = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (275/568)*UIScreen.mainScreen().bounds.size.height, (myMenuViewStudent.frame.width/320)*UIScreen.mainScreen().bounds.size.width, (51/568)*UIScreen.mainScreen().bounds.size.height))
        
        completedView_Student!.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        
        myMenuViewStudent.addSubview(completedView_Student!)
        
        
        
        completedViewimageView_Student  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,  (40/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height))

        
        completedViewimageView_Student.image = UIImage(named:"ic_underreview.png")
        
        
        completedButton_Student.addSubview(completedViewimageView_Student)
        
        completedButton_Student.tag = 3
        
        completedViewLabel_Student.frame = CGRectMake((65/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        // underReviewLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        completedViewLabel_Student.textColor = UIColor.whiteColor()
        completedViewLabel_Student.textAlignment = NSTextAlignment.Left
        completedViewLabel_Student.text = "Completed"
        
        
        completedButton_Student.addSubview(completedViewLabel_Student)
        
        
        completedButton_Student.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(51/568)*UIScreen.mainScreen().bounds.size.height)
        
        
        completedButton_Student.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        //myEssayButton.textColor = UIColor.blackColor()
        completedButton_Student.setTitle ("", forState: UIControlState.Normal)
        completedButton_Student.addTarget(self, action:"goToCompletedView_Student:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        completedView_Student?.addSubview(completedButton_Student)
        
        
        
        // adding an image image
        completedViewlineImage_Student.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (325/568)*UIScreen.mainScreen().bounds.size.height, (0.75*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)
        
        completedViewlineImage_Student.image = UIImage(named:"img_line.png")
        
        myMenuViewStudent .addSubview(completedViewlineImage_Student)
        
        
       
        
        //MARK: - 5.My Plan View Student
        MyPlanView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (326/568)*UIScreen.mainScreen().bounds.size.height, (myMenuViewStudent.frame.width/320)*UIScreen.mainScreen().bounds.size.width, (51/568)*UIScreen.mainScreen().bounds.size.height))
        
        MyPlanView!.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        
        myMenuViewStudent.addSubview(MyPlanView!)
        
        
        
        MyPlanimageView  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width, (5/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        MyPlanimageView.image = UIImage(named:"ic_myplan.png")
        
        
        MyPlanButton.addSubview(MyPlanimageView)
        
        MyPlanButton.tag = 4
        
        
        
        MyPlanLabel.frame = CGRectMake((65/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        //MyPlanLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        MyPlanLabel.textColor = UIColor.whiteColor()
        MyPlanLabel.textAlignment = NSTextAlignment.Left
        MyPlanLabel.text = "My Plan"
        
        
        MyPlanButton.addSubview(MyPlanLabel)
        
        
        MyPlanButton.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(51/568)*UIScreen.mainScreen().bounds.size.height)
        
        MyPlanButton.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        //myEssayButton.textColor = UIColor.blackColor()
        MyPlanButton.setTitle ("", forState: UIControlState.Normal)
        MyPlanButton.addTarget(self, action:"goToSelectPlan:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        MyPlanView?.addSubview(MyPlanButton)
        
        
        
        // adding an image image
        MyPlanlineImage.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (376/568)*UIScreen.mainScreen().bounds.size.height, (0.75*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)
        
        MyPlanlineImage.image = UIImage(named:"img_line.png")
        
        myMenuViewStudent .addSubview(MyPlanlineImage)
        
        
        
        
        
        
        
        //MARK: - 6.My Profile View Student
        myAccountView_Student = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(377/568)*UIScreen.mainScreen().bounds.size.height, (myMenuViewStudent.frame.width/320)*UIScreen.mainScreen().bounds.size.width, (51/568)*UIScreen.mainScreen().bounds.size.height))
        
        myAccountView_Student!.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        
        myMenuViewStudent.addSubview(myAccountView_Student!)
        
        
        myAccountimageView_Student  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width, (5/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        myAccountimageView_Student.image = UIImage(named:"ic_profile.png")
        
        myAccountButton_Student.addSubview(myAccountimageView_Student)
        
        myAccountButton_Student.tag = 5
        
        
        myAccountLabel_Student.frame = CGRectMake((65/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        //myAccountLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        myAccountLabel_Student.textColor = UIColor.whiteColor()
        myAccountLabel_Student.textAlignment = NSTextAlignment.Left
        myAccountLabel_Student.text = "My Account"
        
        
        myAccountButton_Student.addSubview(myAccountLabel_Student)
        
        
        myAccountButton_Student.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(51/568)*UIScreen.mainScreen().bounds.size.height)
        
        myAccountButton_Student.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        myAccountButton_Student.setTitle ("", forState: UIControlState.Normal)
        myAccountButton_Student.addTarget(self, action:"goToMyProfileViewController:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        myAccountView_Student?.addSubview(myAccountButton_Student)
        
        
        
        // adding an image image
        myAccountLineImage_Student.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (426/568)*UIScreen.mainScreen().bounds.size.height, (0.75*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)
        
        myAccountLineImage_Student.image = UIImage(named:"img_line.png")
        
        
        myMenuViewStudent .addSubview(myAccountLineImage_Student)
        
        
        //MARK: - 7.Sign Out View Student
        signOutView_Student = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(427/568)*UIScreen.mainScreen().bounds.size.height, (myMenuViewStudent.frame.width/320)*UIScreen.mainScreen().bounds.size.width, (51/568)*UIScreen.mainScreen().bounds.size.height))
        
        signOutView_Student!.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        
        myMenuViewStudent.addSubview(signOutView_Student!)
        
        signOutButton_Student.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(0.85*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width,(51/568)*UIScreen.mainScreen().bounds.size.height)
        
        signOutButton_Student.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        //myEssayButton.textColor = UIColor.blackColor()
        signOutButton_Student.setTitle ("", forState: UIControlState.Normal)
        signOutButton_Student.addTarget(self, action:"signOut_Student:",forControlEvents: UIControlEvents.TouchUpInside)
        
        signOutButton_Student.tag = 6
        
        
        signOutimageView_Student  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width, (5/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height, (40/568)*UIScreen.mainScreen().bounds.size.height))
        
        signOutimageView_Student.image = UIImage(named:"ic_signout.png")
        
        signOutButton_Student.addSubview(signOutimageView_Student)
        
        
        signOutLabel_Student.frame = CGRectMake((65/320)*UIScreen.mainScreen().bounds.size.width,(6/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        // signOutLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        signOutLabel_Student.textColor = UIColor.whiteColor()
        signOutLabel_Student.textAlignment = NSTextAlignment.Left
        signOutLabel_Student.text = "Sign Out"
        
        
        
        signOutButton_Student.addSubview(signOutLabel_Student)
        
        signOutView_Student?.addSubview(signOutButton_Student)
        
        
        
        
        // adding an image image
        signOutLineImage_Student.frame = CGRectMake((25/320)*UIScreen.mainScreen().bounds.size.width, (477/568)*UIScreen.mainScreen().bounds.size.height , (0.75*(myMenuViewStudent.frame.width)/320)*UIScreen.mainScreen().bounds.size.width, (1/568)*UIScreen.mainScreen().bounds.size.height)
        
        signOutLineImage_Student.image = UIImage(named:"img_line.png")
        
        myMenuViewStudent .addSubview(signOutLineImage_Student)
        
        
        alertView_Student = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (70/568)*UIScreen.mainScreen().bounds.size.height))
        
        alertView_Student.backgroundColor = UIColor.blackColor()

        
    }
    
    
    
    
    
    
    
    //MARK: - TEACHER MODULE VIEW DEFINITIONS
    func goToInvitationViewController(sender: UIButton!)
    {
        
             println("invitation view")
        
             highLightBtn(sender)
        
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let invitationVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("InvitationsViewControllerSegue")as! InvitationsViewController
            
            currentVC.navigationController?.pushViewController(invitationVC, animated: true)
    }

    
  
    
    func goToMyInvitationViewController(sender: UIButton!)
    {
        highLightBtn(sender)
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let myInvitationVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("MyInvitationsViewControllerSegue") as! MyInvitationsViewController
        
        currentVC.navigationController?.pushViewController(myInvitationVC, animated: true)
        
    }
    
    
    func goToCompletedView(sender: UIButton!)
    {
        highLightBtn(sender)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let completedVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("CompletedViewControllerSegue") as! CompletedViewController
        
        currentVC.navigationController?.pushViewController(completedVC, animated: true)
    }
    
    
    
    func goToMyAccount(sender: UIButton!)
    {
        highLightBtn(sender)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let myAccountVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("MyProfileViewControllerSegue") as! MyProfileViewController
        
        currentVC.navigationController?.pushViewController(myAccountVC, animated: true)
    }

    
    
    func signOut(sender: UIButton!)
    {
        
        var btnpressed: Bool
        btnpressed = true
        
        println("Sign Out Button Clicked")
        
        highLightBtn(sender)
        
        if (btnpressed)
        {
            
            alert = UIAlertView(title: "", message: "Do you want to sign out?", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Sign Out")
        
            let signOutBtn = UIAlertAction(title:"Sign Out", style: .Default)
            {
                (action: UIAlertAction!) -> Void in
            }
           
            let cancelBtn = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel)
            {
                (action: UIAlertAction!) -> Void in
            }
            alert.show()
        
            alertView.addSubview(alert)
        }
    }
    
    
    //Sign Out alertView function
    func alertView(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int)
    {
        
        if (buttonIndex == 1)
        {
            println("sign out btn clicked")
            
            let signOut = currentVC.storyboard!.instantiateViewControllerWithIdentifier("LoginViewControllerSegue") as! LoginViewController
            
            currentVC.navigationController?.popToRootViewControllerAnimated(false)
        }
        else
        {
            println("cancel btn clicked")
        }
    }
    

    
    
    
    // HIGHLIGHTED VIEW IN BLUE
    func highLightBtn(sender: UIButton)
    {
        if sender.tag != 4
        {
            var images = [String]()
            images = ["ic_invitation.png", "ic_my_invitation.png", "ic_underreview.png", "ic_profile.png", "ic_signout.png"]
            
            var images_hover = [String]()
            images_hover = ["ic_invitation_hover.png", "ic_my_invitation_hover.png","ic_underreview_hover.png", "ic_profile_hover.png", "ic_signout_hover.png"]
            
            
            var images_icon = [UIImageView]()
            images_icon = [invitationimageView,myInvitationimageView,completedViewimageView,myAccountimageView,signOutimageView]
            
            
            
            
            for vw in myMenuView.subviews as! [UIView]
            {
                
                for svw in vw.subviews as! [UIView]
                {
                    if svw.isKindOfClass(UIButton)
                    {
                        //println("true")
                        if svw == sender
                        {
                            
                            println("hover images")
                            
                            println(images_hover[svw.tag])
                            
                            svw.backgroundColor = UIColor(red: 35/255.0, green: 158/255.0, blue: 219/255.0, alpha: 1.0)
                            images_icon[svw.tag].image = UIImage(named: images_hover[svw.tag] as String)
                        }
                        else
                        {
                            println("images icon")
                            
                            svw.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
                            images_icon[svw.tag].image = UIImage(named: images[svw.tag] as String)
                        }
                    }
                }
            }
        }

    }
    
    
    
    

    //MARK: - STUDENT MODULE VIEW DEFINITIONS
    func goToMyMenuViewController(sender: UIButton!)
    {
        highLightBtn_Student(sender)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let myMenuVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("MyMenuViewControllerSegue") as! MyMenuViewController
        
        currentVC.navigationController?.pushViewController(myMenuVC, animated: true)
    }
    
    
    
    
    func goToNewEssayViewController(sender: UIButton!)
    {
        highLightBtn_Student(sender)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let newEssayVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("NewEssay_MenuViewControllerSegue") as! NewEssay_MenuViewController
        
        currentVC.navigationController?.pushViewController(newEssayVC, animated: true)
    }
    
    
    func goToUnderReview(sender: UIButton!)
    {
        highLightBtn_Student(sender)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let underReviewVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("UnderReviewsViewControllerSegue") as! UnderReviewsViewController
        
        currentVC.navigationController?.pushViewController(underReviewVC, animated: true)
    }
    
    
    
    func goToCompletedView_Student(sender: UIButton!)
    {
        highLightBtn_Student(sender)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let completedVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("CompletedViewControllerSegue") as! CompletedViewController
        
        currentVC.navigationController?.pushViewController(completedVC, animated: true)
    }
    
    
    func goToSelectPlan(sender: UIButton!)
    {
        highLightBtn_Student(sender)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let selectPlanVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("SelectPlanViewControllerSegue") as! SelectPlanViewController
        
        currentVC.navigationController?.pushViewController(selectPlanVC, animated: true)
    }
    
    
    func goToMyProfileViewController(sender: UIButton!)
    {
        highLightBtn_Student(sender)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let myProfileVC = currentVC.storyboard!.instantiateViewControllerWithIdentifier("MyProfileViewControllerSegue") as! MyProfileViewController
        
        currentVC.navigationController?.pushViewController(myProfileVC, animated: true)
    }
    
    

    
    
    func signOut_Student(sender: UIButton!)
    {
        
        var btnpressed: Bool
        btnpressed = true
        
        println("Sign Out Button Clicked")
        
        highLightBtn_Student(sender)
        
        if (btnpressed)
        {
            
            alert_Student = UIAlertView(title: "", message: "Do you want to sign out?", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Sign Out")
            
            let signOutBtn_Student = UIAlertAction(title:"Sign Out", style: .Default)
            {
                    (action: UIAlertAction!) -> Void in
            }
            
            let cancelBtn_Student = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel)
            {
                    (action: UIAlertAction!) -> Void in
            }
            alert_Student.show()
            
            alertView_Student.addSubview(alert_Student)
        }
    }
    
    
    
    
    //Sign Out alertView function
    func alertView_Student(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int)
    {
        
        if (buttonIndex == 1)
        {
            println("sign out btn clicked")
            
            let signOut = currentVC.storyboard!.instantiateViewControllerWithIdentifier("LoginViewControllerSegue") as! LoginViewController
            
            currentVC.navigationController?.popToRootViewControllerAnimated(false)
        }
        else
        {
            println("cancel btn clicked")
        }
    }
    
    
    
    
    // HIGHLIGHTED VIEW IN BLUE
    func highLightBtn_Student(sender: UIButton)
    {
        if sender.tag != 6
        {
            var images = [String]()
            images = ["ic_myessay.png", "ic_newessay.png", "ic_underreview.png","ic_underreview.png", "ic_myplan.png", "ic_profile.png", "ic_signout.png"]
            
            var images_hover = [String]()
            images_hover = ["ic_myessay_hover.png", "ic_newessay_hover.png","ic_underreview_hover.png" ,"ic_underreview_hover.png", "ic_myplan_hover.png", "ic_profile_hover.png", "ic_signout_hover.png"]
            
            
            var images_icon = [UIImageView]()
            images_icon = [myEssayimageView,newEssayimageView, underReviewimageView,completedViewimageView_Student, MyPlanimageView, myAccountimageView_Student, signOutimageView_Student]
            
            
            
            
            for vw in myMenuViewStudent.subviews as! [UIView]
            {
                
                for svw in vw.subviews as! [UIView]
                {
                    if svw.isKindOfClass(UIButton)
                    {
                        //println("true")
                        if svw == sender
                        {
                            svw.backgroundColor = UIColor(red: 35/255.0, green: 158/255.0, blue: 219/255.0, alpha: 1.0)
                            images_icon[svw.tag].image = UIImage(named: images_hover[svw.tag] as String)
                        }
                        else
                        {
                            svw.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
                            images_icon[svw.tag].image = UIImage(named: images[svw.tag] as String)
                        }
                    }
                }
            }

        }
        
    
    }
    
    
    
    

   

    //MARK: - applicationWillResignActive()
    func applicationWillResignActive(application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    
     //MARK: - applicationDidEnterBackground()
    func applicationDidEnterBackground(application: UIApplication)
    {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    
     //MARK: - applicationWillEnterForeground()
    func applicationWillEnterForeground(application: UIApplication)
    {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    
     //MARK: - applicationDidBecomeActive()
    func applicationDidBecomeActive(application: UIApplication)
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    
    
     //MARK: - applicationWillTerminate()
    func applicationWillTerminate(application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}



