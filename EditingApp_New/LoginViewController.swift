//
//  LoginViewController.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/3/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


    var urlData: NSDictionary!
    var namesArray: NSArray! = []


    var profileUrl: String = String()
    var user_id = 0


    var passwordString = ""
    var firstname_menu: String!
    var lastname_menu: String!

class LoginViewController: UIViewController, UITextFieldDelegate,UIAlertViewDelegate
{

    @IBOutlet weak var mySubViewInLogin: UIView!
    
    @IBOutlet weak var user_lbl: UILabel!
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var passwordLbl: UILabel!
    
    
    @IBOutlet weak var mySubViewInlogin2: UIView!
    @IBOutlet weak var loadingMsg: UILabel!
    @IBOutlet weak var backGroundImage: UIImageView!
   
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var pswrdImage: UIImageView!
    @IBOutlet weak var loginBtn_lbl: UIButton!
   
    @IBOutlet weak var userName_lbl: UITextField!
    @IBOutlet weak var pswrd_lbl: UITextField!
    
   
    
    //MARK: - iPadCompatibility() method
    func iPadCompatibility()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            loginBtn_lbl.titleLabel?.font = UIFont.systemFontOfSize(26)
            loginBtn_lbl.frame = CGRectMake(420, 600, 200, 55)

            userImage.frame = CGRectMake(110, 440, 55, 55)
            pswrdImage.frame = CGRectMake(110, 520, 55, 55)
            
            user_lbl.frame = CGRectMake(160, 440, 10, 55)
            passwordLbl.frame = CGRectMake(160, 520, 10, 55)

            userNameField.frame = CGRectMake(170, 440, 450, 55)
            passwordField.frame = CGRectMake(170, 520, 450, 55)
            
            userNameField.font = UIFont.systemFontOfSize(22)
            passwordField.font = UIFont.systemFontOfSize(22)

        }
        
    }

    
    
    //MARK: - LOGIN ACTION
    @IBAction func loginButton(sender: AnyObject)
    {
        
        passwordString = "\(passwordField.text)" as String
        
        println("password = \(passwordString)")

        
         if ((userNameField.text == "") || (passwordField.text == ""))
         {
            
            var loginAlert = UIAlertController(title: "Error..!", message: "Enter valid username and password Fields", preferredStyle: UIAlertControllerStyle.Alert)
            
            loginAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            self.presentViewController(loginAlert, animated: true, completion: nil)
            

         }
         
            
        else
        {
            
            var subView:UIView = UIView()
            subView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
            subView.backgroundColor = UIColor.blackColor()
            subView.alpha = 0.75
            self.view.addSubview(subView)
            
            let activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle:UIActivityIndicatorViewStyle.White)
            subView.addSubview(activityIndicator)
            
            var x = ((subView.frame.width/2)-(activityIndicator.frame.width/2))
            println(x)
            var y = ((subView.frame.height/2)-(activityIndicator.frame.height/2))
            println(y)
            
            activityIndicator.frame = CGRectMake(x, y, activityIndicator.frame.width,activityIndicator.frame.height)
            
            activityIndicator.startAnimating()
            
           var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=login&username=\(userNameField.text)&password=\(passwordField.text)"
            
           println(urlPath)
            
           let url: NSURL = NSURL(string: urlPath.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: nil, range: nil))!
           let session = NSURLSession.sharedSession()
        
           let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            
            if error != nil
            {
                
                subView.hidden = true
                //activityIndicator.stopAnimating()
                //self.loadingMsg.hidden = true
                
                // If there is an error in the web request, print it to the console
                println(error.localizedDescription)
            }
            println(data)
            
            
            
            var err: NSError?
            
            urlData = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
            
            println(urlData)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                subView.hidden = true
                
                
                if (urlData != nil)
                {
                    
                    var success = urlData.valueForKey("status")as! String
                    println(success)
                    
                    
                    if(success == "Success")
                    {
                        println("Login successful")
                        

                        
                        firstname_menu = urlData.valueForKey("firstname") as! String
                        println("firstName: \(firstname_menu)")
                        
                        lastname_menu = urlData.valueForKey("lastname") as! String
                        println("lastName: \(lastname_menu)")
                        
                        var role: String = urlData.valueForKey("role") as! String
                        println("role: \(role)")
                        
                        currentUserRole = role
                        
                        user_id = urlData.valueForKey("id") as! Int
                        //println(user_id)

                        profileUrl = urlData.valueForKey("picture") as! String
                        
                        
                        
                        println("userId: \(profileUrl)")
                        
                        var defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
                        defaults.setObject(user_id, forKey: "id")
                        
                        defaults.setObject(firstname_menu, forKey: "firstname")
                        
                        defaults.setObject(role, forKey: "role")
                        
                        defaults.setObject(lastname_menu, forKey: "lastname")
                        defaults.synchronize()

                        var url: NSURL = NSURL(string: profileUrl)!
                  
                        if currentUserRole == "teacher"
                        {
                            myMenuView.frame.size.width = (0.85)*self.view.frame.width
                            myMenuView.frame.size.height = self.view.frame.height
                            
                            var subframe:UIView!
                            
                            for menusubview in myMenuView.subviews
                            {
                                if(menusubview is UIView)
                                {
                                    subframe = menusubview as? UIView
                                }
                                if(menusubview is UIButton)
                                {
                                    subframe = menusubview as? UIButton
                                }
                                if(menusubview.frame.width>myMenuView.frame.width)
                                {
                                    subframe.frame.size.width = myMenuView.frame.width
                                }
                            }
                            
                        }
                        else
                        {
                            myMenuViewStudent.frame.size.width = (0.85)*self.view.frame.width
                            myMenuViewStudent.frame.size.height = self.view.frame.height
                            
                            var subframe:UIView!
                            
                            for menusubview in myMenuViewStudent.subviews
                            {
                                if(menusubview is UIView)
                                {
                                    subframe = menusubview as? UIView
                                }
                                if(menusubview is UIButton)
                                {
                                    subframe = menusubview as? UIButton
                                }
                                if(menusubview.frame.width>myMenuViewStudent.frame.width)
                                {
                                    subframe.frame.size.width = myMenuViewStudent.frame.width
                                }
                            }
                            
                            
                        }
                      
                        
                        if currentUserRole == "teacher"
                        {
                            
                            
                            if let data = NSData(contentsOfURL: url)
                            {
                                imageView.image = UIImage(data: data)!
                            }
                            
                            
                            nameLabel.text = "\(firstname_menu) \(lastname_menu)"
                            
                            titleLabel.text = "\(role)"
                            
                            var selectBtn: AppDelegate = AppDelegate()
                            selectBtn.highLightBtn(invitationButton)
                            
                            let loginVC = self.storyboard!.instantiateViewControllerWithIdentifier("InvitationsViewControllerSegue") as! InvitationsViewController
                            self.navigationController?.pushViewController(loginVC, animated: true)
                            
                        }
                        else
                        {
                        
                            if let data = NSData(contentsOfURL: url)
                            {
                                imageView_Student.image = UIImage(data: data)!
                            }
                            
                            
                            //imageView.image = UIImage(named:"profileUrl")
                            
                            nameLabel_Student.text = "\(firstname_menu) \(lastname_menu)"
                            
                            titleLabel_Student.text = "\(role)"
                            
                            var selectBtn: AppDelegate = AppDelegate()
                            selectBtn.highLightBtn_Student(myEssayButton)
                            
                            let loginVC = self.storyboard!.instantiateViewControllerWithIdentifier("MyMenuViewControllerSegue") as! MyMenuViewController
                            self.navigationController?.pushViewController(loginVC, animated: true)

                        }

                        
                    }
                        
                    else
                    {
                        
                         var alert = UIAlertView(title: "Error..!", message: "Enter valid username and password", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                        
                            let okBtn = UIAlertAction(title:"OK", style: .Default)
                            {
                                (action: UIAlertAction!) -> Void in
                            }
                        
                        
                        alert.show()
                        
                        subView.hidden = true

                    }
                    
                }
                else
                {
                    var alert = UIAlertView(title: "Internet Error..!", message: "Please check your Internet Settings", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                    {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    alert.show()

                    subView.hidden = true
                }


            })
            
            
            })
            task.resume()
      }
        
  }
    
   
    
    
    //MARK: - viewDidLoad() method
    override func viewDidLoad()
    {
         super.viewDidLoad()

         userNameField.becomeFirstResponder()
        
        
          iPadCompatibility()             //for ipad compatibility
        
          var userDefaults = NSUserDefaults.standardUserDefaults()
          if var user_id = NSUserDefaults.standardUserDefaults().objectForKey("id") as? String
          {
            userNameField.text = user_id as String
            println(userNameField)
          }

    }
    
    
    
     //MARK: - viewWillAppear() method
     override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        
       
        //to hide navigation bar
        self.navigationController?.navigationBar.hidden = true
        
        
        // to hide status bar
        UIApplication.sharedApplication().statusBarHidden = true
        
        nameLabel.text = "\(firstname_menu) \(lastname_menu)"
        
        nameLabel_Student.text = "\(firstname_menu) \(lastname_menu)"
        
    }
    
    
    
     //MARK: - UITextFieldDelegate() method
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField .resignFirstResponder()
        return true
    }
      

    //MARK: - didReceiveMemoryWarning() method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
