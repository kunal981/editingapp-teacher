//
//  NewEssay_MenuViewController.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/30/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class NewEssay_MenuViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate,UIAlertViewDelegate
{

    var isChecked:Bool = true
    var isHighLighted:Bool = false
    
    @IBOutlet weak var uploadPDF: UILabel!
    @IBOutlet weak var menuBarBtn: UIButton!
    
    @IBOutlet weak var plus_lbl: UIButton!
    @IBOutlet weak var titleWrapper: UIView!
    @IBOutlet weak var titleTextField: UITextField!
    
    
    @IBOutlet weak var headingView: UIView!
    @IBOutlet weak var articleTextView: UITextView!
    
    
    @IBOutlet weak var createNew_lbl: UILabel!
    @IBOutlet weak var addCommentTextView: UITextView!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var sendToTeacher_lbl: UILabel!
    
    @IBOutlet weak var rightAlignment: UIButton!
    @IBOutlet weak var leftAlignment: UIButton!
    @IBOutlet weak var centerAlignment: UIButton!
    @IBOutlet weak var boldBtn: UIButton!
    
    @IBOutlet weak var preview_btn: UIButton!
    @IBOutlet weak var italicBtn: UIButton!
    
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var submit_btn: UIButton!
    
    
    @IBOutlet weak var barButton: UIButton!
    @IBOutlet weak var check_btn: UIButton!
    var x:CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    var subView:UIView = UIView()
    
    @IBOutlet weak var preview_lbl: UIButton!
    
    @IBOutlet weak var submit_lbl: UIButton!
    
    //MARK: - menuBtnPressed() method
    @IBAction func menuBtnPressed(sender: AnyObject)
    {
        
        if currentUserRole == "teacher"
        {
            if(x == -myMenuView.frame.size.width)
            {
                //navBar.frame.origin.x = 0
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = 0
                navx = myMenuView.frame.size.width
            }
                
            else
            {
                //navBar.frame.origin.x = myMenuView.frame.size.width
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = -myMenuView.frame.size.width
                navx = 0
            }
            
            
            UIView.animateWithDuration(0.5, animations: {
                
                self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
            })
        }
        else
        {
            if(x == -myMenuViewStudent.frame.size.width)
            {
                //navBar.frame.origin.x = 0
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = 0
                navx = myMenuViewStudent.frame.size.width
            }
                
            else
            {
                //navBar.frame.origin.x = myMenuView.frame.size.width
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = -myMenuViewStudent.frame.size.width
                navx = 0
            }
            
            
            UIView.animateWithDuration(0.5, animations: {
                
                self.navBar.frame.origin.x = self.navx
                myMenuViewStudent.frame.origin.x = self.x
                
            })
        }
        
    }
    
    //MARK: - UITextField Delegates() method
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        titleTextField.resignFirstResponder()
        return true
    }
    
    
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        
        println("editing starts")
        
        var placeholderText = ""
        if textView.tag == 1
        {
            placeholderText = "Write your article here"
            
            articleTextView.frame.size.height -= 30
            
        }
        
        if textView.tag == 2
        {
            placeholderText = "Add a comment"
            
            self.view.frame.origin.y -= 120
        }
        else
        {
            self.view.frame.origin.y = 0
        }
        
        
        
        if textView.text == placeholderText
        {
            textView.text = ""
        }
        
        return true
    }
    
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool
    {
        println("end editing")
        
        var placeholderText = ""
        if textView.tag == 1
        {
            placeholderText = "Write your article here"
            
            articleTextView.frame.size.height += 30
        }
        
        if textView.tag == 2
        {
            placeholderText = "Add a comment"
            
            self.view.frame.origin.y = 0
        }
        
        if textView.text == ""
        {
            textView.text = placeholderText
        }

        return true
    }
    
    
    
    //MARK: - iPadCompatibility() method
    func iPadCompatibility()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            barButton.frame = CGRectMake(0, 0, 45, 45)
            
            preview_lbl.titleLabel?.font = UIFont.systemFontOfSize(22)
            submit_lbl.titleLabel?.font = UIFont.systemFontOfSize(22)
            
            sendToTeacher_lbl.font = UIFont.systemFontOfSize(20)

            titleTextField.font = UIFont.systemFontOfSize(20)

            articleTextView.font = UIFont.systemFontOfSize(20)
            
             addCommentTextView.font = UIFont.systemFontOfSize(20)
            
            createNew_lbl.font = UIFont.systemFontOfSize(20)
            
            plus_lbl.frame = CGRectMake(5, 2, 32, 32)


            
        }
        else
        {
            
            headingView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 10
            
            titleWrapper.frame.origin.y = headingView.frame.origin.y + headingView.frame.height + 10
            //titleTextField.frame.origin.y = headingView.frame.origin.y + headingView.frame.height + 10
            
            articleTextView.frame.origin.y = titleWrapper.frame.origin.y + titleWrapper.frame.height + 10
            
            addCommentTextView.frame.origin.y = articleTextView.frame.origin.y + articleTextView.frame.height + 10
            
            check_btn.frame.origin.y = addCommentTextView.frame.origin.y + addCommentTextView.frame.height + 10
            sendToTeacher_lbl.frame.origin.y = addCommentTextView.frame.origin.y + addCommentTextView.frame.height + 10
            
            preview_btn.frame.origin.y = check_btn.frame.origin.y + check_btn.frame.height + 10
            submit_btn.frame.origin.y = check_btn.frame.origin.y + check_btn.frame.height + 10
            

            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                
                
            }
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                
            }
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                barButton.frame = CGRectMake(0, 0, 35, 35)
            }
                
            else
            {
                
            }

        }
        
    }
    

    //MARK: - viewDidLoad() method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        titleTextField.delegate = self
        articleTextView.delegate = self
        addCommentTextView.delegate = self
        
        //STATUS BAR COLOR
        let statusBarView:UIView = UIView()
        statusBarView.frame = CGRectMake(0, 0, self.view.frame.width, 20)
        statusBarView.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 0.85)
        self.view.addSubview(statusBarView)
        
        navBar.barTintColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1)
        
        
        //newEssayimageView.image = UIImage(named: "ic_newessay.png")
        
        iPadCompatibility()
        
        currentVC = self
        //x = -320
        if currentUserRole == "teacher"
        {
            self.view.addSubview(myMenuView)
            
            myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
            x = -myMenuView.frame.size.width
        }
        else
        {
            self.view.addSubview(myMenuViewStudent)
            
            myMenuViewStudent.frame = CGRectMake(-myMenuViewStudent.frame.size.width, self.view.frame.origin.y,myMenuViewStudent.frame.size.width ,myMenuViewStudent.frame.size.height)
            x = -myMenuViewStudent.frame.size.width
        }
        
        
        
        titleWrapper.layer.borderWidth = 1.5
        titleWrapper.layer.borderColor = UIColor.lightGrayColor().CGColor
        titleWrapper.layer.masksToBounds = true
        
        titleTextField.textColor = UIColor.blackColor()
        //titleTextField.placeholder = "Write your title here"
        
        titleTextField.attributedPlaceholder = NSAttributedString(string:"Write your title here",
            attributes:[NSForegroundColorAttributeName: UIColor.blackColor()])
    
        
        articleTextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        articleTextView.layer.borderWidth = 1.5
        articleTextView.layer.masksToBounds = true
        
        addCommentTextView.layer.borderColor = UIColor.lightGrayColor().CGColor

        
        headingView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 10
        
        titleWrapper.frame.origin.y = headingView.frame.origin.y + headingView.frame.height + 10
       // titleTextField.frame.origin.y = headingView.frame.origin.y + headingView.frame.height + 10
        
        articleTextView.frame.origin.y = titleWrapper.frame.origin.y + titleWrapper.frame.height + 10
        
        addCommentTextView.frame.origin.y = articleTextView.frame.origin.y + articleTextView.frame.height + 10
        check_btn.frame.origin.y = addCommentTextView.frame.origin.y + addCommentTextView.frame.height + 10
        sendToTeacher_lbl.frame.origin.y = addCommentTextView.frame.origin.y + addCommentTextView.frame.height + 10
        
        preview_btn.frame.origin.y = check_btn.frame.origin.y + check_btn.frame.height + 10
        submit_btn.frame.origin.y = check_btn.frame.origin.y + check_btn.frame.height + 10

        
    }
    
    
    
       
    //MARK: - previewButton() method
    @IBAction func previewButton(sender: AnyObject)
    {
        
    }
    
    
    
    //MARK: - submitButton() method
    @IBAction func submitButton(sender: AnyObject)
    {
        
        
        if ((titleTextField.text == "") || (articleTextView.text == "") || (addCommentTextView.text == ""))
        {
            
            
            var refreshAlert = UIAlertController(title: "Error..!", message: "Mandatory Fields.!! Enter Values", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                
            }))
            self.presentViewController(refreshAlert, animated: true, completion: nil)
            
        }

        
               
        else
        {

            //var subView:UIView = UIView()
            subView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
            subView.backgroundColor = UIColor.blackColor()
            subView.alpha = 0.75
            self.view.addSubview(subView)
            
            let activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle:UIActivityIndicatorViewStyle.White)
            subView.addSubview(activityIndicator)
            var x = (subView.frame.width/2)-(activityIndicator.frame.width/2)
            println(x)
            var y = ((subView.frame.height/2)-(activityIndicator.frame.height/2))
            println(y)
            
            activityIndicator.frame = CGRectMake(x, y, activityIndicator.frame.width,activityIndicator.frame.height)
            
            activityIndicator.startAnimating()
            
            
            
            // write new Essay api
            
            var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=writeessay"
            
            println(urlPath)
            
            var post:NSString = "uid=\(user_id)&title=\(titleTextField.text)&content=\(articleTextView.text)&assign=false&comment=\(addCommentTextView.text)"
            
            NSLog("PostData: %@",post);
            
            var url1:NSURL = NSURL(string: urlPath)!
            
            var postData:NSData = post.dataUsingEncoding(NSASCIIStringEncoding)!
            
            var postLength:NSString = String( postData.length )
            
            var request:NSMutableURLRequest = NSMutableURLRequest(URL: url1)
            request.HTTPMethod = "POST"
            request.HTTPBody = postData
            request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            
            
            var reponseError: NSError?
            var response: NSURLResponse?
            
            var urlData: NSData? = NSURLConnection.sendSynchronousRequest(request, returningResponse:&response, error:&reponseError)
            
            if ( urlData != nil )
            {
                
                let res = response as! NSHTTPURLResponse!;
                
                NSLog("Response code: %ld", res.statusCode);
                
                if (res.statusCode >= 200 && res.statusCode < 300)
                {
                    var responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                    
                    NSLog("Response ==> %@", responseData);
                    
                    var error: NSError?
                    
                    var writeEssay = NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers , error: &error) as! NSDictionary
                    
                    
                    var success:String = writeEssay.valueForKey("status") as! String
                    
                    
                    println(success)
                    
                  
                    
                    if(success == "Success")
                    {
                        var alert = UIAlertView(title: "Created..!", message: "Essay Created Successfully", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                        
                        let okBtn = UIAlertAction(title:"OK", style: .Default)
                        {
                                (action: UIAlertAction!) -> Void in
                            
                            //self.subView.hidden=true
                        }
                        
                        
                        alert.show()
                        
                       // subView.hidden=true
                        
                        println("CREATED SUCCESSFUL")
                        
                    }
                    else
                    {
                        
                        var alert = UIAlertView(title: "Error", message: "Essay Not Created..!", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                        
                        let okBtn = UIAlertAction(title:"OK", style: .Default)
                        {
                                (action: UIAlertAction!) -> Void in
                        }
                        
                        
                        alert.show()
                        
                    }
                    
                }
                else
                {
                    //subView.hidden = true
                    println("Login failed2")
                    
                }
            }
            else
            {
                
                var alert = UIAlertView(title: "Internet Error..!", message: "Please check your Internet Settings", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                
                let okBtn = UIAlertAction(title:"OK", style: .Default)
                {
                        (action: UIAlertAction!) -> Void in
                }
                
                
                alert.show()
                
            }
            
        }
        
    }
    
    
    
    
    //MARK: - UIAlertViewDelegate() method
    func alertView(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int)
    {
        
        if (buttonIndex == 0)
        {
            println("ok btn clicked")
            
            subView.hidden=true
            
            let myEssayVC = self.storyboard!.instantiateViewControllerWithIdentifier("MyMenuViewControllerSegue")as! MyMenuViewController
            
            self.navigationController?.pushViewController(myEssayVC, animated: true)
        }
        else
        {
            println("cancel btn clicked")
        }
    }

    
    
    
    
    //MARK: - check_button() method
    @IBAction func check_button(sender: AnyObject)
    {
        let selectedImage = UIImage(named: "check-box.png")
        let unselectedImage = UIImage(named: "checkbox.png")
        
        if isChecked == false
        {
            isChecked = true
            check_Button.setImage(selectedImage, forState: UIControlState.Normal)
        }
        else
        {
            isChecked = false
            check_Button.setImage(unselectedImage, forState: UIControlState.Normal)
        }

    }
    @IBOutlet weak var check_Button: UIButton!
    
    
    
    
    
    //MARK: - hideKeyPad() method
    @IBAction func hideKeyPad(sender: AnyObject)
    {
        titleTextField.resignFirstResponder()
        articleTextView .resignFirstResponder()
        addCommentTextView.resignFirstResponder()
    }
    
    
//    @IBAction func textLeftAligned_btn(sender: AnyObject)
//    {
//        
//        
//        dispatch_async(dispatch_get_main_queue(), {
//            
//            if self.isHighLighted == false
//            {
//                self.leftAlignment.highlighted = true;
//                self.isHighLighted = true
//                
//                self.articleTextView.textAlignment = NSTextAlignment.Left
//            }
//            else
//            {
//                self.leftAlignment.highlighted = false;
//                self.isHighLighted = false
//                
//                self.articleTextView.textAlignment = NSTextAlignment.Natural
//            }
//        })
//        
//        
//    }
//    
//    
//    @IBAction func textCenterAligned_btn(sender: AnyObject)
//    {
//        
//        dispatch_async(dispatch_get_main_queue(), {
//            
//            if self.isHighLighted == false
//            {
//                self.centerAlignment.highlighted = true;
//                self.isHighLighted = true
//                
//                self.articleTextView.textAlignment = NSTextAlignment.Center
//            }
//            else
//            {
//                self.centerAlignment.highlighted = false;
//                self.isHighLighted = false
//                
//                self.articleTextView.textAlignment = NSTextAlignment.Natural
//            }
//        })
//        
//        
//        
//    }
//    
//    @IBAction func textItalic_Btn(sender: AnyObject)
//    {
//        
//        
//        dispatch_async(dispatch_get_main_queue(), {
//            
//            if self.isHighLighted == false
//            {
//                self.italicBtn.highlighted = true;
//                self.isHighLighted = true
//                
//                self.articleTextView.font = UIFont.italicSystemFontOfSize(15)
//            }
//            else
//            {
//                self.italicBtn.highlighted = false;
//                self.isHighLighted = false
//                
//                self.articleTextView.font=UIFont.systemFontOfSize(15)
//            }
//        })
//        
//    }
//    
//    @IBAction func textBold_Btn(sender: AnyObject)
//    {
//        
//        dispatch_async(dispatch_get_main_queue(), {
//            
//            if self.isHighLighted == false
//            {
//                self.boldBtn.highlighted = true;
//                self.isHighLighted = true
//                
//                self.articleTextView.font = UIFont.boldSystemFontOfSize(15)
//            }
//            else
//            {
//                self.boldBtn.highlighted = false;
//                self.isHighLighted = false
//                
//                self.articleTextView.font=UIFont.systemFontOfSize(15)
//                
//            }
//        })
//        
//    }
//    
//    
//    @IBAction func textRightAligned_Btn(sender: AnyObject)
//    {
//        
//        dispatch_async(dispatch_get_main_queue(), {
//            
//            if self.isHighLighted == false
//            {
//                self.rightAlignment.highlighted = true;
//                self.isHighLighted = true
//                
//                self.articleTextView.textAlignment = NSTextAlignment.Right
//            }
//            else
//            {
//                self.rightAlignment.highlighted = false;
//                self.isHighLighted = false
//                
//                self.articleTextView.textAlignment = NSTextAlignment.Natural
//            }
//        })
//        
//        
//    }
    
    
    
    //MARK: - didReceiveMemoryWarning() method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

       

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
