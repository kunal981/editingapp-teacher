//
//  UnderReviewTableViewCell.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/12/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class MyInvitationsTableViewCell: UITableViewCell
{

    var cellView: UIView = UIView()
    var view: UIView = UIView()
    var colorLabel : UILabel = UILabel()
    var countLabel: UILabel = UILabel()
    var text_Label: UILabel = UILabel()
    var assignedTo_Label: UILabel = UILabel()
    var watchImage: UIImageView = UIImageView()
    var date_Label: UILabel = UILabel()
    var accessoryImage: UIButton = UIButton()
    var backBtn: UIButton = UIButton()
    var PostStatus: UILabel = UILabel()
    
    
    var edit_btn:UIButton = UIButton()
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        contentView.addSubview(cellView)
        contentView.addSubview(backBtn)
        view.addSubview(text_Label)
        view.addSubview(PostStatus)
        view.addSubview(assignedTo_Label)
        view.addSubview(watchImage)
        view.addSubview(date_Label)
        view.addSubview(accessoryImage)
        
        cellView.addSubview(colorLabel)
        cellView.addSubview(countLabel)
        cellView.addSubview(view)
        
        
        
        cellView.addSubview(edit_btn)
       
        
        
        
        var contentDictionary = ["color": colorLabel,
            "count": countLabel,
            "text": text_Label,
             "status": PostStatus,
            "task assigned": assignedTo_Label,
            "image": watchImage,
            "date": date_Label,
            "ButtonImage": accessoryImage,
            "editButton": edit_btn]
    
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            text_Label.font = UIFont.systemFontOfSize(20)
            PostStatus.font = UIFont.systemFontOfSize(18)
            assignedTo_Label.font = UIFont.boldSystemFontOfSize(18)
            //date_Label.font = UIFont.systemFontOfSize(18)
            
            countLabel.frame = CGRectMake(20, 0, 40, cellView.frame.height)
            
            edit_btn.frame = CGRectMake(560, 15, 150, 50)
            
            assignedTo_Label.frame = CGRectMake(90, 55, 100, 25)
            
            //watchImage.frame = CGRectMake(200, 55, 26, 26)
            //date_Label.frame = CGRectMake(230, 55, 200, 25)
            
            edit_btn.titleLabel?.font = UIFont.systemFontOfSize(22)
            
            countLabel.font = UIFont.boldSystemFontOfSize(20)
            
        }
        
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                assignedTo_Label.frame = CGRectMake(50, 60, 130, 15)
                
                PostStatus.frame  = CGRectMake(10, 60, 60, 15)
                
            }
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                assignedTo_Label.frame = CGRectMake(50, 60, 130, 15)
                
                PostStatus.frame  = CGRectMake(10, 60, 60, 15)
            }
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                assignedTo_Label.font = UIFont.boldSystemFontOfSize(12)
                assignedTo_Label.frame = CGRectMake(65, 60, 130, 15)
                
                PostStatus.font = UIFont.systemFontOfSize(12)
                PostStatus.frame  = CGRectMake(20, 60, 60, 15)
                
            }
                
            else
            {
                
            }

        }
        


    }

}
