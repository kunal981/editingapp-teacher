//
//  UnderReviewsViewController.swift
//  EditingApp_New
//
//  Created by mrinal khullar on 6/12/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit


//var count: Int = 0

var myInvitationResult = NSDictionary()
var myInvitationNumber = 1

class MyInvitationsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate
{
    
    
    
    @IBOutlet weak var menuBarBtn: UIButton!
       @IBOutlet weak var tableView: UITableView!
    
       //var myColor = [String]()
    
       var refreshControl:UIRefreshControl!
    
       var dynamicView = UIView()
    
       var myInvitatiosArray:NSMutableArray = []
    
       //var elements: NSMutableArray = []
    
      // var currentPage = 0
      // var nextPage = 0
    
       var x: CGFloat = 0.0
       var navx: CGFloat = 0.0
    
       var page = 0
    
       var limit = 50
    
       var hitWebService:Bool = true

    
    
      @IBOutlet weak var navBar: UINavigationBar!
    
    //MARK: - menu button method
      @IBAction func menuBtnPressed(sender: AnyObject)
      {
        
        if currentUserRole == "teacher"
        {
            if(x == -myMenuView.frame.size.width)
            {
                //navBar.frame.origin.x = 0
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = 0
                navx = myMenuView.frame.size.width
            }
                
            else
            {
                //navBar.frame.origin.x = myMenuView.frame.size.width
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = -myMenuView.frame.size.width
                navx = 0
            }
            
            
            UIView.animateWithDuration(0.5, animations: {
                
                self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
            })
        }
        else
        {
            if(x == -myMenuViewStudent.frame.size.width)
            {
                //navBar.frame.origin.x = 0
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = 0
                navx = myMenuViewStudent.frame.size.width
            }
                
            else
            {
                //navBar.frame.origin.x = myMenuView.frame.size.width
                //myMenuView.frame = CGRectMake(self.x, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
                x = -myMenuViewStudent.frame.size.width
                navx = 0
            }
            
            
            UIView.animateWithDuration(0.5, animations: {
                
                self.navBar.frame.origin.x = self.navx
                myMenuViewStudent.frame.origin.x = self.x
                
            })
        }
        
    }
    
    //MARK: - iPadCompatibility()
    func iPadCompatibility()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            menuBarBtn.frame = CGRectMake(0, 0, 45, 45)
        }
        
    }

       
    //MARK: - viewDidLoad() method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //STATUS BAR COLOR
        let statusBarView:UIView = UIView()
        statusBarView.frame = CGRectMake(0, 0, self.view.frame.width, 20)
        statusBarView.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 0.85)
        self.view.addSubview(statusBarView)
        
        navBar.barTintColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1)
        
        
        // x = -320
        currentVC = self
        if currentUserRole == "teacher"
        {
            self.view.addSubview(myMenuView)
            
            myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, self.view.frame.origin.y,myMenuView.frame.size.width ,myMenuView.frame.size.height)
            x = -myMenuView.frame.size.width
        }
        else
        {
            self.view.addSubview(myMenuViewStudent)
            
            myMenuViewStudent.frame = CGRectMake(-myMenuViewStudent.frame.size.width, self.view.frame.origin.y,myMenuViewStudent.frame.size.width ,myMenuViewStudent.frame.size.height)
            x = -myMenuViewStudent.frame.size.width
        }
        
        // myInvitationimageView.image = UIImage(named: "ic_my_invitation.png")
        
        
        iPadCompatibility()
        
        pullDownToRefresh()                 //calling pull down to refresh table view mehtod
        
        createActivityIndicator()                 //calling activity Indicator method
        dynamicView.hidden = false
        fetchData(0, limit: limit, reloadFromStart:false)
        
         tableView.frame.origin.y = navBar.frame.origin.y + navBar.frame.height + 10
    }
    
    
    //MARK: - refresh() method
    func pullDownToRefresh()
    {
        // Pull to Refresh Control
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: Selector("refresh"), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    
    func refresh()
    {
        println("after refreshing")
        
        fetchData(0, limit: myInvitatiosArray.count, reloadFromStart:true)
        
    }
    
    
    
    //MARK: - activityIndicator()
    func createActivityIndicator()
    {
        dynamicView.frame = CGRectMake(0,0,self.view.frame.width,self.view.frame.height)
        dynamicView.backgroundColor = UIColor.blackColor()
        dynamicView.alpha = 0.75
        
        
        //CREATING ACTIVITY INDICATOR
        var activityIndicator: UIActivityIndicatorView
        = UIActivityIndicatorView(activityIndicatorStyle:UIActivityIndicatorViewStyle.White)
        
        var x_activityIndicator = ((dynamicView.frame.width/2) - (activityIndicator.frame.width/2))
        var y_activityIndicator = ((dynamicView.frame.height/2) - (activityIndicator.frame.height/2))
        activityIndicator.frame = CGRectMake(x_activityIndicator, y_activityIndicator, activityIndicator.frame.width, activityIndicator.frame.height)
        
        dynamicView.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        
        self.view.addSubview(dynamicView)
    }
    
    
    
    //MARK: - fetchData() method for api
    func fetchData(page: Int, limit: Int, reloadFromStart:Bool)
    {
        
        // completed essay api
        var urlPath = "http://beta.brstdev.com/editingapp/webservice/index.php?operation=myinvitation&uid=\(user_id)&page=\(page)&limit=\(limit)"
        
        println(urlPath)
        
        let url: NSURL = NSURL(string : urlPath)!
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            
            
            
            if error != nil
            {
                
                self.dynamicView.hidden = true
                println(error.localizedDescription)
            }
            
            println(data)
            
            var err: NSError?
            
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                self.dynamicView.hidden = true
                
                var resultDict:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as! NSDictionary
                
                if err != nil
                {
                    println("JSON Error \(err!.localizedDescription)")
                }
                
                
                println(resultDict)
                
                var success = resultDict.valueForKey("status") as! String
                println(success)
                
                
                if var posts = resultDict.valueForKey("post") as? NSMutableArray
                {
                    
                    println(posts)
                    
                    if reloadFromStart
                    {
                        self.myInvitatiosArray = []
                    }
                    
                    for post in posts
                    {
                        self.myInvitatiosArray.addObject(post)
                    }
                    
                    if posts.count < limit
                    {
                        self.hitWebService = false
                    }
                    else
                    {
                        self.hitWebService = true
                    }
                    
                    self.tableView.reloadData()
                    
                    if reloadFromStart
                    {
                        self.refreshControl?.endRefreshing()
                    }
                    
                    
                    
                }
                else
                {
                    var alert = UIAlertView(title: "Error..!", message: "Data not Found", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                    
                    let okBtn = UIAlertAction(title:"OK", style: .Default)
                        {
                            (action: UIAlertAction!) -> Void in
                    }
                    
                    
                    alert.show()
                    println("Login failed1")
                    
                    self.dynamicView.hidden=true
                    
                }
                
                self.tableView.reloadData()
                
                self.dynamicView.hidden=true
                
            })
            
        })
        
        task.resume()
        
        
    }
    
    
    //MARK: - scrollViewDidScroll() method
    func scrollViewDidScroll(_scrollView: UIScrollView){
        var newScroll = Int(_scrollView.contentOffset.y)
        
        var maxScroll = (myInvitatiosArray.count*100)-Int(self.tableView.frame.height)
        
        if newScroll >= maxScroll && hitWebService == true
        {
            hitWebService = false
            fetchData(myInvitatiosArray.count, limit: limit, reloadFromStart:false)
        }
        
        println((6*100)-self.tableView.frame.height)
    }
    
    
    
    //MARK: - UItableView() methods
    func tableView(tableView: UITableView, numberOfSectionsInTable sections:Int) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return myInvitatiosArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! MyInvitationsTableViewCell
        
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        tableView.separatorColor = UIColor.clearColor()
        
        
        
        
        
        cell.backBtn.frame = CGRectMake(0, 0, cell.frame.width, cell.frame.height)
        //cell.backBtn.backgroundColor = UIColor.grayColor()
        cell.backBtn.userInteractionEnabled = false

        
        
        cell.cellView.frame = CGRectMake((0.03125*cell.contentView.frame.width), 0, (cell.contentView.frame.width)-(2*(0.03125*cell.contentView.frame.width)), 80)
        cell.cellView.layer.borderWidth = 1.5
        cell.cellView.layer.borderColor = UIColor.lightGrayColor().CGColor
        cell.cellView.layer.masksToBounds = true
        
        
        cell.colorLabel.frame = CGRectMake(0, 0, 0.01875*cell.contentView.frame.width, cell.cellView.frame.height)
        
        cell.colorLabel.backgroundColor = getRandomColor()    //calling colors method
        
//        switch (indexPath.row)
//        {
//        case 0,11,21:  cell.colorLabel.backgroundColor = UIColor(red: 219/255.0, green: 35/255.0, blue: 35/255.0, alpha: 1.0)    //for red color
//        case 1,12,22:  cell.colorLabel.backgroundColor = UIColor(red: 35/255.0, green: 158/255.0, blue: 219/255.0, alpha: 1.0)   //for blue color
//        case 2,13,23:  cell.colorLabel.backgroundColor = UIColor(red: 5/255.0, green: 177/255.0, blue: 173/255.0, alpha: 1.0)  // for green color
//        case 3,14,24:  cell.colorLabel.backgroundColor = UIColor(red: 115/255.0, green: 14/255.0, blue: 114/255.0, alpha: 1.0)  //purple color
//        case 4,15,25:  cell.colorLabel.backgroundColor = UIColor(red: 246/255.0, green: 209/255.0, blue: 27/255.0, alpha: 1.0)  //yellow color
//        case 5,16,26:  cell.colorLabel.backgroundColor = UIColor(red: 35/255.0, green: 158/255.0, blue: 219/255.0, alpha: 1.0)  //blue color
//        case 6,17,27:  cell.colorLabel.backgroundColor = UIColor(red: 219/255.0, green: 35/255.0, blue: 35/255.0, alpha: 1.0)    //red color
//        case 7,18,28:  cell.colorLabel.backgroundColor = UIColor(red: 5/255.0, green: 177/255.0, blue: 173/255.0, alpha: 1.0)   //for green color
//        case 8,19,29:  cell.colorLabel.backgroundColor = UIColor(red: 115/255.0, green: 14/255.0, blue: 114/255.0, alpha: 1.0)  //purple color
//        case 9,20,30:  cell.colorLabel.backgroundColor = UIColor(red: 246/255.0, green: 209/255.0, blue: 27/255.0, alpha: 1.0)  //yellow color
//        default:       cell.colorLabel.backgroundColor = UIColor(red: 5/255.0, green: 177/255.0, blue: 173/255.0, alpha: 1.0)  // for green color
//
//        }
        
        
        
        cell.view.frame = CGRectMake(0.109375*cell.contentView.frame.width, 0, 0.590625*cell.contentView.frame.width, 80)
       
        
        
        cell.countLabel.text = "\(indexPath.row + 1)"
        cell.countLabel.frame = CGRectMake(0.03125*cell.contentView.frame.width, 0, 30, cell.cellView.frame.height)
        cell.countLabel.textColor = UIColor.blackColor()
        //cell.countLabel.font = UIFont.systemFontOfSize(20)
        cell.countLabel.font = UIFont.boldSystemFontOfSize(15)
        cell.countLabel.textAlignment = NSTextAlignment.Center
        
        
        cell.text_Label.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
        cell.text_Label.frame = CGRectMake(0.03125*cell.contentView.frame.width, 0, 0.6875*cell.contentView.frame.width, 58)
        cell.text_Label.numberOfLines = 3
        cell.text_Label.font = UIFont.boldSystemFontOfSize(14)
        cell.text_Label.textColor = UIColor.darkGrayColor()
        
        
        cell.PostStatus.text = "Author:"
        cell.PostStatus.frame = CGRectMake(0.03125*cell.contentView.frame.width, 60, 0.390*cell.contentView.frame.width, 15)
        cell.PostStatus.font = UIFont.systemFontOfSize(11)
        cell.PostStatus.textColor = UIColor(red: 50/255.0, green: 50/255.0, blue: 50/255.0, alpha: 1.0)

        
        
        cell.assignedTo_Label.text = "Assigned to Robert Thomas"
        cell.assignedTo_Label.frame = CGRectMake(0.146875*cell.contentView.frame.width, 60, 0.390*cell.contentView.frame.width, 15)
        cell.assignedTo_Label.font = UIFont.boldSystemFontOfSize(11)
        cell.assignedTo_Label.textColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
        
//        cell.date_Label.text = "Jan 24, 2015"
//        cell.date_Label.frame = CGRectMake(0.453125*cell.contentView.frame.width, 60, 0.21875*cell.contentView.frame.width, 15)
//        cell.date_Label.font = UIFont.boldSystemFontOfSize(11)
//        cell.date_Label.textColor = UIColor(red: 80/255.0, green: 80/255.0, blue: 80/255.0, alpha: 1.0)
//        
//        cell.watchImage.image = UIImage(named: "icon-watch.png")
//        cell.watchImage.frame = CGRectMake(0.40625*cell.contentView.frame.width, 60, 0.04375*cell.contentView.frame.width, 14)
    
        
        
        cell.edit_btn.frame = CGRectMake(0.671875*cell.contentView.frame.width,20,0.25*cell.contentView.frame.width, 35)
        cell.edit_btn.backgroundColor = UIColor(red: 37/255, green: 176/255, blue:165/255, alpha: 1)
        cell.edit_btn.setTitle ("Edit", forState: UIControlState.Normal)
        cell.edit_btn.titleLabel?.font = UIFont.boldSystemFontOfSize(16)
        
        let editImage:UIImage = UIImage(named: "ic_edit.png")!
        
        cell.edit_btn.setImage(editImage, forState: UIControlState.Normal)
        cell.edit_btn.tag = indexPath.row
        
        cell.edit_btn.addTarget(self, action:"editMyInvitations:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
            
        cell.edit_btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5)
        cell.edit_btn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
        

                
        var content="";
        var obj = myInvitatiosArray[indexPath.row]as! NSDictionary
        println(obj)
        
        println(myInvitatiosArray[indexPath.row]["post-title"])
        println(myInvitatiosArray[indexPath.row]["author"])
        println(myInvitatiosArray[indexPath.row]["post-id"])
        
        cell.text_Label.text = myInvitatiosArray[indexPath.row]["post-title"] as? String
        //cell.date_Label.text = myInvitatiosArray[indexPath.row]["acceptdate"] as? String
        cell.assignedTo_Label.text = myInvitatiosArray[indexPath.row]["author"] as? String
        
        return cell
        
    }
    
   
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        myInvitationResult = myInvitatiosArray[indexPath.row] as! NSDictionary
        
        println(myInvitatiosArray[indexPath.row])
        
        
        myInvitationNumber = indexPath.row+1
        
        println("move to View My Invitation")
        
        println(myInvitatiosArray[indexPath.row]["post-id"])
        
        println(myInvitationResult)
        
        let underReviewVC = self.storyboard!.instantiateViewControllerWithIdentifier("ViewMyInvitationsViewControllerSegue") as! ViewMyInvitationsViewController
        
        self.navigationController?.pushViewController(underReviewVC, animated: true)

        
    }
    
    
    
     //MARK: - editMyInvitations method
    func editMyInvitations(sender: UIButton!)
    {
        myInvitationResult = myInvitatiosArray[sender.tag] as! NSDictionary
        
        println(myInvitationResult)
        
                
        let myInvitationsEditVC = self.storyboard!.instantiateViewControllerWithIdentifier("EditMyInvitationsViewControllerSegue") as! EditMyInvitationsViewController
    
        
        self.navigationController?.pushViewController(myInvitationsEditVC, animated: true)
    }
    

    
    

    
    //MARK: - getRandomColor() method
    func getRandomColor() -> UIColor
    {
        
        var randomRed:CGFloat = CGFloat(drand48())
        
        var randomGreen:CGFloat = CGFloat(drand48())
        
        var randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
    

    

    
    //MARK: - didReceiveMemoryWarning() method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
